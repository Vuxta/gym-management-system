﻿// <copyright file="StateMachine.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>12/5/2018 13:30:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, December 5, 2018

namespace VuxtaStudio.Common.FiniteStateMachine
{
    /// <summary>
    /// StateMachine handler class
    /// </summary>
    public class StateMachine
    {
        #region Private Fields

        IState m_CurrentState;

        #endregion

        #region Public Methods

        /// <summary>
        /// Change state to the desired state
        /// </summary>
        /// <param name="newState">state to be changed</param>
        public void ChangeState(IState newState)
        {
            if (m_CurrentState != null)
                m_CurrentState.OnStateExit();

            m_CurrentState = newState;
            m_CurrentState.OnStateEnter();
        }

        /// <summary>
        /// Update state machine
        /// </summary>
        public void OnMachineUpdate()
        {
            if (m_CurrentState != null)
                m_CurrentState.OnStateUpdate();
        }

        #endregion
    }
}
