﻿// <copyright file="EventCenter.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>12/3/2018 14:27:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, December 3, 2018

using System.Collections.Generic;
using System;

namespace VuxtaStudio.Common
{
    /// <summary>
    /// Event helper class that handles event messages
    /// </summary>
    public static class EventCenter
    {
        #region Private Fields

        private static Dictionary<Type, List<Delegate>> m_EventList = new Dictionary<Type, List<Delegate>>();

        #endregion

        #region Public Methods

        /// <summary>
        /// Register event listener with callback
        /// </summary>
        /// <typeparam name="T">Type of passing parameters</typeparam>
        /// <param name="callback">Callback functions</param>
        public static void StartListenToEvent<T>(Action<T> callback)
        {
            if (m_EventList.ContainsKey(typeof(T)))
            {
                m_EventList[typeof(T)].Add(callback);
            }
            else
            {
                List<Delegate> list = new List<Delegate>();
                list.Add(callback);
                m_EventList.Add(typeof(T), list);
            }
        }

        /// <summary>
        /// Remove event listener of callbacks
        /// </summary>
        /// <typeparam name="T">Type of passing parameters</typeparam>
        /// <param name="callback">Callback functions</param>
        public static void StopListenToEvent<T>(Action<T> callback)
        {
            if (m_EventList.ContainsKey(typeof(T)))
            {
                m_EventList[typeof(T)].Remove(callback);
            }
        }

        /// <summary>
        /// Triggering events to listeners
        /// </summary>
        /// <typeparam name="T">Type of passing parameters</typeparam>
        /// <param name="args">argument</param>
        public static void TriggerEvent<T>(T args)
        {
            List<Delegate> subscribers;
            if (m_EventList.TryGetValue(typeof(T), out subscribers))
            {
                foreach (Delegate action in subscribers)
                {
                    (action as Action<T>).Invoke(args);
                }
            }
        }

        #endregion
    }
}
