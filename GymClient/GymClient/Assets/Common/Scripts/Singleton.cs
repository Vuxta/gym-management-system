﻿// <copyright file="Singleton.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/24/2019 14:31:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 24, 2019

using System;

namespace VuxtaStudio.Common
{
    /// <summary>
    /// Class singleton
    /// </summary>
    /// <typeparam name="T">Type of class</typeparam>
    public class Singleton<T> where T : class
    {
        private static T m_Instance;

        public void Create()
        {
            if (m_Instance == null)
                m_Instance = (T)Activator.CreateInstance(typeof(T), true);

            return;
        }

        /* Serve the single instance to callers	*/
        public static T Instance
        {
            get
            {
                if (m_Instance == null)
                    m_Instance = (T)Activator.CreateInstance(typeof(T), true);

                return m_Instance;
            }
        }

        /*	Destroy	*/
        public void Destroy()
        {

            m_Instance = null;

            return;
        }
    }
}
