﻿// <copyright file="JsonHelper.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/26/2019 15:01:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 26, 2019

using System;
using System.Text.RegularExpressions;

namespace VuxtaStudio.Common.Utils
{
    /// <summary>
    /// Helper class to handle json array
    /// </summary>
    public static class JsonHelper
    {
        /// <summary>
        /// Parsing json string to arrays
        /// </summary>
        /// <typeparam name="T">Type of class</typeparam>
        /// <param name="json">Json strings</param>
        /// <returns></returns>
        public static T[] FromJson<T>(string json)
        {
            string serviceData = "{\"Items\":" + json + "}";
            UnityEngine.Debug.LogFormat("Try parsing {0} to objects", serviceData);

            Wrapper<T> wrapper = UnityEngine.JsonUtility.FromJson<Wrapper<T>>(serviceData);
            return wrapper.Items;
        }

        /// <summary>
        /// Format a list of objects to json strings
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <returns></returns>
        public static string ToJson<T>(T[] array)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;

            string jsonString = UnityEngine.JsonUtility.ToJson(wrapper);

            jsonString = jsonString.TrimEnd('}');
            jsonString = jsonString.Replace("{\"Items\":", string.Empty);

            return jsonString;
        }

        [Serializable]
        private class Wrapper<T>
        {
            public T[] Items;
        }
    }
}
