﻿// <copyright file="VirtualButtonComponent.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>01/04/2019 15:14:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, Janunary 04, 2019

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections;

namespace VuxtaStudio.Common.InputSystems
{
    /// <summary>
    /// A UGUI button component for button inputs
    /// </summary>
    public class VirtualButtonComponent : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IButtonHandler
    {
        #region Private Serialized Fields

        [SerializeField]
        private Image buttonImage;

        [SerializeField]
        private Color m_NormalColor = Color.white;

        [SerializeField]
        private Color m_PressedColor = Color.gray;

        #endregion

        #region Private Fields

        private YieldInstruction m_WaitForButtonCleanupCycle = new WaitForFixedUpdate();

        private bool m_ButtonDown = false;
        private bool m_ButtonPressed = false;
        private bool m_ButtonUp = false;

        #endregion

        #region Unity MonoBehaviour Methods

        private void Start()
        {
            buttonImage.color = m_NormalColor;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get button down status from input
        /// </summary>
        /// <returns>Return true if button is down</returns>
        public bool GetButtonDown()
        {
            return m_ButtonDown;
        }

        /// <summary>
        /// Get button pressed status from input
        /// </summary>
        /// <returns>Return true if button is pressed</returns>
        public bool GetButtonPressed()
        {
            return m_ButtonPressed;
        }

        /// <summary>
        /// Get button up status from input
        /// </summary>
        /// <returns>Return true if button is up</returns>
        public bool GetButtonUp()
        {
            return m_ButtonUp;
        }

        /// <summary>
        /// Implementation of IPointerDownHandler;
        /// Update status flags when user just press down the button from UGUI
        /// And start a corutine to reset button states
        /// </summary>
        /// <param name="eventData">Event data passed by the unity event system</param>
        public void OnPointerDown(PointerEventData eventData)
        {
            m_ButtonDown = true;
            m_ButtonPressed = true;
            m_ButtonUp = false;
            buttonImage.color = m_PressedColor;
            StartCoroutine(CleanupButtonState());
        }

        /// <summary>
        /// Implementation of IPointerUpHandler;
        /// Update status flags when user just release the button from UGUI
        /// And start a corutine to reset button states
        /// </summary>
        /// <param name="eventData">Event data passed by the unity event system</param>
        public void OnPointerUp(PointerEventData eventData)
        {
            m_ButtonDown = false;
            m_ButtonPressed = false;
            m_ButtonUp = true;
            buttonImage.color = m_NormalColor;
            StartCoroutine(CleanupButtonState());
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Reset button status after pointer up/down being called
        /// </summary>
        /// <returns>Wait for a button clean up cycle</returns>
        private IEnumerator CleanupButtonState()
        {
            yield return m_WaitForButtonCleanupCycle;

            if (m_ButtonUp)
            {
                m_ButtonUp = false;
                m_ButtonDown = false;
                m_ButtonPressed = false;
            }
            else if (m_ButtonDown)
            {
                m_ButtonUp = false;
                m_ButtonDown = false;
                m_ButtonPressed = true;
            }
        }

        #endregion
    }
}
