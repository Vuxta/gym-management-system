﻿// <copyright file="InputSystemManager.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>12/17/2018 13:23:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, December 17, 2018
using UnityEngine;
using System.Collections.Generic;

namespace VuxtaStudio.Common.InputSystems
{
    /// <summary>
    /// Input system manager responsible managing and stores all system for different user input
    /// </summary>
    public class InputSystemManager : MonoSingleton<InputSystemManager>
    {
        #region Private Fields

        private Dictionary<int, InputSystem> userInputSystems = new Dictionary<int, InputSystem>();

        #endregion

        #region Public Methods

        /// <summary>
        /// Register an user input system with an ID
        /// </summary>
        /// <param name="id">user ID</param>
        /// <param name="inputSystem">input system for specific user</param>
        /// <param name="systemProfile">input profile</param>
        /// <returns>Return true if register success</returns>
        public bool RegisterUser(int id,
                                out InputSystem inputSystem,
                                InputSystemProfile systemProfile = null)
        {
            inputSystem = null;

            if (userInputSystems.ContainsKey(id))
            {
                Debug.LogError("[InputSystemManager::RegisterUser] user has been registered, abort...");
                return false;
            }

            inputSystem = new InputSystem(systemProfile);
            userInputSystems.Add(id, inputSystem);

            return true;
        }

        /// <summary>
        /// Un-register user input
        /// </summary>
        /// <param name="id">used ID</param>
        /// <returns>Return true if unregister success</returns>
        public bool UnRegisterUser(int id)
        {
            InputSystem inputSystem;
            if (userInputSystems.TryGetValue(id, out inputSystem) == false)
            {
                Debug.LogError("[InputSystemManager::UnRegisterUser] user has not been registered, abort...");
                return false;
            }

            inputSystem = null;
            userInputSystems.Remove(id);

            return true;
        }

        /// <summary>
        /// Get user input system by the given ID
        /// </summary>
        /// <param name="id">user ID</param>
        /// <param name="inputSystem">input system</param>
        /// <returns>Return true if success</returns>
        public bool GetUser(int id, out InputSystem inputSystem)
        {
            inputSystem = null;
            if (userInputSystems.TryGetValue(id, out inputSystem) == false)
            {
                Debug.LogError("[InputSystemManager::GetUser] user has not been registered, abort...");
                return false;
            }

            return true;
        }

        #endregion
    }
}
