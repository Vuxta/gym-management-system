﻿// <copyright file="IButtonHandler.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>12/11/2018 10:53:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, December 11, 2018

namespace VuxtaStudio.Common.InputSystems
{
    /// <summary>
    /// Interface of button handler
    /// </summary>
    public interface IButtonHandler
    {
        /// <summary>
        /// Get button down status
        /// </summary>
        /// <returns>Return true if button is down</returns>
        bool GetButtonDown();

        /// <summary>
        /// Get button up status
        /// </summary>
        /// <returns>Return true if button is up</returns>
        bool GetButtonUp();

        /// <summary>
        /// Get button pressed status
        /// </summary>
        /// <returns>Return true if button is pressed</returns>
        bool GetButtonPressed();
    }
}
