﻿// <copyright file="IAxisHandler.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>12/11/2018 10:53:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, December 11, 2018

using UnityEngine;

namespace VuxtaStudio.Common.InputSystems
{
    /// <summary>
    /// Interface of axis handler
    /// </summary>
    public interface IAxisHandler
    {
        /// <summary>
        /// Get horizontal/X-Axis input value
        /// </summary>
        /// <returns>Return from -1f to 1f</returns>
        float GetHorizontalAxis();

        /// <summary>
        /// Get vertical/Y-Axis input value
        /// </summary>
        /// <returns>Return from -1f to 1f</returns>
        float GetVerticalAxis();

        /// <summary>
        /// Get XY-Axis input value
        /// </summary>
        /// <returns>Both X and Y value are from -1f to 1f</returns>
        Vector2 GetXYAxis();
    }
}
