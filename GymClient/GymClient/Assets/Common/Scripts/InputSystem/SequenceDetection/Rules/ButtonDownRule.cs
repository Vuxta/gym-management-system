﻿// <copyright file="ButtonDownRule.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>12/21/2018 14:00:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, December 21, 2018

namespace VuxtaStudio.Common.InputSystems
{
    /// <summary>
    /// Rules for button down
    /// </summary>
    public class ButtonDownRule : ISequenceRule
    {
        #region Private Fields

        private InputSystem m_InputSystem;
        private readonly int m_ButtonID;

        #endregion

        #region Constrcutors

        /// <summary>
        /// Constructor of button down rules
        /// </summary>
        /// <param name="buttonID">button ID to detect</param>
        /// <param name="inputSystem">input system to detect</param>
        public ButtonDownRule(int buttonID, InputSystem inputSystem = null)
        {
            this.m_InputSystem = inputSystem;
            this.m_ButtonID = buttonID;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Assign input system at runtime.
        /// This will be useful for setting up sequence ahead without assigning the input system first.
        /// </summary>
        /// <param name="inputSystem">input system to detect</param>
        public void AssignInputSystem(InputSystem inputSystem)
        {
            this.m_InputSystem = inputSystem;
        }

        /// <summary>
        /// Check if button is down
        /// </summary>
        /// <returns>Return true if button is down</returns>
        public bool Match()
        {
            if (m_InputSystem != null
                && m_InputSystem.GetButtonDown(m_ButtonID))
                return true;
            else
                return false;
        }

        #endregion
    }
}
