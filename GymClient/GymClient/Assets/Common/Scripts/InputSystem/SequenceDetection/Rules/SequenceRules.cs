﻿// <copyright file="SequenceRules.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>12/21/2018 14:00:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, December 21, 2018

using UnityEngine;

namespace VuxtaStudio.Common.InputSystems
{
    /// <summary>
    /// A collection of rule sequences
    /// </summary>
    public class SequenceRules : ISequenceRule
    {
        #region Private Fields

        private float m_LastSequenceMatchTime = 0f;
        private readonly float m_AllowTimeBetweenSequence = 0f;
        private readonly ISequenceRule[] m_Sequences;
        private int m_SequenceIndex = 0;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor of sequences.
        /// Set a collection of sequences, and check if all sequences is match
        /// </summary>
        /// <param name="allowTimeBetweenSequence">Tolerance time before next sequence achieved</param>
        /// <param name="sequences">Sequences in order</param>
        public SequenceRules(float allowTimeBetweenSequence = 0.3f, params ISequenceRule[] sequences)
        {
            this.m_AllowTimeBetweenSequence = allowTimeBetweenSequence;
            this.m_Sequences = sequences;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Assign input system to all sequences in this class
        /// This will be useful for setting up sequence ahead without assigning the input system first.
        /// </summary>
        /// <param name="inputSystem">input system to detect</param>
        public void AssignInputSystem(InputSystem inputSystem)
        {
            if (m_Sequences == null
                || m_Sequences.Length <= 0)
            {
                Debug.LogError("[SequenceRules::AssignInputSystem] fail to assign input system, sequences is null !!!");
                return;
            }

            for (int i = 0; i < m_Sequences.Length; i++)
            {
                if (m_Sequences[i] == null)
                    continue;

                m_Sequences[i].AssignInputSystem(inputSystem);
            }
        }

        /// <summary>
        /// Check if all sequences was matched, better to be called in any update calls
        /// </summary>
        /// <returns>Return true if all sequence are matched</returns>
        public bool Match()
        {
            if (Time.realtimeSinceStartup > m_LastSequenceMatchTime + m_AllowTimeBetweenSequence)
                m_SequenceIndex = 0;

            if (m_SequenceIndex < m_Sequences.Length)
            {
                if (m_Sequences[m_SequenceIndex].Match())
                {
                    m_LastSequenceMatchTime = Time.realtimeSinceStartup;
                    ++m_SequenceIndex;
                }

                if (m_SequenceIndex >= m_Sequences.Length)
                {
                    m_SequenceIndex = 0;
                    return true;
                }
                else
                {
                    return false;
                }
            }

            return false;
        }

        #endregion
    }
}
