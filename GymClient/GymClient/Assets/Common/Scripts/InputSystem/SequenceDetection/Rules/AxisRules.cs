﻿// <copyright file="AxisRules.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>12/21/2018 14:00:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, December 21, 2018

using UnityEngine;

namespace VuxtaStudio.Common.InputSystems
{
    /// <summary>
    /// Axis direction defines in Vector2
    /// </summary>
    public static class AxisDirection
    {
        public static readonly Vector2 Right = Vector2.right;
        public static readonly Vector2 Down = Vector2.down;
        public static readonly Vector2 Up = Vector2.up;
        public static readonly Vector2 Left = Vector2.left;
        public static readonly Vector2 UpRight = Vector2.one;
        public static readonly Vector2 UpLeft = new Vector2(-1f, 1f);
        public static readonly Vector2 DownLeft = new Vector2(-1f, -1f);
        public static readonly Vector2 DownRight = new Vector2(1f, -1f);
    }

    /// <summary>
    /// Rules to detect if user input axis is in certain direction
    /// </summary>
    public class AxisRules : ISequenceRule
    {
        #region Private Fields

        private InputSystem m_InputSystem;
        private readonly int m_AxisID;

        private readonly Vector2 m_DirectionToDetect;
        private readonly float m_AngleThreshold;
        private readonly float m_SqrMagnitudeThreshold;

        private float m_Angle = 0f;
        private Vector2 m_CurrentInputAxis = Vector2.zero;

        #endregion

        #region Constructors

        /// <summary>
        /// Constrcut a rules to detect if user input axis is in certain direction
        /// </summary>
        /// <param name="axisID">Axis ID to detect</param>
        /// <param name="directionToDetect">Input direction to detect</param>
        /// <param name="magnitudeThreshold">Minimum magnitude threshold of axis vector</param>
        /// <param name="angleThreshold">Angle tolerance to desired direction</param>
        /// <param name="inputSystem">Input system to detect</param>
        public AxisRules(int axisID,
                        Vector2 directionToDetect,
                        float magnitudeThreshold = 0.3f,
                        float angleThreshold = 45f,
                        InputSystem inputSystem = null)
        {
            this.m_InputSystem = inputSystem;
            this.m_AxisID = axisID;

            this.m_DirectionToDetect = directionToDetect;
            this.m_AngleThreshold = angleThreshold;
            this.m_SqrMagnitudeThreshold = magnitudeThreshold * magnitudeThreshold;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Assign input system at runtime.
        /// This will be useful for setting up sequence ahead without assigning the input system first.
        /// </summary>
        /// <param name="inputSystem">input system to detect</param>
        public void AssignInputSystem(InputSystem inputSystem)
        {
            this.m_InputSystem = inputSystem;
        }

        /// <summary>
        /// Check if input axis direction and magnitude matched to the requirement
        /// </summary>
        /// <returns>Return true if matched</returns>
        public bool Match()
        {
            if (m_InputSystem == null)
                return false;

            m_CurrentInputAxis = m_InputSystem.GetXYAxis(m_AxisID);

            if (m_CurrentInputAxis.sqrMagnitude < m_SqrMagnitudeThreshold)
                return false;

            m_Angle = Vector2.Angle(m_DirectionToDetect, m_CurrentInputAxis);

            if (m_Angle < m_AngleThreshold)
                return true;
            else
                return false;
        }

        #endregion
    }
}
