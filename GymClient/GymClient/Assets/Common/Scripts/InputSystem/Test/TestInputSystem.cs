﻿#if (VUXSTUDIO_COMMON_INPUTSYSTEMS_TEST)

using UnityEngine;

namespace VuxtaStudio.Common.InputSystems.Test
{
    public class TestInputSystem : MonoBehaviour
    {
        public enum TestID
        {
            DirectionAxis = 0,
            AimAxis = 1,
            JumpButton = 2,
            FireButton = 3,
        }

        private InputSystemProfile m_Profile = new InputSystemProfile();
        private InputSystem m_InputSystem;

        private float m_DirHorizontalValue = 0f;
        private float m_DirVerticalValue = 0f;
        private Vector2 m_DirAxis = Vector2.zero;

        private float m_AimHorizontalValue = 0f;
        private float m_AimVerticalValue = 0f;
        private Vector2 m_AimAxis = Vector2.zero;

        private bool m_JumpPressed = false;

        private bool m_FirePressed = false;

        SequenceRules m_TestSequence;

        private void Start()
        {
            //Direction axis
            ButtonAxisComponent directionAxisComponent = gameObject.AddComponent<ButtonAxisComponent>();
            directionAxisComponent.Initialize(new IButtonHandler[] { new KeyboardButtonHandler(KeyCode.D), new KeyboardButtonHandler(KeyCode.RightArrow) },
                                            new IButtonHandler[] { new KeyboardButtonHandler(KeyCode.A), new KeyboardButtonHandler(KeyCode.LeftArrow) },
                                            new IButtonHandler[] { new KeyboardButtonHandler(KeyCode.W), new KeyboardButtonHandler(KeyCode.UpArrow) },
                                            new IButtonHandler[] { new KeyboardButtonHandler(KeyCode.S), new KeyboardButtonHandler(KeyCode.DownArrow) });

            IAxisHandler[] directionAxisHandler = new IAxisHandler[] { directionAxisComponent };
            m_Profile.RegisterAxisHandler((int)TestID.DirectionAxis, directionAxisHandler);

            //Aim axis
            ButtonAxisComponent aimAxisComponentFirst = gameObject.AddComponent<ButtonAxisComponent>();
            aimAxisComponentFirst.Initialize(new IButtonHandler[] { new KeyboardButtonHandler(KeyCode.Keypad6) },
                                            new IButtonHandler[] { new KeyboardButtonHandler(KeyCode.Keypad4) },
                                            new IButtonHandler[] { new KeyboardButtonHandler(KeyCode.Keypad8) },
                                            new IButtonHandler[] { new KeyboardButtonHandler(KeyCode.Keypad2) });

            SimpleButtonAxisHandler aimAxisComponentSecond = new SimpleButtonAxisHandler(new KeyboardButtonHandler(KeyCode.L),
                                                                                        new KeyboardButtonHandler(KeyCode.J),
                                                                                        new KeyboardButtonHandler(KeyCode.I),
                                                                                        new KeyboardButtonHandler(KeyCode.K));

            IAxisHandler[] aimAxisHandler = new IAxisHandler[] { aimAxisComponentFirst, aimAxisComponentSecond };
            m_Profile.RegisterAxisHandler((int)TestID.AimAxis, aimAxisHandler);

            //Jump Button
            IButtonHandler[] jumpButtons = new IButtonHandler[] { new KeyboardButtonHandler(KeyCode.Space) };
            m_Profile.RegisterButtonHandler((int)TestID.JumpButton, jumpButtons);

            IButtonHandler[] fireButtons = new IButtonHandler[] { new KeyboardButtonHandler(KeyCode.RightControl), new KeyboardButtonHandler(KeyCode.RightAlt) };
            m_Profile.RegisterButtonHandler((int)TestID.FireButton, fireButtons);

            InputSystemManager.Instance.RegisterUser(0, out m_InputSystem, m_Profile);

            //Sequences
            m_TestSequence = new SequenceRules(0.3f,
                                                new ButtonDownRule((int)TestID.FireButton),
                                                new AxisRules((int)TestID.DirectionAxis, AxisDirection.Down, 0.01f),
                                                new ButtonDownRule((int)TestID.JumpButton));

            m_TestSequence.AssignInputSystem(m_InputSystem);
        }

        private void Update()
        {
            if (m_TestSequence.Match())
            {
                Debug.Log("Test sequence matched");
            }

            //Dir Axis
            float horizontalValue = m_InputSystem.GetHorizontalAxis((int)TestID.DirectionAxis);
            float verticalValue = m_InputSystem.GetVerticalAxis((int)TestID.DirectionAxis);
            Vector2 axisValue = m_InputSystem.GetXYAxis((int)TestID.DirectionAxis);

            if (horizontalValue != m_DirHorizontalValue
                || verticalValue != m_DirVerticalValue
                || axisValue != m_DirAxis)
            {
                m_DirHorizontalValue = horizontalValue;
                m_DirVerticalValue = verticalValue;
                m_DirAxis = axisValue;

                Debug.LogFormat("Dir H: {0}, V: {1}, Axis: {2}", m_DirHorizontalValue, m_DirVerticalValue, m_DirAxis);
            }

            //Aim Axis
            horizontalValue = m_InputSystem.GetHorizontalAxis((int)TestID.AimAxis);
            verticalValue = m_InputSystem.GetVerticalAxis((int)TestID.AimAxis);
            axisValue = m_InputSystem.GetXYAxis((int)TestID.AimAxis);

            if (horizontalValue != m_AimHorizontalValue
                || verticalValue != m_AimVerticalValue
                || axisValue != m_AimAxis)
            {
                m_AimHorizontalValue = horizontalValue;
                m_AimVerticalValue = verticalValue;
                m_AimAxis = axisValue;

                Debug.LogFormat("Aim H: {0}, V: {1}, Axis: {2}", m_AimHorizontalValue, m_AimVerticalValue, m_AimAxis);
            }

            //Jump buttons
            if (m_InputSystem.GetButtonDown((int)TestID.JumpButton))
            {
                Debug.Log("Jump button down");
            }

            if (m_InputSystem.GetButtonUp((int)TestID.JumpButton))
            {
                Debug.Log("Jump button up");
            }

            if (m_InputSystem.GetButtonPressed((int)TestID.JumpButton))
            {
                if (m_JumpPressed == false)
                {
                    Debug.Log("Jump button pressed");
                    m_JumpPressed = true;
                }
            }
            else
            {
                if (m_JumpPressed == true)
                {
                    Debug.Log("Jump button released");
                    m_JumpPressed = false;
                }
            }

            if (m_InputSystem.GetButtonDown((int)TestID.FireButton))
            {
                Debug.Log("Fire button down");
            }

            if (m_InputSystem.GetButtonUp((int)TestID.FireButton))
            {
                Debug.Log("Fire button up");
            }

            if (m_InputSystem.GetButtonPressed((int)TestID.FireButton))
            {
                if (m_FirePressed == false)
                {
                    Debug.Log("Fire button pressed");
                    m_FirePressed = true;
                }
            }
            else
            {
                if (m_FirePressed == true)
                {
                    Debug.Log("Fire button released");
                    m_FirePressed = false;
                }
            }
        }
    }
}

#endif