﻿// <copyright file="VirtualJoystickAxisComponent.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>01/04/2019 15:14:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, Janunary 04, 2019

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace VuxtaStudio.Common.InputSystems
{
    /// <summary>
    /// A UGUI joystick component for touch screen axis input
    /// </summary>
    public class VirtualJoystickAxisComponent : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IAxisHandler
    {
        #region Private Serialized Field

        [SerializeField]
        private Image m_JoystickContainer;

        [SerializeField]
        private Image m_Joystick;

        [SerializeField]
        private bool m_IsFixedPosition = false;

        [SerializeField]
        private bool m_IsAlwaysVisible = false;

        #endregion

        #region Private Fields

        private Vector2 m_InputPosition = Vector3.zero;

        #endregion

        #region Unity MonoBehaviour Methods

        /// <summary>
        /// Init joystick visibility
        /// </summary>
        private void Start()
        {
            m_JoystickContainer.enabled = m_IsAlwaysVisible;
            m_Joystick.enabled = m_IsAlwaysVisible;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Interface implementation on IDragHandler;
        /// This functions transform touch screen input position as Axis inputs, also updates the Knob(Joystick) position accordingly
        /// </summary>
        /// <param name="eventData">Event data passed by the unity event system</param>
        public void OnDrag(PointerEventData eventData)
        {
            Vector2 pos;
            if (RectTransformUtility.ScreenPointToLocalPointInRectangle(m_JoystickContainer.rectTransform,
                                                                        eventData.position,
                                                                        eventData.pressEventCamera,
                                                                        out pos))
            {

                pos.x = (pos.x / m_JoystickContainer.rectTransform.sizeDelta.x);
                pos.y = (pos.y / m_JoystickContainer.rectTransform.sizeDelta.y);

                m_InputPosition = pos;
                m_InputPosition = (m_InputPosition.magnitude > 1.0f) ? m_InputPosition.normalized : m_InputPosition;

                m_Joystick.rectTransform.anchoredPosition = new Vector2(m_InputPosition.x * (m_JoystickContainer.rectTransform.sizeDelta.x * .4f),
                                                                         m_InputPosition.y * (m_JoystickContainer.rectTransform.sizeDelta.y * .4f));
            }
        }

        /// <summary>
        /// Interface implementation of IPointerDownHandler;
        /// This functions force enabled the Joystick/Container images visibility,
        /// also update the transform of container rectangle to where user point on touch screen
        /// </summary>
        /// <param name="eventData">Event data passed by the unity event system</param>
        public void OnPointerDown(PointerEventData eventData)
        {
            m_JoystickContainer.enabled = true;
            m_Joystick.enabled = true;

            if (!m_IsFixedPosition)
            {
                m_JoystickContainer.rectTransform.position = eventData.position;
            }

            OnDrag(eventData);
        }

        /// <summary>
        /// Interface implementation of IPointerUpHandler;
        /// this functions updates the visibility of joystick and user unpressed the joystick,
        /// and reset axis position as well
        /// </summary>
        /// <param name="eventData">Event data passed by the unity event system</param>
        public void OnPointerUp(PointerEventData eventData)
        {
            m_JoystickContainer.enabled = m_IsAlwaysVisible;
            m_Joystick.enabled = m_IsAlwaysVisible;
            m_InputPosition = Vector3.zero;
            m_Joystick.rectTransform.anchoredPosition = Vector2.zero;
        }

        /// <summary>
        /// Get horizontal/X-Axis input value
        /// </summary>
        /// <returns>Return from -1f to 1f</returns>
        public float GetHorizontalAxis()
        {
            return m_InputPosition.x;
        }

        /// <summary>
        /// Get vertical/Y-Axis input value
        /// </summary>
        /// <returns>Return from -1f to 1f</returns>
        public float GetVerticalAxis()
        {
            return m_InputPosition.y;
        }

        /// <summary>
        /// Get XY-Axis input value
        /// </summary>
        /// <returns>Both X and Y value are from -1f to 1f</returns>
        public Vector2 GetXYAxis()
        {
            return m_InputPosition;
        }

        #endregion
    }
}
