﻿// <copyright file="SimpleButtonAxisHandler.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>12/11/2018 10:53:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, December 11, 2018

using UnityEngine;

namespace VuxtaStudio.Common.InputSystems
{
    /// <summary>
    /// Axis handler for simulating axis control by the desired button handlers.
    /// This handlers simply returns 1f/-1f when button was pressed
    /// For more complex draging simulation, see ButtonAxisComponent.cs
    /// </summary>
    public class SimpleButtonAxisHandler : IAxisHandler
    {
        #region Private Fields

        private IButtonHandler m_XAxisPositiveHandler;
        private IButtonHandler m_XAxisNegativeHandler;
        private IButtonHandler m_YAxisPositiveHandler;
        private IButtonHandler m_YAxisNegativeHandler;

        private Vector2 m_VectorAxis;
        private float m_HorizontalValue;
        private float m_VerticalValue;

        #endregion

        #region Constructors

        /// <summary>
        /// Constructor of button axis handler.
        /// Assign all button handlers for different axis and positive/negative values
        /// </summary>
        /// <param name="XAxisPositiveHandler">Button handler represents positive values of Horizontal/X-Axis</param>
        /// <param name="XAxisNegativeHandler">Button handler represents negative values of Horizontal/X-Axis</param>
        /// <param name="YAxisPositiveHandler">Button handler represents positive values of Vertical/Y-Axis</param>
        /// <param name="YAxisNegativeHandler">Button handler represents negative values of Vertical/Y-Axis</param>
        public SimpleButtonAxisHandler(IButtonHandler XAxisPositiveHandler,
                                        IButtonHandler XAxisNegativeHandler,
                                        IButtonHandler YAxisPositiveHandler,
                                        IButtonHandler YAxisNegativeHandler)
        {
            this.m_XAxisPositiveHandler = XAxisPositiveHandler;
            this.m_XAxisNegativeHandler = XAxisNegativeHandler;
            this.m_YAxisPositiveHandler = YAxisPositiveHandler;
            this.m_YAxisNegativeHandler = YAxisNegativeHandler;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get horizontal/X-Axis input value
        /// </summary>
        /// <returns>Return from -1f to 1f</returns>
        public float GetHorizontalAxis()
        {
            m_HorizontalValue = 0f;

            if (m_XAxisPositiveHandler != null
                && m_XAxisPositiveHandler.GetButtonPressed())
            {
                m_HorizontalValue += 1f;
            }

            if (m_XAxisNegativeHandler != null
                && m_XAxisNegativeHandler.GetButtonPressed())
            {
                m_HorizontalValue -= 1f;
            }

            return m_HorizontalValue;
        }

        /// <summary>
        /// Get vertical/Y-Axis input value
        /// </summary>
        /// <returns>Return from -1f to 1f</returns>
        public float GetVerticalAxis()
        {
            m_VerticalValue = 0f;

            if (m_YAxisPositiveHandler != null
                && m_YAxisPositiveHandler.GetButtonPressed())
            {
                m_VerticalValue += 1f;
            }

            if (m_YAxisNegativeHandler != null
                && m_YAxisNegativeHandler.GetButtonPressed())
            {
                m_VerticalValue -= 1f;
            }

            return m_VerticalValue;
        }

        /// <summary>
        /// Get XY-Axis input value
        /// </summary>
        /// <returns>Both X and Y value are from -1f to 1f</returns>
        public Vector2 GetXYAxis()
        {
            m_VectorAxis.x = GetHorizontalAxis();
            m_VectorAxis.y = GetVerticalAxis();

            return m_VectorAxis;
        }

        #endregion
    }
}
