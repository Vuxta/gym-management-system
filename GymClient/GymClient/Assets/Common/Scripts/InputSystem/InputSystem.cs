﻿// <copyright file="InputSystem.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>12/11/2018 10:53:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, December 11, 2018
using UnityEngine;

namespace VuxtaStudio.Common.InputSystems
{
    /// <summary>
    /// Input system manages status of button and axis input
    /// </summary>
    public class InputSystem
    {
        #region Private Fields

        private InputSystemProfile m_InputSystemProfile;

        #endregion

        #region Constructors

        public InputSystem(InputSystemProfile profile)
        {
            this.m_InputSystemProfile = profile;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Set an input profile which defines button and axis inputs
        /// </summary>
        /// <param name="profile"></param>
        public void UseProfile(InputSystemProfile profile)
        {
            this.m_InputSystemProfile = profile;
        }

        /// <summary>
        /// Get button down status
        /// </summary>
        /// <param name="id">Button id registered in the profile</param>
        /// <returns>Return true once the button was pressed down</returns>
        public bool GetButtonDown(int id)
        {
            IButtonHandler[] buttonHandlers;
            if (GetButtonHandlers(id, out buttonHandlers) == false)
                return false;

            long pressedMask = 0;
            long downMask = 0;
            long mask = 0;

            for (int i = 0; i < buttonHandlers.Length; i++)
            {
                mask = 1 << i;

                if (buttonHandlers[i] == null)
                    continue;

                if (buttonHandlers[i].GetButtonDown())
                {
                    downMask |= mask;
                }

                if (buttonHandlers[i].GetButtonPressed())
                {
                    pressedMask |= mask;
                }
            }

            if (downMask == 0)
                return false;

            long orMask = downMask | pressedMask;

            if (orMask == downMask)
                return true;

            return false;
        }

        /// <summary>
        /// Get button up status
        /// </summary>
        /// <param name="id">Button id registered in the profile</param>
        /// <returns>Return true once the button was released from pressed</returns>
        public bool GetButtonUp(int id)
        {
            if (GetButtonPressed(id))
                return false;

            IButtonHandler[] buttonHandlers;
            if (GetButtonHandlers(id, out buttonHandlers) == false)
                return false;

            for (int i = 0; i < buttonHandlers.Length; i++)
            {
                if (buttonHandlers[i] == null)
                    continue;

                if (buttonHandlers[i].GetButtonUp())
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Get button pressed status
        /// </summary>
        /// <param name="id">Button id registered in the profile</param>
        /// <returns>Return true if button is pressed</returns>
        public bool GetButtonPressed(int id)
        {
            IButtonHandler[] buttonHandlers;
            if (GetButtonHandlers(id, out buttonHandlers) == false)
                return false;

            for (int i = 0; i < buttonHandlers.Length; i++)
            {
                if (buttonHandlers[i] == null)
                    continue;

                if (buttonHandlers[i].GetButtonPressed())
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Get horizontal/X-Axis input value
        /// </summary>
        /// <param name="id">Axis id registered in the profile</param>
        /// <returns>Return from -1f to 1f</returns>
        public float GetHorizontalAxis(int id)
        {
            IAxisHandler[] axisHandlers;
            if (GetAxisHandlers(id, out axisHandlers) == false)
                return 0f;

            int foundIndex = -1;
            float currentAbsoluteMax = -1f;
            float absoluteValue = 0f;

            for (int i = 0; i < axisHandlers.Length; i++)
            {
                if (axisHandlers[i] == null)
                    continue;

                absoluteValue = Mathf.Abs(axisHandlers[i].GetHorizontalAxis());
                if (absoluteValue > currentAbsoluteMax)
                {
                    foundIndex = i;
                    currentAbsoluteMax = absoluteValue;
                }
            }

            if (foundIndex < 0)
                return 0f;
            else
                return axisHandlers[foundIndex].GetHorizontalAxis();
        }

        /// <summary>
        /// Get vertical/Y-Axis input value
        /// </summary>
        /// <param name="id">Axis id registered in the profile</param>
        /// <returns>Return from -1f to 1f</returns>
        public float GetVerticalAxis(int id)
        {
            IAxisHandler[] axisHandlers;
            if (GetAxisHandlers(id, out axisHandlers) == false)
                return 0f;

            int foundIndex = -1;
            float currentAbsoluteMax = -1f;
            float absoluteValue = 0f;

            for (int i = 0; i < axisHandlers.Length; i++)
            {
                if (axisHandlers[i] == null)
                    continue;

                absoluteValue = Mathf.Abs(axisHandlers[i].GetVerticalAxis());
                if (absoluteValue > currentAbsoluteMax)
                {
                    foundIndex = i;
                    currentAbsoluteMax = absoluteValue;
                }
            }

            if (foundIndex < 0)
                return 0f;
            else
                return axisHandlers[foundIndex].GetVerticalAxis();
        }

        /// <summary>
        /// Get XY-Axis input value
        /// </summary>
        /// <param name="id">Axis id registered in the profile</param>
        /// <returns>Both X and Y value are from -1f to 1f</returns>
        public Vector2 GetXYAxis(int id)
        {
            IAxisHandler[] axisHandlers;
            if (GetAxisHandlers(id, out axisHandlers) == false)
                return Vector2.zero;

            int foundIndex = -1;
            float currentSqrMagnitude = 0.0001f;
            float sqrMagnitude = 0f;

            for (int i = 0; i < axisHandlers.Length; i++)
            {
                if (axisHandlers[i] == null)
                    continue;

                sqrMagnitude = axisHandlers[i].GetXYAxis().sqrMagnitude;
                if (sqrMagnitude > currentSqrMagnitude)
                {
                    foundIndex = i;
                    currentSqrMagnitude = sqrMagnitude;
                }
            }

            if (foundIndex < 0)
                return Vector2.zero;
            else
                return axisHandlers[foundIndex].GetXYAxis();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get all button handlers registered with given ID in the input profile
        /// </summary>
        /// <param name="id">Button id registered in the profile</param>
        /// <param name="buttonHandlers">Button handlers registered with button ID</param>
        /// <returns>Return true if handlers with specific ID was registered</returns>
        private bool GetButtonHandlers(int id, out IButtonHandler[] buttonHandlers)
        {
            buttonHandlers = null;
            if (m_InputSystemProfile == null)
                return false;

            if (m_InputSystemProfile.ButtonHandlerList.TryGetValue(id, out buttonHandlers) == false)
                return false;

            return true;
        }

        /// <summary>
        /// Get all axis handlers registered with given ID in the input profile
        /// </summary>
        /// <param name="id">Axis id registered in the profile</param>
        /// <param name="buttonHandlers">Axis handlers registered with axis ID</param>
        /// <returns>Return true if handlers with specific ID was registered</returns>
        private bool GetAxisHandlers(int id, out IAxisHandler[] axisHandlers)
        {
            axisHandlers = null;
            if (m_InputSystemProfile == null)
                return false;

            if (m_InputSystemProfile.AxisHandlerList.TryGetValue(id, out axisHandlers) == false)
                return false;

            return true;
        }

        #endregion
    }
}
