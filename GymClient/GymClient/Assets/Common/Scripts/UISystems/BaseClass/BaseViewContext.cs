﻿// <copyright file="BaseViewContext.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/24/2019 14:31:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 24, 2019

namespace VuxtaStudio.Common.UISystems
{
    /// <summary>
    /// Base class of storing info about current view context
    /// </summary>
    public class BaseViewContext
    {
        public UIType ViewType { get; private set; }

        public BaseViewContext(UIType viewType)
        {
            ViewType = viewType;
        }
    }
}
