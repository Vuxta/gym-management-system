﻿// <copyright file="UIManager.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/24/2019 14:31:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 24, 2019

using UnityEngine;
using System.Collections.Generic;
using VuxtaStudio.Common.Extensions;

namespace VuxtaStudio.Common.UISystems
{
    /// <summary>
    /// Manager to create/destroy UI views
    /// </summary>
    public class UIManager : Singleton<UIManager>
    {
        private Dictionary<UIType, GameObject> m_UIDictionary = new Dictionary<UIType, GameObject>();
        private Transform m_Canvas;

        /// <summary>
        /// Constructors to find root UI canvas
        /// </summary>
        private UIManager()
        {
            m_Canvas = GameObject.Find("Canvas").transform;

            if (m_Canvas == null)
            {
                Debug.LogError("Missing Canvas");
                return;
            }

            foreach (Transform item in m_Canvas)
            {
                GameObject.Destroy(item.gameObject);
            }
        }

        /// <summary>
        /// Find UI by UIType and loading them from resource folder
        /// </summary>
        /// <param name="uiType">UI Type</param>
        /// <returns>Game object loaded from resources</returns>
        public GameObject GetSingleUI(UIType uiType)
        {
            if (m_Canvas == null)
            {
                Debug.LogError("Missing Canvas");
                return null;
            }

            if (m_UIDictionary.ContainsKey(uiType) == false || m_UIDictionary[uiType] == null)
            {
                GameObject go = GameObject.Instantiate(Resources.Load<GameObject>(uiType.Path)) as GameObject;
                go.transform.SetParent(m_Canvas, false);
                go.name = uiType.Name;
                m_UIDictionary.AddOrReplace(uiType, go);
                return go;
            }
            return m_UIDictionary[uiType];
        }

        /// <summary>
        /// Destroy by UI type
        /// </summary>
        /// <param name="uiType">UI Type</param>
        public void DestroySingleUI(UIType uiType)
        {
            if (!m_UIDictionary.ContainsKey(uiType))
            {
                return;
            }

            if (m_UIDictionary[uiType] == null)
            {
                m_UIDictionary.Remove(uiType);
                return;
            }

            GameObject.Destroy(m_UIDictionary[uiType]);
            m_UIDictionary.Remove(uiType);
        }
    }
}
