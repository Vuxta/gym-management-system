﻿// <copyright file="ViewContextManager.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/24/2019 14:31:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 24, 2019

using System.Collections.Generic;

namespace VuxtaStudio.Common.UISystems
{
    /// <summary>
    /// Managers that store stack info about view context
    /// </summary>
    public class ViewContextManager : Singleton<ViewContextManager>
    {
        private Stack<BaseViewContext> m_ContextStack = new Stack<BaseViewContext>();

        /// <summary>
        /// Push a new view context and jump to it
        /// </summary>
        /// <param name="nextContext"></param>
        public void Push(BaseViewContext nextContext)
        {

            if (m_ContextStack.Count != 0)
            {
                BaseViewContext curContext = m_ContextStack.Peek();
                BaseView curView = Singleton<UIManager>.Instance.GetSingleUI(curContext.ViewType).GetComponent<BaseView>();
                curView.OnPause(curContext);
            }

            m_ContextStack.Push(nextContext);
            BaseView nextView = Singleton<UIManager>.Instance.GetSingleUI(nextContext.ViewType).GetComponent<BaseView>();
            nextView.OnEnter(nextContext);
        }

        /// <summary>
        /// Pop a view context and return to the previous one
        /// </summary>
        public void Pop()
        {
            if (m_ContextStack.Count != 0)
            {
                BaseViewContext curContext = m_ContextStack.Peek();
                m_ContextStack.Pop();

                BaseView curView = Singleton<UIManager>.Instance.GetSingleUI(curContext.ViewType).GetComponent<BaseView>();
                curView.OnExit(curContext);
            }

            if (m_ContextStack.Count != 0)
            {
                BaseViewContext lastContext = m_ContextStack.Peek();
                BaseView curView = Singleton<UIManager>.Instance.GetSingleUI(lastContext.ViewType).GetComponent<BaseView>();
                curView.OnResume(lastContext);
            }
        }

        public BaseViewContext PeekOrNull()
        {
            if (m_ContextStack.Count != 0)
            {
                return m_ContextStack.Peek();
            }
            return null;
        }
    }
}
