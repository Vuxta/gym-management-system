﻿// <copyright file="GridToggler.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/27/2019 10:50:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 27, 2019

using System;
using UnityEngine;
using UnityEngine.UI;
using VuxtaStudio.Common.Extensions;

namespace VuxtaStudio.Common.UISystems
{
    /// <summary>
    /// Grid Layout controller for toggle items
    /// </summary>
    public class GridToggler : MonoBehaviour
    {
        // public UI elements //
        [SerializeField]
        private Transform m_ItemPrefab;

        [SerializeField]
        private GridLayoutGroup m_Grid;

        public void Init(Action<bool, string> callback, params string[] toggleNames)
        {
            Clear();

            for (int i = 0; i < toggleNames.Length; i++)
            {
                Transform item = m_Grid.transform.AddChildFromPrefab(m_ItemPrefab, i.ToString());
                item.GetComponent<ToggleItem>().Init(toggleNames[i], callback);
            }
        }

        public void Clear()
        {
            if (m_Grid != null)
            {
                m_Grid.GetComponent<RectTransform>().DestroyChildren();
            }
        }
    }
}
