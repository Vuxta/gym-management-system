﻿// <copyright file="PhysicalInfo.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/08/2019 16:00:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 8, 2019

using System;

namespace VuxtaStudio.GymClient.WebServices.Models
{
    /// <summary>
    /// Physical info
    /// </summary>
    [Serializable]
    public class PhysicalInfo : JsonData
    {
        public double Height;

        public double Weight;

        public string Date;
    }
}
