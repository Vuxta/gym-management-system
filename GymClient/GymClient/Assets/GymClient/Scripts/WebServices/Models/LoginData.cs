﻿// <copyright file="LoginData.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/24/2019 14:31:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 24, 2019

namespace VuxtaStudio.GymClient.WebServices.Models
{
    /// <summary>
    /// Login model
    /// </summary>
    public class LoginData : JsonData
    {
        public string UserId;
        public string Password;
        public string Key;
    }
}
