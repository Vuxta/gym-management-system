﻿using System;
using System.Collections.Generic;

namespace VuxtaStudio.GymClient.WebServices.Models
{
    [Serializable]
    public class CourseCategory : CrudModelBase
    {
        public string Name;
        public string Description;
    }
}
