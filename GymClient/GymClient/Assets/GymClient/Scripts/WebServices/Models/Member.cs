﻿using System;
using System.Collections.Generic;
namespace VuxtaStudio.GymClient.WebServices.Models
{
    /// <summary>
    /// Member CRUD model
    /// </summary>
    [Serializable]
    public class Member : UserCrudModel
    {
        public List<string> ContactNumbers;
        public List<string> ContactEmails;
    }
}
