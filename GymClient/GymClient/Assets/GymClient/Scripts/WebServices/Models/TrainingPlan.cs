﻿using System;
using System.Collections.Generic;

namespace VuxtaStudio.GymClient.WebServices.Models
{
    [Serializable]
    public class TrainingPlan : CrudModelBase
    {
        public string Name;

        public string TrainingCategoryId;

        public string StartDate;

        public string EndDate;

        public string Place;

        public string Description;

        public List<string> AssignedInstructorIds;

        public List<string> ScheduledMemberIds;

        public List<string> AttendedInstructorIds;

        public List<string> AttendedMemberIds;
    }
}
