﻿// <copyright file="AdminUser.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/26/2019 15:01:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 26, 2019

using System;
using System.Collections.Generic;

namespace VuxtaStudio.GymClient.WebServices.Models
{
    /// <summary>
    /// Admin user CRUD model
    /// </summary>
    [Serializable]
    public class AdminUser : UserCrudModel
    {
        public List<string> ContactNumbers;
        public List<string> ContactEmails;
    }
}
