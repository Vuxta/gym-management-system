﻿using System;
using System.Collections.Generic;
namespace VuxtaStudio.GymClient.WebServices.Models
{
    /// <summary>
    /// Instructor CRUD model
    /// </summary>
    [Serializable]
    public class Instructor : UserCrudModel
    {
        public List<string> ContactNumbers;
        public List<string> ContactEmails;
    }
}
