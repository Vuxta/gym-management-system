﻿using System;
using System.Collections.Generic;

namespace VuxtaStudio.GymClient.WebServices.Models
{
    [Serializable]
    public class InstructorTrainingSchedule : CrudModelBase
    {
        public List<string> ScheduledTrainingPlanIds;

        public List<string> AttendedTrainingPlanIds;
    }
}
