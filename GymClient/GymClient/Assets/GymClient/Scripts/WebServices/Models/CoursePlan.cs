﻿using System;
using System.Collections.Generic;

namespace VuxtaStudio.GymClient.WebServices.Models
{
    [Serializable]
    public class CoursePlan : CrudModelBase
    {
        public string Name;

        public string CourseCategoryId;

        public string StartDate;

        public string EndDate;

        public string Place;

        public string Description;

        public List<string> TrainingPlanIds;
    }
}
