﻿// <copyright file="CrudModelBase.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/26/2019 15:01:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 26, 2019

using System;

namespace VuxtaStudio.GymClient.WebServices.Models
{
    /// <summary>
    /// Base class for CRUD model
    /// </summary>
    [Serializable]
    public abstract class CrudModelBase : JsonData
    {
        public string Id;
    }
}
