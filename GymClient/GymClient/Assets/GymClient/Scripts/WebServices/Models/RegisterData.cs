﻿// <copyright file="RegisterData.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/02/2019 10:19:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 2, 2019

namespace VuxtaStudio.GymClient.WebServices.Models
{
    /// <summary>
    /// Register data for register web service
    /// </summary>
    public class RegisterData : JsonData
    {
        public string UserId;
        public string Password;
        public string Group;
        public string[] Extras;
    }
}
