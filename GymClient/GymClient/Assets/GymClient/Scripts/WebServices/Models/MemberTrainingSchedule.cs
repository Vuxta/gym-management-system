﻿using System;
using System.Collections.Generic;

namespace VuxtaStudio.GymClient.WebServices.Models
{
    [Serializable]
    public class MemberTrainingSchedule : CrudModelBase
    {
        public List<string> CoursePlanIds;

        public List<string> ScheduledTrainingPlanIds;

        public List<string> AttendedTrainingPlanIds;
    }
}
