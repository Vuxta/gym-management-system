﻿using UnityEngine;
using System.IO;
using VuxtaStudio.Common;
using VuxtaStudio.GymClient.Events;
using VuxtaStudio.GymClient.Global.Defines;
using System.Text;
using System;

namespace VuxtaStudio.GymClient.WebServices.Services
{
    public class InstructorLoginService : LoginServiceBase<InstructorLoginService>
    {
        protected override string m_Route
        {
            get
            {
                return "/api/InstructorLogin";
            }
        }

        protected override string m_KeyFilePath
        {
            get
            {
                return FilePaths.InstructorApiKeyFile;
            }
        }

        //TODO: Use event to update session tokens and receive updated keys
        private void Start()
        {
            EventCenter.StartListenToEvent<InstructorApiKeyUpdateEvent>(OnApiKeyUpdated);
        }

        private void OnDestroy()
        {
            EventCenter.StopListenToEvent<InstructorApiKeyUpdateEvent>(OnApiKeyUpdated);
        }

        private void OnApiKeyUpdated(InstructorApiKeyUpdateEvent updateEvent)
        {
            if (File.Exists(m_KeyFilePath))
            {
                using (StreamReader streamReader = new StreamReader(m_KeyFilePath))
                {
                    m_LoginData.Key = streamReader.ReadLine();
                    Debug.LogFormat("Update key: {0}", m_LoginData.Key);

                    if (isLogin)
                    {
                        serviceHandler.SendRequest("POST",
                                (isSucess, message) => OnLoginFinish(isSucess, message, m_LoginData.UserId, null),
                                Encoding.UTF8.GetBytes(m_LoginData.SaveToJson()));
                    }
                }
            }
        }

        protected override void OnLogoutFinish()
        {
            base.OnLogoutFinish();
            EventCenter.TriggerEvent(new InstructorLogoutEvent());
        }

        protected override void OnLoginFinish(bool isSuccess, string message, string id, Action<bool, string, string> callback)
        {
            base.OnLoginFinish(isSuccess, message, id, callback);
            EventCenter.TriggerEvent(new InstructorSessionTokenUpdateEvent(isSuccess, m_SessionToken, id));
        }
    }
}
