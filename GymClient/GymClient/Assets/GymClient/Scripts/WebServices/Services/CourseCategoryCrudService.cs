﻿using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.WebServices.Services
{
    public class CourseCategoryCrudService : CrudServiceBase<CourseCategoryCrudService, CourseCategory>
    {
        protected override string m_Route
        {
            get
            {
                return "/api/CourseCategoryCrud";
            }
        }
    }
}
