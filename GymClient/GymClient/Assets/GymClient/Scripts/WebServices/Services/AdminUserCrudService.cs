﻿// <copyright file="AdminUserCrudService.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/26/2019 15:01:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 26, 2019

using UnityEngine;
using VuxtaStudio.Common;
using VuxtaStudio.GymClient.Events;
using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.WebServices.Services
{
    /// <summary>
    /// Admin user CRUD web service
    /// </summary>
    public class AdminUserCrudService : CrudServiceBase<AdminUserCrudService, AdminUser>
    {
        protected override string m_Route
        {
            get
            {
                return "/api/AdminUserCrud";
            }
        }
    }
}
