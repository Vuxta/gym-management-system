﻿using System;
using System.Text;
using UnityEngine;
using VuxtaStudio.Common;
using VuxtaStudio.Common.Utils;
using VuxtaStudio.GymClient.Events;
using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.WebServices.Services
{
    public class TrainingScheduleService : MonoSingleton<TrainingScheduleService>
    {
        private string m_Route { get { return "/api/TrainingSchedule"; } }

        private string m_SessionToken = string.Empty;
        private WebServiceHandler m_ServiceHandler;

        private void Awake()
        {
            m_ServiceHandler = new WebServiceHandler(this);
            m_ServiceHandler.SetRoute(m_Route);
            m_ServiceHandler.AddHeaders("Cache-Control", "no-cache");
            m_ServiceHandler.AddHeaders("Accept", "*/*");
            m_ServiceHandler.AddHeaders("Content-Type", "application/json");
        }

        private void Start()
        {
#if(GYMCLIENT_ADMIN_APP || GYMCLIENT_DEBUG_APP)
            EventCenter.StartListenToEvent<AdminSessionTokenUpdateEvent>(OnAdminSessionTokenUpdated);
            EventCenter.StartListenToEvent<AdminLogoutEvent>(OnAdminLogoutEvent);
#endif

#if(GYMCLIENT_MEMBER_APP || GYMCLIENT_DEBUG_APP)
            EventCenter.StartListenToEvent<MemberSessionTokenUpdateEvent>(OnMemberSessionTokenUpdated);
            EventCenter.StartListenToEvent<MemberLogoutEvent>(OnMemberLogoutEvent);
#endif

#if (GYMCLIENT_INSTRUCTOR_APP || GYMCLIENT_DEBUG_APP)
            EventCenter.StartListenToEvent<InstructorSessionTokenUpdateEvent>(OnInstructorSessionTokenUpdated);
            EventCenter.StartListenToEvent<InstructorLogoutEvent>(OnInstructorLogoutEvent);
#endif
        }

        private void OnDestroy()
        {
#if (GYMCLIENT_ADMIN_APP || GYMCLIENT_DEBUG_APP)
            EventCenter.StopListenToEvent<AdminSessionTokenUpdateEvent>(OnAdminSessionTokenUpdated);
            EventCenter.StopListenToEvent<AdminLogoutEvent>(OnAdminLogoutEvent);
#endif

#if (GYMCLIENT_MEMBER_APP || GYMCLIENT_DEBUG_APP)
            EventCenter.StopListenToEvent<MemberSessionTokenUpdateEvent>(OnMemberSessionTokenUpdated);
            EventCenter.StopListenToEvent<MemberLogoutEvent>(OnMemberLogoutEvent);
#endif

#if (GYMCLIENT_INSTRUCTOR_APP || GYMCLIENT_DEBUG_APP)
            EventCenter.StopListenToEvent<InstructorSessionTokenUpdateEvent>(OnInstructorSessionTokenUpdated);
            EventCenter.StopListenToEvent<InstructorLogoutEvent>(OnInstructorLogoutEvent);
#endif
        }

#if (GYMCLIENT_ADMIN_APP || GYMCLIENT_DEBUG_APP)
        private void OnAdminLogoutEvent(AdminLogoutEvent logoutEvent)
        {
            RemoveSessionToken();
        }

        private void OnAdminSessionTokenUpdated(AdminSessionTokenUpdateEvent tokenUpdateEvent)
        {
            if (tokenUpdateEvent.LoginSuccess)
            {
                Debug.LogFormat("{0} Update admin session token: {1}", typeof(TrainingScheduleService), tokenUpdateEvent.SessionToken);
                SetSessionToken(tokenUpdateEvent.SessionToken);
            }
        }
#endif

#if (GYMCLIENT_MEMBER_APP || GYMCLIENT_DEBUG_APP)
        private void OnMemberSessionTokenUpdated(MemberSessionTokenUpdateEvent tokenUpdateEvent)
        {
            if (tokenUpdateEvent.LoginSuccess)
            {
                Debug.LogFormat("{0} Update member session, token: {1}", typeof(TrainingScheduleService), tokenUpdateEvent.SessionToken);
                SetSessionToken(tokenUpdateEvent.SessionToken);
            }
        }

        private void OnMemberLogoutEvent(MemberLogoutEvent obj)
        {
            RemoveSessionToken();
        }
#endif

#if (GYMCLIENT_INSTRUCTOR_APP || GYMCLIENT_DEBUG_APP)
        private void OnInstructorSessionTokenUpdated(InstructorSessionTokenUpdateEvent tokenUpdateEvent)
        {
            if (tokenUpdateEvent.LoginSuccess)
            {
                Debug.LogFormat("{0} Update instructor session, token: {1}", typeof(TrainingScheduleService), tokenUpdateEvent.SessionToken);
                SetSessionToken(tokenUpdateEvent.SessionToken);
            }
        }

        private void OnInstructorLogoutEvent(InstructorLogoutEvent obj)
        {
            RemoveSessionToken();
        }
#endif

        /// <summary>
        /// Set session token for accessing the web service
        /// </summary>
        /// <param name="token">session token</param>
        private void SetSessionToken(string token)
        {
            m_SessionToken = token;
            m_ServiceHandler.AddHeaders("Authorization", string.Format("Bearer {0}", token));
        }

        /// <summary>
        /// Remove session tokens
        /// </summary>
        private void RemoveSessionToken()
        {
            m_ServiceHandler.RemoveHeaders("Authorization");
        }

        /// <summary>
        /// Called at start for creating instance
        /// </summary>
        public virtual void Setup()
        {
        }

        public void AddToMemberSchedule(string memberId, Action<bool, string> callback, params string[] trainingPlanIds)
        {
            UpdateSchedule(string.Format("MemberSchedule/{0}/AddSchedule", memberId),
                            callback, trainingPlanIds);
        }

        public void RemoveFromMemberSchedule(string memberId, Action<bool, string> callback, params string[] trainingPlanIds)
        {
            UpdateSchedule(string.Format("MemberSchedule/{0}/RemoveSchedule", memberId),
                            callback, trainingPlanIds);
        }

        public void AddToMemberAttended(string memberId, Action<bool, string> callback, params string[] trainingPlanIds)
        {
            UpdateSchedule(string.Format("MemberSchedule/{0}/AddAttended", memberId),
                            callback, trainingPlanIds);
        }

        public void RemoveFromMemberAttended(string memberId, Action<bool, string> callback, params string[] trainingPlanIds)
        {
            UpdateSchedule(string.Format("MemberSchedule/{0}/RemoveAttended", memberId),
                            callback, trainingPlanIds);
        }

        public void AddToInstructorSchedule(string instructorId, Action<bool, string> callback, params string[] trainingPlanIds)
        {
            UpdateSchedule(string.Format("InstructorSchedule/{0}/AddSchedule", instructorId),
                            callback, trainingPlanIds);
        }

        public void RemoveFromInstructorSchedule(string instructorId, Action<bool, string> callback, params string[] trainingPlanIds)
        {
            UpdateSchedule(string.Format("InstructorSchedule/{0}/RemoveSchedule", instructorId),
                            callback, trainingPlanIds);
        }

        public void AddToInstructorAttended(string instructorId, Action<bool, string> callback, params string[] trainingPlanIds)
        {
            UpdateSchedule(string.Format("InstructorSchedule/{0}/AddAttended", instructorId),
                            callback, trainingPlanIds);
        }

        public void RemoveFromInstructorAttended(string instructorId, Action<bool, string> callback, params string[] trainingPlanIds)
        {
            UpdateSchedule(string.Format("InstructorSchedule/{0}/RemoveAttended", instructorId),
                            callback, trainingPlanIds);
        }

        public void GetMemberScheduledPlans(string memberId, Action<bool, string, TrainingPlan[]> callback)
        {
            GetTrainingPlans(string.Format("MemberSchedule/{0}/ScheduledPlans", memberId), callback);
        }

        public void GetMemberAttendedPlans(string memberId, Action<bool, string, TrainingPlan[]> callback)
        {
            GetTrainingPlans(string.Format("MemberSchedule/{0}/AttendedPlans", memberId), callback);
        }

        public void GetInstructorScheduledPlans(string memberId, Action<bool, string, TrainingPlan[]> callback)
        {
            GetTrainingPlans(string.Format("InstructorSchedule/{0}/ScheduledPlans", memberId), callback);
        }

        public void GetInstructorAttendedPlans(string memberId, Action<bool, string, TrainingPlan[]> callback)
        {
            GetTrainingPlans(string.Format("InstructorSchedule/{0}/AttendedPlans", memberId), callback);
        }

        public void GetAllTrainingPlans(Action<bool, string, TrainingPlan[]> callback)
        {
            GetTrainingPlans("TrainingPlans", callback);
        }

        private void GetTrainingPlans(string route, Action<bool, string, TrainingPlan[]> callback)
        {
            m_ServiceHandler.SendRequest("GET",
                (isSuccess, message) => OnGetTrainingPlans(isSuccess, message, callback),
                null,
                route);
        }

        private void OnGetTrainingPlans(bool isSuccess, string message, Action<bool, string, TrainingPlan[]> callback)
        {
            if (!isSuccess)
            {
                Debug.LogWarningFormat("Get all data failed: {0}", message);
                callback?.Invoke(false, message, null);
                return;
            }

            TrainingPlan[] datas = JsonHelper.FromJson<TrainingPlan>(message);

            if (datas == null)
            {
                Debug.LogFormat("Get all data failed on parsing: {0}", message);
                callback?.Invoke(false, message, null);
                return;
            }

            Debug.LogFormat("Get all data success: {0}", message);
            callback?.Invoke(true, message, datas);
        }

        private void UpdateSchedule(string route, Action<bool, string> callback, params string[] trainingPlanIds)
        {
            string dataString = JsonHelper.ToJson<string>(trainingPlanIds);

            Debug.LogFormat("Upload data: {0}", dataString);

            m_ServiceHandler.SendRequest("PUT",
                (isSuccess, message) => OnUpdateData(isSuccess, message, callback),
                Encoding.UTF8.GetBytes(dataString),
                route);
        }

        private void OnUpdateData(bool isSuccess, string message, Action<bool, string> callback)
        {
            if (!isSuccess)
            {
                Debug.LogWarningFormat("Upload data failed: {0}", message);
                callback?.Invoke(false, message);
                return;
            }

            Debug.LogFormat("Upload data success: {0}", message);
            callback?.Invoke(true, message);
        }
    }
}
