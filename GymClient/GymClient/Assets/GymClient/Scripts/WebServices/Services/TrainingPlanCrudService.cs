﻿using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.WebServices.Services
{
    public class TrainingPlanCrudService : CrudServiceBase<TrainingPlanCrudService, TrainingPlan>
    {
        protected override string m_Route
        {
            get
            {
                return "/api/TrainingPlanCrud";
            }
        }
    }
}
