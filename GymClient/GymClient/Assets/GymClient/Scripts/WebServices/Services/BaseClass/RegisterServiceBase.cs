﻿// <copyright file="RegisterServiceBase.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/02/2019 10:19:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 2, 2019

using System;
using System.Text;
using UnityEngine;
using VuxtaStudio.Common;
using VuxtaStudio.GymClient.Events;
using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.WebServices.Services
{
    /// <summary>
    /// Base class for user register web service
    /// </summary>
    /// <typeparam name="T">Type of register service</typeparam>
    public abstract class RegisterServiceBase<T> : MonoSingleton<T> where T : MonoBehaviour
    {
        protected abstract string m_Route { get; }
        protected abstract string m_GroupName { get; }

        protected string m_SessionToken = string.Empty;
        protected WebServiceHandler m_ServiceHandler;

        private void Awake()
        {
            m_ServiceHandler = new WebServiceHandler(this);
            m_ServiceHandler.SetRoute(m_Route);
            m_ServiceHandler.AddHeaders("Cache-Control", "no-cache");
            m_ServiceHandler.AddHeaders("Accept", "*/*");
            m_ServiceHandler.AddHeaders("Content-Type", "application/json");
        }

        private void Start()
        {
            EventCenter.StartListenToEvent<AdminSessionTokenUpdateEvent>(OnSessionTokenUpdated);
            EventCenter.StartListenToEvent<AdminLogoutEvent>(OnLogoutEvent);
        }

        private void OnDestroy()
        {
            EventCenter.StopListenToEvent<AdminSessionTokenUpdateEvent>(OnSessionTokenUpdated);
            EventCenter.StopListenToEvent<AdminLogoutEvent>(OnLogoutEvent);
        }

        private void OnLogoutEvent(AdminLogoutEvent logoutEvent)
        {
            RemoveSessionToken();
        }

        private void OnSessionTokenUpdated(AdminSessionTokenUpdateEvent tokenUpdateEvent)
        {
            if (tokenUpdateEvent.LoginSuccess)
            {
                Debug.LogFormat("{0} Update session token: {1}", typeof(T), tokenUpdateEvent.SessionToken);
                SetSessionToken(tokenUpdateEvent.SessionToken);
            }
        }

        /// <summary>
        /// Set session token for accessing the web service
        /// </summary>
        /// <param name="token">session token</param>
        private void SetSessionToken(string token)
        {
            m_SessionToken = token;
            m_ServiceHandler.AddHeaders("Authorization", string.Format("Bearer {0}", token));
        }

        /// <summary>
        /// Remove session tokens
        /// </summary>
        private void RemoveSessionToken()
        {
            m_ServiceHandler.RemoveHeaders("Authorization");
        }

        /// <summary>
        /// Called at start for creating instance
        /// </summary>
        public virtual void Setup()
        {
        }

        /// <summary>
        /// Register with user id and password, return with API Keys if success
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <param name="callback">Callback when register complete</param>
        public void Register(string userId, string password, Action<bool, string> callback)
        {
            RegisterData registerData = new RegisterData();
            registerData.UserId = userId;
            registerData.Password = password;
            registerData.Group = m_GroupName;

            string dataString = registerData.SaveToJson();
            Debug.LogFormat("Register with data: {0}", dataString);

            m_ServiceHandler.SendRequest("POST",
                                        (isSuccess, message) => OnRegisterFinish(isSuccess, message, callback),
                                        Encoding.UTF8.GetBytes(dataString));
        }

        private void OnRegisterFinish(bool isSuccess, string message, Action<bool, string> callback)
        {
            if (!isSuccess)
            {
                Debug.LogWarningFormat("Register failed: {0}", message);
                callback?.Invoke(false, message);
                return;
            }

            Debug.LogFormat("Register success: {0}", message);
            callback?.Invoke(true, message);
        }
    }
}
