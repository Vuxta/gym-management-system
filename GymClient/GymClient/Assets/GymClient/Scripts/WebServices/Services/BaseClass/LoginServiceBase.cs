﻿// <copyright file="LoginServiceBase.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/24/2019 14:31:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 24, 2019

using System;
using System.IO;
using System.Text;
using UnityEngine;
using VuxtaStudio.Common;
using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.WebServices.Services
{
    /// <summary>
    /// Base class of login services
    /// </summary>
    /// <typeparam name="T">Type of login service</typeparam>
    public abstract class LoginServiceBase<T> : MonoSingleton<T> where T : MonoBehaviour
    {
        protected string m_SessionToken;

        protected abstract string m_Route { get; }
        protected abstract string m_KeyFilePath { get; }

        protected const float m_RefreshTokenPeriod = 3000;
        protected LoginData m_LoginData = new LoginData();
        protected WebServiceHandler serviceHandler;
        protected float m_RefreshTokenTime = float.MaxValue;
        protected bool isLogin = false;

        /// <summary>
        /// Setting up headers and reading API keys
        /// </summary>
        private void Awake()
        {
            serviceHandler = new WebServiceHandler(this);
            serviceHandler.SetRoute(m_Route);
            serviceHandler.AddHeaders("Cache-Control", "no-cache");
            serviceHandler.AddHeaders("Accept", "*/*");
            serviceHandler.AddHeaders("Content-Type", "application/json");

            if (File.Exists(m_KeyFilePath))
            {
                using (StreamReader streamReader = new StreamReader(m_KeyFilePath))
                {
                    m_LoginData.Key = streamReader.ReadLine();
                    Debug.LogFormat("Setup key: {0}", m_LoginData.Key);
                }
            }
        }

        /// <summary>
        /// Refresh token after certain period
        /// </summary>
        private void Update()
        {
            if (Time.realtimeSinceStartup > m_RefreshTokenTime)
            {
                serviceHandler.SendRequest("POST",
                                            (isSucess, message) => OnLoginFinish(isSucess, message, m_LoginData.UserId, null),
                                            Encoding.UTF8.GetBytes(m_LoginData.SaveToJson()));
            }
        }

        /// <summary>
        /// Called at start for creating instance
        /// </summary>
        public virtual void Setup()
        {
        }

        /// <summary>
        /// Set up API keys
        /// </summary>
        /// <param name="key"></param>
        public void SetKey(string key)
        {
            m_LoginData.Key = key;
        }

        /// <summary>
        /// Login by id and password
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <param name="password">password</param>
        /// <param name="callback">bacllback when login complete</param>
        public void Login(string userId, string password, Action<bool, string, string> callback = null)
        {
            m_LoginData.UserId = userId;
            m_LoginData.Password = password;

            string loginStrings = m_LoginData.SaveToJson();

            Debug.LogFormat("login data: {0}", loginStrings);

            serviceHandler.SendRequest("POST",
                                        (isSucess, message) => OnLoginFinish(isSucess, message, userId, callback),
                                        Encoding.UTF8.GetBytes(loginStrings));
        }

        /// <summary>
        /// Logout
        /// </summary>
        public void Logout()
        {
            m_SessionToken = string.Empty;
            m_RefreshTokenTime = float.MaxValue;
            OnLogoutFinish();
        }

        protected virtual void OnLogoutFinish()
        {
        }

        /// <summary>
        /// Called when finish login request
        /// </summary>
        /// <param name="isSuccess">True if login success</param>
        /// <param name="message">Message/data received</param>
        /// <param name="callback">Callback to call when login request finished</param>
        protected virtual void OnLoginFinish(bool isSuccess, string message, string id, Action<bool, string, string> callback)
        {
            if (!isSuccess)
            {
                Debug.LogWarningFormat("login failed: {0}", message);
                isLogin = false;
                m_RefreshTokenTime = float.MaxValue;
                m_SessionToken = string.Empty;
                callback?.Invoke(false, message, id);
                return;
            }

            Debug.LogFormat("login success: {0}", message);
            isLogin = true;
            m_RefreshTokenTime = Time.realtimeSinceStartup + m_RefreshTokenPeriod;
            m_SessionToken = message;
            callback?.Invoke(true, message, id);
        }
    }
}
