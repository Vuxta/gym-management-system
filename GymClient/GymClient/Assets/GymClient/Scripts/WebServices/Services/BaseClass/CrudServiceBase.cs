﻿// <copyright file="CrudServiceBase.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/26/2019 15:01:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 26, 2019

using System;
using System.Text;
using UnityEngine;
using VuxtaStudio.Common;
using VuxtaStudio.Common.Utils;
using VuxtaStudio.GymClient.Events;
using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.WebServices.Services
{
    /// <summary>
    /// Base class for handling CRUD web services
    /// </summary>
    /// <typeparam name="T">Type of web service</typeparam>
    /// <typeparam name="U">Type of model</typeparam>
    public abstract class CrudServiceBase<T, U> : MonoSingleton<T> where T : MonoBehaviour where U : CrudModelBase
    {
        protected abstract string m_Route { get; }

        protected string m_SessionToken = string.Empty;
        protected WebServiceHandler m_ServiceHandler;

        private void Awake()
        {
            m_ServiceHandler = new WebServiceHandler(this);
            m_ServiceHandler.SetRoute(m_Route);
            m_ServiceHandler.AddHeaders("Cache-Control", "no-cache");
            m_ServiceHandler.AddHeaders("Accept", "*/*");
            m_ServiceHandler.AddHeaders("Content-Type", "application/json");
        }

        private void Start()
        {
            EventCenter.StartListenToEvent<AdminSessionTokenUpdateEvent>(OnSessionTokenUpdated);
            EventCenter.StartListenToEvent<AdminLogoutEvent>(OnLogoutEvent);
        }

        private void OnDestroy()
        {
            EventCenter.StopListenToEvent<AdminSessionTokenUpdateEvent>(OnSessionTokenUpdated);
            EventCenter.StopListenToEvent<AdminLogoutEvent>(OnLogoutEvent);
        }

        private void OnLogoutEvent(AdminLogoutEvent logoutEvent)
        {
            RemoveSessionToken();
        }

        private void OnSessionTokenUpdated(AdminSessionTokenUpdateEvent tokenUpdateEvent)
        {
            if (tokenUpdateEvent.LoginSuccess)
            {
                Debug.LogFormat("{0} Update session token: {1}", typeof(T), tokenUpdateEvent.SessionToken);
                SetSessionToken(tokenUpdateEvent.SessionToken);
            }
        }

        /// <summary>
        /// Set session token for accessing the web service
        /// </summary>
        /// <param name="token">session token</param>
        private void SetSessionToken(string token)
        {
            m_SessionToken = token;
            m_ServiceHandler.AddHeaders("Authorization", string.Format("Bearer {0}", token));
        }

        /// <summary>
        /// Remove session tokens
        /// </summary>
        private void RemoveSessionToken()
        {
            m_ServiceHandler.RemoveHeaders("Authorization");
        }

        /// <summary>
        /// Called at start for creating instance
        /// </summary>
        public virtual void Setup()
        {
        }

        /// <summary>
        /// Request for getting all datas
        /// </summary>
        /// <param name="callback">Callback when request complete</param>
        public void GetAllData(Action<bool, string, U[]> callback)
        {
            m_ServiceHandler.SendRequest("GET",
                            (isSuccess, message) => OnGetAllDataFinish(isSuccess, message, callback));
        }

        /// <summary>
        /// Request data by ID
        /// </summary>
        /// <param name="id">ID of model</param>
        /// <param name="callback">Callback when request complete</param>
        public void GetData(string id, Action<bool, string, U> callback)
        {
            m_ServiceHandler.SendRequest("GET",
                            (isSuccess, message) => OnGetDataFinish(isSuccess, message, callback),
                            null,
                            id);
        }

        /// <summary>
        /// Create data
        /// </summary>
        /// <param name="item">Item to create</param>
        /// <param name="callback">callback when complete</param>
        public void CreateData(U item, Action<bool, string, U> callback)
        {
            string dataString = item.SaveToJson();
            Debug.LogFormat("Create data: {0}", dataString);

            m_ServiceHandler.SendRequest("POST",
                (isSuccess, message) => OnCreateDataFinish(isSuccess, message, callback),
                Encoding.UTF8.GetBytes(dataString));
        }

        /// <summary>
        /// Update data by ID
        /// </summary>
        /// <param name="id">ID of model</param>
        /// <param name="item">model to update</param>
        /// <param name="callback">complete callback</param>
        public void UpdateData(string id, U item, Action<bool, string> callback)
        {
            string dataString = item.SaveToJson();
            Debug.LogFormat("Update data: {0}", dataString);

            m_ServiceHandler.SendRequest("PUT",
                (isSuccess, message) => OnUpdateDataFinish(isSuccess, message, callback),
                Encoding.UTF8.GetBytes(dataString),
                id);
        }

        /// <summary>
        /// Delete data by ID
        /// </summary>
        /// <param name="id">ID of model</param>
        /// <param name="callback">complete callback</param>
        public void RemoveData(string id, Action<bool, string> callback)
        {
            m_ServiceHandler.SendRequest("DELETE",
                (isSuccess, message) => OnDeleteDataFinish(isSuccess, message, callback),
                null,
                id);
        }

        protected virtual void OnDeleteDataFinish(bool isSuccess, string message, Action<bool, string> callback)
        {
            if (!isSuccess)
            {
                Debug.LogWarningFormat("Delete data failed: {0}", message);
                callback?.Invoke(false, message);
                return;
            }

            Debug.LogFormat("Delete data success: {0}", message);
            callback?.Invoke(true, message);
        }

        protected virtual void OnUpdateDataFinish(bool isSuccess, string message, Action<bool, string> callback)
        {
            if (!isSuccess)
            {
                Debug.LogWarningFormat("Update data failed: {0}", message);
                callback?.Invoke(false, message);
                return;
            }


            Debug.LogFormat("Update data success: {0}", message);
            callback?.Invoke(true, message);
        }

        protected virtual void OnCreateDataFinish(bool isSuccess, string message, Action<bool, string, U> callback)
        {
            if (!isSuccess)
            {
                Debug.LogWarningFormat("Create data failed: {0}", message);
                callback?.Invoke(false, message, null);
                return;
            }

            U data = JsonUtility.FromJson<U>(message);

            if (data == null)
            {
                Debug.LogFormat("Create data failed on parsing: {0}", message);
                callback?.Invoke(false, message, null);
                return;
            }

            Debug.LogFormat("Create data success: {0}", message);
            callback?.Invoke(true, message, data);
        }

        protected virtual void OnGetDataFinish(bool isSuccess, string message, Action<bool, string, U> callback)
        {
            if (!isSuccess)
            {
                Debug.LogWarningFormat("Get data failed: {0}", message);
                callback?.Invoke(false, message, null);
                return;
            }

            U data = JsonUtility.FromJson<U>(message);

            if (data == null)
            {
                Debug.LogFormat("Get data failed on parsing: {0}", message);
                callback?.Invoke(false, message, null);
                return;
            }

            Debug.LogFormat("Get data success: {0}", message);
            callback?.Invoke(true, message, data);
        }

        protected virtual void OnGetAllDataFinish(bool isSuccess, string message, Action<bool, string, U[]> callback)
        {
            if (!isSuccess)
            {
                Debug.LogWarningFormat("Get all data failed: {0}", message);
                callback?.Invoke(false, message, null);
                return;
            }

            U[] datas = JsonHelper.FromJson<U>(message);

            if (datas == null)
            {
                Debug.LogFormat("Get all data failed on parsing: {0}", message);
                callback?.Invoke(false, message, null);
                return;
            }

            Debug.LogFormat("Get all data success: {0}", message);
            callback?.Invoke(true, message, datas);
        }
    }
}
