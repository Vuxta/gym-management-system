﻿// <copyright file="AdminLoginService.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/24/2019 14:31:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 24, 2019

using UnityEngine;
using System.IO;
using VuxtaStudio.Common;
using VuxtaStudio.GymClient.Events;
using VuxtaStudio.GymClient.Global.Defines;
using System.Text;
using System;

namespace VuxtaStudio.GymClient.WebServices.Services
{
    public class AdminLoginService : LoginServiceBase<AdminLoginService>
    {
        protected override string m_Route
        {
            get
            {
                return "/api/AdminUserLogin";
            }
        }

        protected override string m_KeyFilePath
        {
            get
            {
                return FilePaths.AdminApiKeyFile;
            }
        }

        //TODO: Use event to update session tokens and receive updated keys
        private void Start()
        {
            EventCenter.StartListenToEvent<AdminApiKeyUpdateEvent>(OnApiKeyUpdated);
        }

        private void OnDestroy()
        {
            EventCenter.StopListenToEvent<AdminApiKeyUpdateEvent>(OnApiKeyUpdated);
        }

        private void OnApiKeyUpdated(AdminApiKeyUpdateEvent updateEvent)
        {
            if (File.Exists(m_KeyFilePath))
            {
                using (StreamReader streamReader = new StreamReader(m_KeyFilePath))
                {
                    m_LoginData.Key = streamReader.ReadLine();
                    Debug.LogFormat("Update key: {0}", m_LoginData.Key);

                    if (isLogin)
                    {
                        serviceHandler.SendRequest("POST",
                                (isSucess, message) => OnLoginFinish(isSucess, message, m_LoginData.UserId, null),
                                Encoding.UTF8.GetBytes(m_LoginData.SaveToJson()));
                    }
                }
            }
        }

        protected override void OnLogoutFinish()
        {
            base.OnLogoutFinish();
            EventCenter.TriggerEvent(new AdminLogoutEvent());
        }

        protected override void OnLoginFinish(bool isSuccess, string message, string id, Action<bool, string, string> callback)
        {
            base.OnLoginFinish(isSuccess, message, id, callback);
            EventCenter.TriggerEvent(new AdminSessionTokenUpdateEvent(isSuccess, m_SessionToken, id));
        }
    }
}
