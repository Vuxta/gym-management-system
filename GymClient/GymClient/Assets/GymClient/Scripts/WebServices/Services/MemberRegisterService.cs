﻿// <copyright file="MemberRegisterService.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/02/2019 10:19:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 2, 2019

using UnityEngine;
using VuxtaStudio.Common;
using VuxtaStudio.GymClient.Events;
using VuxtaStudio.GymClient.Global.Defines;

namespace VuxtaStudio.GymClient.WebServices.Services
{
    /// <summary>
    /// Member register web service
    /// </summary>
    public class MemberRegisterService : RegisterServiceBase<MemberRegisterService>
    {
        protected override string m_Route
        {
            get
            {
                return "/api/MemberRegister";
            }
        }

        protected override string m_GroupName
        {
            get
            {
                return GroupName.Member;
            }
        }
    }
}
