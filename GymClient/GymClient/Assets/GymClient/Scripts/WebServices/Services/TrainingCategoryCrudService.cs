﻿using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.WebServices.Services
{
    public class TrainingCategoryCrudService : CrudServiceBase<TrainingCategoryCrudService, TrainingCategory>
    {
        protected override string m_Route
        {
            get
            {
                return "/api/TrainingCategoryCrud";
            }
        }
    }
}
