﻿// <copyright file="InstructorCrudService.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/01/2019 11:01:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 1, 2019

using UnityEngine;
using VuxtaStudio.Common;
using VuxtaStudio.GymClient.Events;
using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.WebServices.Services
{
    /// <summary>
    /// Instructor CRUD web service
    /// </summary>
    public class InstructorCrudService : CrudServiceBase<InstructorCrudService, Instructor>
    {
        protected override string m_Route
        {
            get
            {
                return "/api/InstructorCrud";
            }
        }
    }
}
