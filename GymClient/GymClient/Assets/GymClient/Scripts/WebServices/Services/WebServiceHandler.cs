﻿// <copyright file="WebServiceHandler.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/24/2019 14:31:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 24, 2019

using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine;
using System.Text;

namespace VuxtaStudio.GymClient.WebServices.Services
{
    /// <summary>
    /// Handler class for communicating with web services
    /// </summary>
    public class WebServiceHandler
    {
        //private string m_HostAddress = "https://localhost:5001";
        private string m_HostAddress = "https://ka7bl62np9.execute-api.ap-southeast-1.amazonaws.com/Prod";
        private string m_Route = "/api/";
        private Dictionary<string, string> m_RequestHeaders = new Dictionary<string, string>();
        private MonoBehaviour m_MonoBehaviour;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="monoBehaviour">Mono behaviour that runs for async request</param>
        public WebServiceHandler(MonoBehaviour monoBehaviour)
        {
            m_MonoBehaviour = monoBehaviour;
        }

        /// <summary>
        /// Setup host URLs
        /// </summary>
        /// <param name="host">host address</param>
        public void SetHost(string host)
        {
            m_HostAddress = host;
        }

        /// <summary>
        /// Route to API
        /// </summary>
        /// <param name="route">API routes</param>
        public void SetRoute(string route)
        {
            m_Route = route;
        }

        /// <summary>
        /// Headers to add in request
        /// </summary>
        /// <param name="key">Header key</param>
        /// <param name="value">Header value</param>
        public void AddHeaders(string key, string value)
        {
            if (m_RequestHeaders.ContainsKey(key))
            {
                m_RequestHeaders[key] = value;
                return;
            }

            m_RequestHeaders.Add(key, value);
        }

        /// <summary>
        /// Header to remove
        /// </summary>
        /// <param name="key">Header key</param>
        public void RemoveHeaders(string key)
        {
            m_RequestHeaders.Remove(key);
        }

        /// <summary>
        /// Request to send
        /// </summary>
        /// <param name="method">HTTP method such as POST, DELETE, PUT, GET</param>
        /// <param name="callback">callback when request finished</param>
        /// <param name="data">data to added in request body</param>
        public void SendRequest(string method, Action<bool, string> callback = null, byte[] data = null, string subRoute = "")
        {
            string url = m_HostAddress + m_Route;

            if (!string.IsNullOrEmpty(subRoute))
            {
                url += "/" + subRoute;
            }

            Debug.LogFormat("Send web request to {0}", url);
            UnityWebRequest webRequest = new UnityWebRequest(url, method);

            webRequest.certificateHandler = new BypassCertificate();

            if (data != null)
            {
                Debug.LogFormat("Encoded data: {0}", Encoding.UTF8.GetString(data));
                Debug.LogFormat("Adding datas: {0} bytes", data.Length);
                UploadHandler uploadHandler = new UploadHandlerRaw(data);
                //uploadHandler.contentType = "application/json";
                webRequest.uploadHandler = uploadHandler;
            }

            webRequest.downloadHandler = new DownloadHandlerBuffer();
            SetupRequestHeaders(ref webRequest);

            m_MonoBehaviour.StartCoroutine(SendRequestAsync(webRequest, callback));
        }

        /// <summary>
        /// Send request async by coroutine
        /// </summary>
        /// <param name="webRequest">Web request</param>
        /// <param name="responseCallback">Callback when request finished</param>
        /// <returns></returns>
        private IEnumerator SendRequestAsync(UnityWebRequest webRequest, Action<bool, string> responseCallback = null)
        {
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError || webRequest.isHttpError)
            {
                Debug.LogFormat("{0}, code: {1}, message: {2}", webRequest.error, webRequest.responseCode, webRequest.downloadHandler.text);
                responseCallback?.Invoke(false, string.Format("{0}, code: {1}, message: {2}", webRequest.error, webRequest.responseCode, webRequest.downloadHandler.text));
            }
            else
            {
                Debug.Log(webRequest.downloadHandler.text);
                responseCallback?.Invoke(true, webRequest.downloadHandler.text);
            }
        }

        /// <summary>
        /// Setting up request headers
        /// </summary>
        /// <param name="webRequest">current web request</param>
        private void SetupRequestHeaders(ref UnityWebRequest webRequest)
        {
            if (m_RequestHeaders == null || m_RequestHeaders.Count <= 0)
                return;

            foreach (KeyValuePair<string, string> entry in m_RequestHeaders)
            {
                Debug.LogFormat("Set headers: {0} / {1}", entry.Key, entry.Value);
                webRequest.SetRequestHeader(entry.Key, entry.Value);
            }
        }
    }
}
