﻿// <copyright file="MemberLoginService.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/03/2019 16:51:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 3, 2019

using UnityEngine;
using System.IO;
using VuxtaStudio.Common;
using VuxtaStudio.GymClient.Events;
using VuxtaStudio.GymClient.Global.Defines;
using System.Text;
using System;

namespace VuxtaStudio.GymClient.WebServices.Services
{
    public class MemberLoginService : LoginServiceBase<MemberLoginService>
    {
        protected override string m_Route
        {
            get
            {
                return "/api/MemberLogin";
            }
        }

        protected override string m_KeyFilePath
        {
            get
            {
                return FilePaths.MemberApiKeyFile;
            }
        }

        //TODO: Use event to update session tokens and receive updated keys
        private void Start()
        {
            EventCenter.StartListenToEvent<MemberApiKeyUpdateEvent>(OnApiKeyUpdated);
        }

        private void OnDestroy()
        {
            EventCenter.StopListenToEvent<MemberApiKeyUpdateEvent>(OnApiKeyUpdated);
        }

        private void OnApiKeyUpdated(MemberApiKeyUpdateEvent updateEvent)
        {
            if (File.Exists(m_KeyFilePath))
            {
                using (StreamReader streamReader = new StreamReader(m_KeyFilePath))
                {
                    m_LoginData.Key = streamReader.ReadLine();
                    Debug.LogFormat("Update key: {0}", m_LoginData.Key);

                    if (isLogin)
                    {
                        serviceHandler.SendRequest("POST",
                                (isSucess, message) => OnLoginFinish(isSucess, message, m_LoginData.UserId, null),
                                Encoding.UTF8.GetBytes(m_LoginData.SaveToJson()));
                    }
                }
            }
        }

        protected override void OnLogoutFinish()
        {
            base.OnLogoutFinish();
            EventCenter.TriggerEvent(new MemberLogoutEvent());
        }

        protected override void OnLoginFinish(bool isSuccess, string message, string id, Action<bool, string, string> callback)
        {
            base.OnLoginFinish(isSuccess, message, id, callback);
            EventCenter.TriggerEvent(new MemberSessionTokenUpdateEvent(isSuccess, m_SessionToken, id));
        }
    }
}
