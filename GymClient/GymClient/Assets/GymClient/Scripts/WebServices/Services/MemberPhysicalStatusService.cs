﻿// <copyright file="MemberPhysicalStatusService.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/08/2019 16:00:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 8, 2019

using System;
using System.Text;
using UnityEngine;
using VuxtaStudio.Common;
using VuxtaStudio.Common.Utils;
using VuxtaStudio.GymClient.Events;
using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.WebServices.Services
{
    /// <summary>
    /// Member physical status web service
    /// </summary>
    public class MemberPhysicalStatusService : MonoSingleton<MemberPhysicalStatusService>
    {
        private string m_Route { get { return "/api/MemberPhysicalStatus"; } }

        private string m_SessionToken = string.Empty;
        private WebServiceHandler m_ServiceHandler;

        private void Awake()
        {
            m_ServiceHandler = new WebServiceHandler(this);
            m_ServiceHandler.SetRoute(m_Route);
            m_ServiceHandler.AddHeaders("Cache-Control", "no-cache");
            m_ServiceHandler.AddHeaders("Accept", "*/*");
            m_ServiceHandler.AddHeaders("Content-Type", "application/json");
        }

        private void Start()
        {
#if(GYMCLIENT_ADMIN_APP || GYMCLIENT_DEBUG_APP)
            EventCenter.StartListenToEvent<AdminSessionTokenUpdateEvent>(OnAdminSessionTokenUpdated);
            EventCenter.StartListenToEvent<AdminLogoutEvent>(OnAdminLogoutEvent);
#endif

#if(GYMCLIENT_MEMBER_APP || GYMCLIENT_DEBUG_APP)
            EventCenter.StartListenToEvent<MemberSessionTokenUpdateEvent>(OnMemberSessionTokenUpdated);
            EventCenter.StartListenToEvent<MemberLogoutEvent>(OnMemberLogoutEvent);
#endif
        }

        private void OnDestroy()
        {
#if (GYMCLIENT_ADMIN_APP || GYMCLIENT_DEBUG_APP)
            EventCenter.StopListenToEvent<AdminSessionTokenUpdateEvent>(OnAdminSessionTokenUpdated);
            EventCenter.StopListenToEvent<AdminLogoutEvent>(OnAdminLogoutEvent);
#endif

#if (GYMCLIENT_MEMBER_APP || GYMCLIENT_DEBUG_APP)
            EventCenter.StopListenToEvent<MemberSessionTokenUpdateEvent>(OnMemberSessionTokenUpdated);
            EventCenter.StopListenToEvent<MemberLogoutEvent>(OnMemberLogoutEvent);
#endif
        }

#if (GYMCLIENT_ADMIN_APP || GYMCLIENT_DEBUG_APP)
        private void OnAdminLogoutEvent(AdminLogoutEvent logoutEvent)
        {
            RemoveSessionToken();
        }

        private void OnAdminSessionTokenUpdated(AdminSessionTokenUpdateEvent tokenUpdateEvent)
        {
            if (tokenUpdateEvent.LoginSuccess)
            {
                Debug.LogFormat("{0} Update admin session token: {1}", typeof(MemberPhysicalStatusService), tokenUpdateEvent.SessionToken);
                SetSessionToken(tokenUpdateEvent.SessionToken);
            }
        }
#endif

#if (GYMCLIENT_MEMBER_APP || GYMCLIENT_DEBUG_APP)
        private void OnMemberSessionTokenUpdated(MemberSessionTokenUpdateEvent tokenUpdateEvent)
        {
            if (tokenUpdateEvent.LoginSuccess)
            {
                Debug.LogFormat("{0} Update member session, token: {1}", typeof(MemberPhysicalStatusService), tokenUpdateEvent.SessionToken);
                SetSessionToken(tokenUpdateEvent.SessionToken);
            }
        }

        private void OnMemberLogoutEvent(MemberLogoutEvent obj)
        {
            RemoveSessionToken();
        }
#endif

        /// <summary>
        /// Set session token for accessing the web service
        /// </summary>
        /// <param name="token">session token</param>
        private void SetSessionToken(string token)
        {
            m_SessionToken = token;
            m_ServiceHandler.AddHeaders("Authorization", string.Format("Bearer {0}", token));
        }

        /// <summary>
        /// Remove session tokens
        /// </summary>
        private void RemoveSessionToken()
        {
            m_ServiceHandler.RemoveHeaders("Authorization");
        }

        /// <summary>
        /// Called at start for creating instance
        /// </summary>
        public virtual void Setup()
        {
        }

        /// <summary>
        /// Upload physical infos to specific member's status
        /// </summary>
        /// <param name="id">Member ID</param>
        /// <param name="callback">Upload finish callback</param>
        /// <param name="items">infos</param>
        public void UploadInfo(string id, Action<bool, string, PhysicalStatus> callback, params PhysicalInfo[] items)
        {
            string dataString = JsonHelper.ToJson<PhysicalInfo>(items);

            Debug.LogFormat("Upload data: {0}", dataString);

            m_ServiceHandler.SendRequest("PUT",
                (isSuccess, message) => OnUploadDataFinish(isSuccess, message, callback),
                Encoding.UTF8.GetBytes(dataString),
                id);
        }

        private void OnUploadDataFinish(bool isSuccess, string message, Action<bool, string, PhysicalStatus> callback)
        {
            if (!isSuccess)
            {
                Debug.LogWarningFormat("Upload data failed: {0}", message);
                callback?.Invoke(false, message, null);
                return;
            }

            PhysicalStatus data = JsonUtility.FromJson<PhysicalStatus>(message);

            if (data == null)
            {
                Debug.LogFormat("Get response data failed on parsing: {0}", message);
                callback?.Invoke(false, message, null);
                return;
            }

            Debug.LogFormat("Upload data success: {0}", message);
            callback?.Invoke(true, message, data);
        }

        /// <summary>
        /// Request data by ID
        /// </summary>
        /// <param name="id">ID of model</param>
        /// <param name="callback">Callback when request complete</param>
        public void GetMemberStatus(string id, Action<bool, string, PhysicalStatus> callback)
        {
            m_ServiceHandler.SendRequest("GET",
                            (isSuccess, message) => OnGetDataFinish(isSuccess, message, callback),
                            null,
                            id);
        }

        private void OnGetDataFinish(bool isSuccess, string message, Action<bool, string, PhysicalStatus> callback)
        {
            if (!isSuccess)
            {
                Debug.LogWarningFormat("Get data failed: {0}", message);
                callback?.Invoke(false, message, null);
                return;
            }

            PhysicalStatus data = JsonUtility.FromJson<PhysicalStatus>(message);

            if (data == null)
            {
                Debug.LogFormat("Get data failed on parsing: {0}", message);
                callback?.Invoke(false, message, null);
                return;
            }

            Debug.LogFormat("Get data success: {0}", message);
            callback?.Invoke(true, message, data);
        }
    }
}
