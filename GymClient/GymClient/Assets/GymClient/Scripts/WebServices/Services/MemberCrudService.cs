﻿// <copyright file="MemberCrudService.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/28/2019 17:56:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 28, 2019

using UnityEngine;
using VuxtaStudio.Common;
using VuxtaStudio.GymClient.Events;
using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.WebServices.Services
{
    public class MemberCrudService : CrudServiceBase<MemberCrudService, Member>
    {
        protected override string m_Route
        {
            get
            {
                return "/api/MemberCrud";
            }
        }
    }
}
