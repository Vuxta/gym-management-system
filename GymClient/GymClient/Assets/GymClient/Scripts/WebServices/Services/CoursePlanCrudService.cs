﻿using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.WebServices.Services
{
    public class CoursePlanCrudService : CrudServiceBase<CoursePlanCrudService, CoursePlan>
    {
        protected override string m_Route
        {
            get
            {
                return "/api/CoursePlanCrud";
            }
        }
    }
}
