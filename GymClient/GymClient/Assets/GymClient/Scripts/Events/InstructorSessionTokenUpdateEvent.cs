﻿namespace VuxtaStudio.GymClient.Events
{
    public class InstructorSessionTokenUpdateEvent
    {
        public readonly bool LoginSuccess;
        public readonly string SessionToken;
        public readonly string LoginID;

        public InstructorSessionTokenUpdateEvent(bool isSuccess, string token, string id)
        {
            LoginSuccess = isSuccess;
            SessionToken = token;
            LoginID = id;
        }
    }
}
