﻿// <copyright file="AdminSessionTokenUpdateEvent.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/24/2019 14:31:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 24, 2019

namespace VuxtaStudio.GymClient.Events
{
    public class AdminSessionTokenUpdateEvent
    {
        public readonly bool LoginSuccess;
        public readonly string SessionToken;
        public readonly string LoginID;

        public AdminSessionTokenUpdateEvent(bool isSuccess, string token, string id)
        {
            LoginSuccess = isSuccess;
            SessionToken = token;
            LoginID = id;
        }
    }
}
