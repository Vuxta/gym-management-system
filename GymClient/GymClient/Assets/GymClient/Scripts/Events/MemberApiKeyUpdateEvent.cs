﻿// <copyright file="MemberApiKeyUpdateEvent.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/03/2019 16:51:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 3, 2019

namespace VuxtaStudio.GymClient.Events
{
    public class MemberApiKeyUpdateEvent
    {
        public MemberApiKeyUpdateEvent()
        {
        }
    }
}
