﻿// <copyright file="MemberSessionTokenUpdateEvent.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/03/2019 16:51:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 3, 2019

namespace VuxtaStudio.GymClient.Events
{
    public class MemberSessionTokenUpdateEvent
    {
        public readonly bool LoginSuccess;
        public readonly string SessionToken;
        public readonly string LoginID;

        public MemberSessionTokenUpdateEvent(bool isSuccess, string token, string id)
        {
            LoginSuccess = isSuccess;
            SessionToken = token;
            LoginID = id;
        }
    }
}