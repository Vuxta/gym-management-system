﻿// <copyright file="InstructorRoles.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/28/2019 17:56:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 28, 2019

namespace VuxtaStudio.GymClient.Global.Defines
{
    public static class InstructorRoles
    {
        public const long Normal = 1 << 0;
    }
}
