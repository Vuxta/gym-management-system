﻿// <copyright file="GroupName.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/28/2019 17:56:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 28, 2019

namespace VuxtaStudio.GymClient.Global.Defines
{
    /// <summary>
    /// Group name defines
    /// </summary>
    public static class GroupName
    {
        public const string Admin = "Admin";
        public const string Member = "Member";
        public const string Instructor = "Instructor";
    }
}
