﻿// <copyright file="AdminRoles.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/26/2019 15:01:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 26, 2019

namespace VuxtaStudio.GymClient.Global.Defines
{
    /// <summary>
    /// Define roles in admin, should be same as backend
    /// </summary>
    public static class AdminRoles
    {
        public const long SuperUser = 1 << 0;
        public const long AdminManager = 1 << 1;
        public const long InstructorManager = 1 << 2;
        public const long MemberManager = 1 << 3;
    }
}
