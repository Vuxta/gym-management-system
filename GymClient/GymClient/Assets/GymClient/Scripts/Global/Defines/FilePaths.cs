﻿// <copyright file="FilePaths.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/24/2019 14:31:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 24, 2019

using System.IO;
using UnityEngine;

namespace VuxtaStudio.GymClient.Global.Defines
{
    /// <summary>
    /// Defines for all file IO paths
    /// </summary>
    public static class FilePaths
    {
        public static string TokenDirectory = Path.Combine(Application.dataPath, "key");
        public static string AdminApiKeyFile = Path.Combine(TokenDirectory, "admin.key");
        public static string InstructorApiKeyFile = Path.Combine(TokenDirectory, "instructor.key");
        public static string MemberApiKeyFile = Path.Combine(TokenDirectory, "member.key");
    }
}
