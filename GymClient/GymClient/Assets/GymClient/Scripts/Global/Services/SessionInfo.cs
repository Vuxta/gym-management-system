﻿// <copyright file="SessionInfo.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/08/2019 16:00:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 8, 2019

using UnityEngine;
using VuxtaStudio.Common;
using VuxtaStudio.GymClient.Events;
using VuxtaStudio.GymClient.Global.Defines;

namespace VuxtaStudio.GymClient.Global.Services
{
    /// <summary>
    /// A instance that saves login session infos
    /// </summary>
    public class SessionInfo : MonoSingleton<SessionInfo>
    {
        public bool Login { get; private set; } = false;
        public string ID { get; private set; } = string.Empty;
        public string Group { get; private set; } = string.Empty;

        public void Setup()
        {
        }

        private void Start()
        {
#if(GYMCLIENT_ADMIN_APP || GYMCLIENT_DEBUG_APP)
            EventCenter.StartListenToEvent<AdminSessionTokenUpdateEvent>(OnAdminSessionTokenUpdated);
            EventCenter.StartListenToEvent<AdminLogoutEvent>(OnAdminLogoutEvent);
#endif

#if(GYMCLIENT_MEMBER_APP || GYMCLIENT_DEBUG_APP)
            EventCenter.StartListenToEvent<MemberSessionTokenUpdateEvent>(OnMemberSessionTokenUpdated);
            EventCenter.StartListenToEvent<MemberLogoutEvent>(OnMemberLogoutEvent);
#endif

#if (GYMCLIENT_INSTRUCTOR_APP || GYMCLIENT_DEBUG_APP)
            EventCenter.StartListenToEvent<InstructorSessionTokenUpdateEvent>(OnInstructorSessionTokenUpdated);
            EventCenter.StartListenToEvent<InstructorLogoutEvent>(OnInstructorLogoutEvent);
#endif
        }

        private void OnDestroy()
        {
#if (GYMCLIENT_ADMIN_APP || GYMCLIENT_DEBUG_APP)
            EventCenter.StopListenToEvent<AdminSessionTokenUpdateEvent>(OnAdminSessionTokenUpdated);
            EventCenter.StopListenToEvent<AdminLogoutEvent>(OnAdminLogoutEvent);
#endif

#if (GYMCLIENT_MEMBER_APP || GYMCLIENT_DEBUG_APP)
            EventCenter.StopListenToEvent<MemberSessionTokenUpdateEvent>(OnMemberSessionTokenUpdated);
            EventCenter.StopListenToEvent<MemberLogoutEvent>(OnMemberLogoutEvent);
#endif

#if (GYMCLIENT_INSTRUCTOR_APP || GYMCLIENT_DEBUG_APP)
            EventCenter.StopListenToEvent<InstructorSessionTokenUpdateEvent>(OnInstructorSessionTokenUpdated);
            EventCenter.StopListenToEvent<InstructorLogoutEvent>(OnInstructorLogoutEvent);
#endif
        }

#if (GYMCLIENT_ADMIN_APP || GYMCLIENT_DEBUG_APP)
        private void OnAdminLogoutEvent(AdminLogoutEvent logoutEvent)
        {
            Login = false;
            ID = string.Empty;
            Group = string.Empty;
        }

        private void OnAdminSessionTokenUpdated(AdminSessionTokenUpdateEvent tokenUpdateEvent)
        {
            if (tokenUpdateEvent.LoginSuccess)
            {
                Debug.LogFormat("{0} Update admin session token: {1}", typeof(SessionInfo), tokenUpdateEvent.SessionToken);
                Login = true;
                ID = tokenUpdateEvent.LoginID;
                Group = GroupName.Admin;
            }
        }
#endif

#if (GYMCLIENT_MEMBER_APP || GYMCLIENT_DEBUG_APP)
        private void OnMemberSessionTokenUpdated(MemberSessionTokenUpdateEvent tokenUpdateEvent)
        {
            if (tokenUpdateEvent.LoginSuccess)
            {
                Debug.LogFormat("{0} Update member session, token: {1}", typeof(SessionInfo), tokenUpdateEvent.SessionToken);
                Login = true;
                ID = tokenUpdateEvent.LoginID;
                Group = GroupName.Member;
            }
        }

        private void OnMemberLogoutEvent(MemberLogoutEvent obj)
        {
            Login = false;
            ID = string.Empty;
            Group = string.Empty;
        }
#endif

#if (GYMCLIENT_INSTRUCTOR_APP || GYMCLIENT_DEBUG_APP)
        private void OnInstructorSessionTokenUpdated(InstructorSessionTokenUpdateEvent tokenUpdateEvent)
        {
            if (tokenUpdateEvent.LoginSuccess)
            {
                Debug.LogFormat("{0} Update instructor session, token: {1}", typeof(SessionInfo), tokenUpdateEvent.SessionToken);
                Login = true;
                ID = tokenUpdateEvent.LoginID;
                Group = GroupName.Instructor;
            }
        }

        private void OnInstructorLogoutEvent(InstructorLogoutEvent obj)
        {
            Login = false;
            ID = string.Empty;
            Group = string.Empty;
        }
#endif
    }
}
