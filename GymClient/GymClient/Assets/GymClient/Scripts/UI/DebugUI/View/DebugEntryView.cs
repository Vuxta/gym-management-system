﻿// <copyright file="DebugEntryView.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/03/2019 16:51:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 3, 2019

using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserEntry.Context;
using VuxtaStudio.GymClient.UI.UserEntry.Controller;

namespace VuxtaStudio.GymClient.UI.DebugUI.View
{
    /// <summary>
    /// Used for debugging, allow entering different apps
    /// </summary>
    public class DebugEntryView : AnimateView
    {
        public void OnAdminAppButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserMainMenuViewContext(new AdminMainMenuController()));
        }

        public void OnMemberAppButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserMainMenuViewContext(new MemberMainMenuController()));
        }

        public void OnInstructorAppButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserMainMenuViewContext(new InstructorMainMenuController()));
        }
    }
}
