﻿using VuxtaStudio.Common.UISystems;

namespace VuxtaStudio.GymClient.UI.Admin.Context
{
    public class AdminTrainingPlanManagementViewContext : BaseViewContext
    {
        private static readonly UIType m_UiType = new UIType("View/Admin/AdminTrainingPlanManagementView");

        public AdminTrainingPlanManagementViewContext() : base(m_UiType)
        {
        }
    }
}
