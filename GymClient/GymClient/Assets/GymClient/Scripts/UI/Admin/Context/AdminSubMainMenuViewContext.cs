﻿using VuxtaStudio.Common.UISystems;

namespace VuxtaStudio.GymClient.UI.Admin.Context
{
    public class AdminSubMainMenuViewContext : BaseViewContext
    {
        private static readonly UIType m_UiType = new UIType("View/Admin/AdminSubMainMenuView");

        public AdminSubMainMenuViewContext() : base(m_UiType)
        {
        }
    }
}
