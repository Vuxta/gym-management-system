﻿using VuxtaStudio.Common.UISystems;

namespace VuxtaStudio.GymClient.UI.Admin.Context
{
    public class AdminLessonManagementViewContext : BaseViewContext
    {
        private static readonly UIType m_UiType = new UIType("View/Admin/AdminLessonManagementView");

        public AdminLessonManagementViewContext() : base(m_UiType)
        {
        }
    }
}
