﻿using VuxtaStudio.Common.UISystems;

namespace VuxtaStudio.GymClient.UI.Admin.Context
{
    public class AdminTrainingPlanCreateViewContext : BaseViewContext
    {
        private static readonly UIType m_UiType = new UIType("View/Admin/AdminTrainingPlanCreateView");

        public AdminTrainingPlanCreateViewContext() : base(m_UiType)
        {
        }
    }
}
