﻿// <copyright file="AdminUserManagementViewContext.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/26/2019 15:01:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 26, 2019
using VuxtaStudio.Common.UISystems;

namespace VuxtaStudio.GymClient.UI.Admin.Context
{
    public class AdminUserManagementViewContext : BaseViewContext
    {
        private static readonly UIType m_UiType = new UIType("View/Admin/AdminUserManagementView");

        public AdminUserManagementViewContext() : base(m_UiType)
        {
        }
    }
}
