﻿using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.Admin.Context;
using VuxtaStudio.GymClient.UI.TrainingPlanUI.Context;
using VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller;

namespace VuxtaStudio.GymClient.UI.Admin.View
{
    public class AdminTrainingPlanManagementView : AnimateView
    {
        public void OnCreatePlanButtonPressed()
        {
            ViewContextManager.Instance.Push(new AdminTrainingPlanCreateViewContext());
        }

        public void OnListPlanButtonPressed()
        {
            ViewContextManager.Instance.Push(new TrainingPlanListViewContext(new AdminTrainingPlanListController()));
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }
    }
}
