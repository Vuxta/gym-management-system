﻿using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.Admin.Context;

namespace VuxtaStudio.GymClient.UI.Admin.View
{
    public class AdminLessonManagementView : AnimateView
    {
        public void OnTrainingPlanManagementButtonPressed()
        {
            ViewContextManager.Instance.Push(new AdminTrainingPlanManagementViewContext());
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }
    }
}
