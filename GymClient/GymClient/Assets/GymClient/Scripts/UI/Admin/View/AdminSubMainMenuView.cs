﻿using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.Admin.Context;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.Admin.View
{
    public class AdminSubMainMenuView : AnimateView
    {
        public void OnUserManagementButtonPressed()
        {
            ViewContextManager.Instance.Push(new AdminUserManagementViewContext());
        }

        public void OnLessionManagementButtonPressed()
        {
            ViewContextManager.Instance.Push(new AdminLessonManagementViewContext());
        }

        public void OnLogoutButtonPressed()
        {
            AdminLoginService.Instance.Logout();
            ViewContextManager.Instance.Pop();
        }
    }
}
