﻿// <copyright file="AdminSubMainMenuView.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/26/2019 15:01:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 26, 2019
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.Admin.Context;
using VuxtaStudio.GymClient.UI.UserCrud.Context;
using VuxtaStudio.GymClient.UI.UserCrud.Controller;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.Admin.View
{
    /// <summary>
    /// Main menu after admin logins
    /// </summary>
    public class AdminUserManagementView : AnimateView
    {
        public override void OnPause(BaseViewContext context)
        {
            m_Animator.SetTrigger("OnExit");
        }

        public override void OnResume(BaseViewContext context)
        {
            m_Animator.SetTrigger("OnEnter");
        }

        public void OnAdminManagementButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserManagementViewContext(new AdminUserManagementController()));
        }

        public void OnMemberManagementButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserManagementViewContext(new MemberManagementController()));
        }

        public void OnInstructorManagementButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserManagementViewContext(new InstructorManagementController()));
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }
    }
}
