﻿using VuxtaStudio.Common.UISystems;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using VuxtaStudio.GymClient.UI.Admin.Context;
using VuxtaStudio.GymClient.UI.UserCrud.Context;
using VuxtaStudio.GymClient.UI.UserCrud.Controller;
using VuxtaStudio.GymClient.WebServices.Services;
using System.Linq;
using VuxtaStudio.GymClient.WebServices.Models;
using VuxtaStudio.GymClient.UI.Common.Context;

namespace VuxtaStudio.GymClient.UI.Admin.View
{
    public class AdminTrainingPlanCreateView : AnimateView
    {
        [SerializeField]
        private Dropdown m_StartYearDropDown;

        [SerializeField]
        private Dropdown m_StartMonthDropDown;

        [SerializeField]
        private Dropdown m_StartDayDropDown;

        [SerializeField]
        private Dropdown m_StartHourDropDown;

        [SerializeField]
        private Dropdown m_StartMinuteDropDown;

        [SerializeField]
        private Dropdown m_DurationInMinutes;

        [SerializeField]
        private InputField m_NameInput;

        [SerializeField]
        private InputField m_LocationInput;

        [SerializeField]
        private InputField m_DescriptionInput;

        public override void OnEnter(BaseViewContext context)
        {
            base.OnEnter(context);

            m_StartMonthDropDown.onValueChanged.RemoveAllListeners();
            m_StartYearDropDown.onValueChanged.RemoveAllListeners();

            DateTimeDropdownInit();
            PopulateIntegerDropdowns(ref m_DurationInMinutes, 1, 90, 60);

            m_StartMonthDropDown.onValueChanged.AddListener(OnMonthValueChanged);
            m_StartYearDropDown.onValueChanged.AddListener(OnYearValueChanged);
        }

        public void OnSubmit()
        {
            TrainingPlan trainingPlan = new TrainingPlan();
            trainingPlan.Name = m_NameInput.text;
            trainingPlan.Place = m_LocationInput.text;
            trainingPlan.Description = m_DescriptionInput.text;

            DateTime startDateTime = new DateTime(int.Parse(m_StartYearDropDown.options[m_StartYearDropDown.value].text),
                                                int.Parse(m_StartMonthDropDown.options[m_StartMonthDropDown.value].text),
                                                int.Parse(m_StartDayDropDown.options[m_StartDayDropDown.value].text),
                                                int.Parse(m_StartHourDropDown.options[m_StartHourDropDown.value].text),
                                                int.Parse(m_StartMinuteDropDown.options[m_StartMinuteDropDown.value].text),
                                                0);

            trainingPlan.StartDate = startDateTime.ToString("yyyyMMddHHmm");

            DateTime endDateTime = startDateTime.AddMinutes(int.Parse(m_DurationInMinutes.options[m_DurationInMinutes.value].text));

            trainingPlan.EndDate = endDateTime.ToString("yyyyMMddHHmm");

            TrainingPlanCrudService.Instance.CreateData(trainingPlan, OnCreatePlanComplete);
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        private void OnCreatePlanComplete(bool isSuccess, string message, TrainingPlan returnedData)
        {
            if (isSuccess)
            {
                //TODO: create result UI ?
                Debug.LogFormat("Create plan success: {0}", returnedData.SaveToJson());
                ViewContextManager.Instance.Push(new PopupMessageViewContext(
                                                                string.Format("Create plan success !!!\n{0}/{1}",
                                                                            returnedData.Name,
                                                                            returnedData.Id)));
            }
            else
            {
                Debug.LogWarningFormat("Create plan fail: {0}", message);
                ViewContextManager.Instance.Push(new PopupMessageViewContext(string.Format("Create plan fail !!!\n{0}", message)));
            }
        }

        private void OnYearValueChanged(int val)
        {
            DateTime currentTime = DateTime.Now;

            PopulateIntegerDropdowns(ref m_StartDayDropDown, 1,
                        DateTime.DaysInMonth(int.Parse(m_StartYearDropDown.options[val].text),
                                            int.Parse(m_StartMonthDropDown.options[m_StartMonthDropDown.value].text)),
                        currentTime.Day);
        }

        private void OnMonthValueChanged(int val)
        {
            DateTime currentTime = DateTime.Now;

            PopulateIntegerDropdowns(ref m_StartDayDropDown, 1,
                        DateTime.DaysInMonth(int.Parse(m_StartYearDropDown.options[m_StartYearDropDown.value].text),
                                            int.Parse(m_StartMonthDropDown.options[val].text)),
                        currentTime.Day);
        }

        private void DateTimeDropdownInit()
        {
            DateTime currentTime = DateTime.Now;

            PopulateIntegerDropdowns(ref m_StartYearDropDown,
                                    currentTime.Year,
                                    currentTime.Year + 1,
                                    currentTime.Year);

            PopulateIntegerDropdowns(ref m_StartMonthDropDown, 1, 12, currentTime.Month);

            PopulateIntegerDropdowns(ref m_StartDayDropDown, 1,
                                    DateTime.DaysInMonth(currentTime.Year, currentTime.Month),
                                    currentTime.Day);

            PopulateIntegerDropdowns(ref m_StartHourDropDown, 0, 23, currentTime.Hour);
            PopulateIntegerDropdowns(ref m_StartMinuteDropDown, 0, 59, currentTime.Minute);
        }

        private void PopulateIntegerDropdowns(ref Dropdown dropdown, int start, int end, int startValue)
        {
            dropdown.options.Clear();

            List<int> integerList = Enumerable.Range(start, end - start + 1).ToList();

            if (startValue < start)
                startValue = start;

            if (startValue > end)
                startValue = end;

            for (int i = 0; i < integerList.Count; i++)
            {
                Dropdown.OptionData integerData = new Dropdown.OptionData();
                integerData.text = integerList[i].ToString();
                dropdown.options.Add(integerData);
            }

            dropdown.value = startValue - start;

            dropdown.RefreshShownValue();
        }
    }
}
