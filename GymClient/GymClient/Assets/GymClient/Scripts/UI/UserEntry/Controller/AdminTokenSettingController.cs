﻿// <copyright file="AdminTokenSettingController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/01/2019 11:01:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 1, 2019

using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.Global.Defines;
using VuxtaStudio.Common;
using VuxtaStudio.GymClient.Events;
using System;
using System.IO;
using VuxtaStudio.GymClient.UI.Common.Context;

namespace VuxtaStudio.GymClient.UI.UserEntry.Controller
{
    /// <summary>
    /// Admin token settings view controller
    /// </summary>
    public class AdminTokenSettingController : IUserTokenSettingController
    {
        public void OnSubmit(string inputText)
        {
            if (!Directory.Exists(FilePaths.TokenDirectory))
                Directory.CreateDirectory(FilePaths.TokenDirectory);

            ThreadManager.Instance.RunAsync(() => {
                WriteApiToken(inputText);
                ThreadManager.Instance.QueueOnMainThread(() => {
                    EventCenter.TriggerEvent(new AdminApiKeyUpdateEvent());
                });
            });

            ViewContextManager.Instance.Push(new PopupMessageViewContext("Admin token updated !!!"));
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        private void WriteApiToken(string token)
        {
            try
            {
                if (!Directory.Exists(FilePaths.TokenDirectory))
                    Directory.CreateDirectory(FilePaths.TokenDirectory);

                using (StreamWriter streamWriter = new StreamWriter(FilePaths.AdminApiKeyFile, false))
                {
                    streamWriter.WriteLine(token);
                    streamWriter.Close();
                }
            }
            catch (Exception e)
            {
                Debug.LogErrorFormat("Fail to write admin tokens: {0}", e.Message);
            }
        }
    }
}
