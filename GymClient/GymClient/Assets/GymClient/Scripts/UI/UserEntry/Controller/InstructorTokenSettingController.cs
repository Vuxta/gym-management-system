﻿using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.Global.Defines;
using VuxtaStudio.Common;
using VuxtaStudio.GymClient.Events;
using System;
using System.IO;
using VuxtaStudio.GymClient.UI.Common.Context;

namespace VuxtaStudio.GymClient.UI.UserEntry.Controller
{
    public class InstructorTokenSettingController : IUserTokenSettingController
    {
        public void OnSubmit(string inputText)
        {
            if (!Directory.Exists(FilePaths.TokenDirectory))
                Directory.CreateDirectory(FilePaths.TokenDirectory);

            ThreadManager.Instance.RunAsync(() => {
                WriteApiToken(inputText);
                ThreadManager.Instance.QueueOnMainThread(() => {
                    EventCenter.TriggerEvent(new InstructorApiKeyUpdateEvent());
                });
            });

            ViewContextManager.Instance.Push(new PopupMessageViewContext("Instructor token updated !!!"));
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        private void WriteApiToken(string token)
        {
            try
            {
                if (!Directory.Exists(FilePaths.TokenDirectory))
                    Directory.CreateDirectory(FilePaths.TokenDirectory);

                using (StreamWriter streamWriter = new StreamWriter(FilePaths.InstructorApiKeyFile, false))
                {
                    streamWriter.WriteLine(token);
                    streamWriter.Close();
                }
            }
            catch (Exception e)
            {
                Debug.LogErrorFormat("Fail to write instructor tokens: {0}", e.Message);
            }
        }
    }
}
