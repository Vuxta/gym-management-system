﻿using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.Common.Context;
using VuxtaStudio.GymClient.UI.InstructorUI.Context;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.UserEntry.Controller
{
    public class InstructorLoginController : IUserLoginController
    {
        public void OnLoginSubmit(string id, string password)
        {
            //TODO: use events
            InstructorLoginService.Instance.Login(id, password, OnLoginFinished);
        }

        private void OnLoginFinished(bool isSuccess, string message, string id)
        {
            if (isSuccess)
            {
                Debug.Log("Instructor login success");
                ViewContextManager.Instance.Push(new InstructorSubMainMenuViewContext());
            }
            else
            {
                Debug.LogWarningFormat("Instructor login fail: {0}", message);
                ViewContextManager.Instance.Push(new PopupMessageViewContext(string.Format("Instructor login failed!!!\n{0}", message)));
            }
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }
    }
}
