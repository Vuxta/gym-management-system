﻿// <copyright file="MemberSettingsController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/03/2019 16:51:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 3, 2019

using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserEntry.Context;

namespace VuxtaStudio.GymClient.UI.UserEntry.Controller
{
    public class MemberSettingsController : IUserSettingsController
    {
        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        public void OnTokenSettingsButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserTokenSettingViewContext(new MemberTokenSettingController()));
        }
    }
}
