﻿// <copyright file="AdminSettingsController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/01/2019 11:01:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 1, 2019

using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserEntry.Context;

namespace VuxtaStudio.GymClient.UI.UserEntry.Controller
{
    /// <summary>
    /// Admin settings view controller
    /// </summary>
    public class AdminSettingsController : IUserSettingsController
    {
        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        public void OnTokenSettingsButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserTokenSettingViewContext(new AdminTokenSettingController()));
        }
    }
}
