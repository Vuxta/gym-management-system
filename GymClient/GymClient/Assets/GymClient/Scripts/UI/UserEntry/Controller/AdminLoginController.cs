﻿// <copyright file="AdminLoginController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/01/2019 11:01:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 1, 2019

using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.Admin.Context;
using VuxtaStudio.GymClient.UI.Common.Context;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.UserEntry.Controller
{
    /// <summary>
    /// Controller for admin login view
    /// </summary>
    public class AdminLoginController : IUserLoginController
    {
        public void OnLoginSubmit(string id, string password)
        {
            //TODO: use events
            AdminLoginService.Instance.Login(id, password, OnLoginFinished);
        }

        private void OnLoginFinished(bool isSuccess, string message, string id)
        {
            if (isSuccess)
            {
                ViewContextManager.Instance.Push(new AdminSubMainMenuViewContext());
            }
            else
            {
                Debug.LogWarningFormat("Admin login fail: {0}", message);
                ViewContextManager.Instance.Push(new PopupMessageViewContext(string.Format("Admin login failed!!!\n{0}", message)));
            }
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }
    }
}
