﻿// <copyright file="MemberMainMenuController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/03/2019 16:51:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 3, 2019

using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserEntry.Context;

namespace VuxtaStudio.GymClient.UI.UserEntry.Controller
{
    public class MemberMainMenuController : IUserMainMenuController
    {
        public void OnLoginButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserLoginViewContext(new MemberLoginController()));
        }

        public void OnSettingsButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserSettingsViewContext(new MemberSettingsController()));
        }

        public void OnExitButtonPressed()
        {
#if (GYMCLIENT_DEBUG_APP)
            ViewContextManager.Instance.Pop();
#endif
        }
    }
}
