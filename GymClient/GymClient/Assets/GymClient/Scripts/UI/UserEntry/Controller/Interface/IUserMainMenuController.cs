﻿// <copyright file="IUserMainMenuController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/01/2019 11:01:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 1, 2019

namespace VuxtaStudio.GymClient.UI.UserEntry.Controller
{
    public interface IUserMainMenuController
    {
        void OnLoginButtonPressed();
        void OnSettingsButtonPressed();
        void OnExitButtonPressed();
    }
}
