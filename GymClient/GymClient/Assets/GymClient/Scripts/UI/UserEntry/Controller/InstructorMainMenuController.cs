﻿using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserEntry.Context;

namespace VuxtaStudio.GymClient.UI.UserEntry.Controller
{
    public class InstructorMainMenuController : IUserMainMenuController
    {
        public void OnLoginButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserLoginViewContext(new InstructorLoginController()));
        }

        public void OnSettingsButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserSettingsViewContext(new InstructorSettingsController()));
        }

        public void OnExitButtonPressed()
        {
#if (GYMCLIENT_DEBUG_APP)
            ViewContextManager.Instance.Pop();
#endif
        }
    }
}
