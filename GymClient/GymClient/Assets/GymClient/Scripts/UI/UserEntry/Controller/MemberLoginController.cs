﻿// <copyright file="MemberLoginController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/03/2019 16:51:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 3, 2019

using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.Common.Context;
using VuxtaStudio.GymClient.UI.MemberUI.Context;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.UserEntry.Controller
{
    public class MemberLoginController : IUserLoginController
    {
        public void OnLoginSubmit(string id, string password)
        {
            //TODO: use events
            MemberLoginService.Instance.Login(id, password, OnLoginFinished);
        }

        private void OnLoginFinished(bool isSuccess, string message, string id)
        {
            if (isSuccess)
            {
                Debug.Log("Member login success");
                ViewContextManager.Instance.Push(new MemberSubMainMenuViewContext());
            }
            else
            {
                Debug.LogWarningFormat("Member login fail: {0}", message);
                ViewContextManager.Instance.Push(new PopupMessageViewContext(string.Format("Member login failed!!!\n{0}", message)));
            }
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }
    }
}
