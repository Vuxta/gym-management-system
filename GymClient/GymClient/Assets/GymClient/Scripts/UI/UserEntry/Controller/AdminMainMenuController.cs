﻿// <copyright file="AdminMainMenuController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/01/2019 11:01:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 1, 2019

using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserEntry.Context;

namespace VuxtaStudio.GymClient.UI.UserEntry.Controller
{
    /// <summary>
    /// Admin main menu view controller
    /// </summary>
    public class AdminMainMenuController : IUserMainMenuController
    {
        public void OnLoginButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserLoginViewContext(new AdminLoginController()));
        }

        public void OnSettingsButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserSettingsViewContext(new AdminSettingsController()));
        }

        public void OnExitButtonPressed()
        {
#if (GYMCLIENT_DEBUG_APP)
            ViewContextManager.Instance.Pop();
#endif
        }
    }
}
