﻿using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserEntry.Context;

namespace VuxtaStudio.GymClient.UI.UserEntry.Controller
{
    public class InstructorSettingsController : IUserSettingsController
    {
        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        public void OnTokenSettingsButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserTokenSettingViewContext(new InstructorTokenSettingController()));
        }
    }
}
