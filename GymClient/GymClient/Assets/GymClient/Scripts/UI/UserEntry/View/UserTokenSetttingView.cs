﻿using UnityEngine;
using UnityEngine.UI;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserEntry.Context;
using VuxtaStudio.GymClient.UI.UserEntry.Controller;

namespace VuxtaStudio.GymClient.UI.UserEntry.View
{
    public class UserTokenSetttingView : AnimateView
    {
        [SerializeField]
        private InputField m_TokenInputs;

        private IUserTokenSettingController m_Controller;

        public override void OnEnter(BaseViewContext context)
        {
            base.OnEnter(context);
            m_Controller = ((UserTokenSettingViewContext)context).Controller;
        }

        public void OnSubmitButtonPressed()
        {
            m_Controller.OnSubmit(m_TokenInputs.text);
            m_TokenInputs.text = string.Empty;
        }

        public void OnBackButtonPressed()
        {
            m_Controller.OnBackButtonPressed();
        }
    }
}
