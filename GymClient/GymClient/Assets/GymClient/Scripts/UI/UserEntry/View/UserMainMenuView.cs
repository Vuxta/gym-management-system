﻿using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserEntry.Context;
using VuxtaStudio.GymClient.UI.UserEntry.Controller;

namespace VuxtaStudio.GymClient.UI.UserEntry.View
{
    public class UserMainMenuView : AnimateView
    {
        private IUserMainMenuController m_Controller;

        public override void OnEnter(BaseViewContext context)
        {
            base.OnEnter(context);
            m_Controller = ((UserMainMenuViewContext)context).Controller;
        }

        public override void OnExit(BaseViewContext context)
        {
            base.OnExit(context);
        }

        public override void OnPause(BaseViewContext context)
        {
            m_Animator.SetTrigger("OnExit");
        }

        public override void OnResume(BaseViewContext context)
        {
            m_Animator.SetTrigger("OnEnter");
        }

        public void OnLoginButtonPressed()
        {
            m_Controller.OnLoginButtonPressed();
        }

        public void OnSettingsButtonPressed()
        {
            m_Controller.OnSettingsButtonPressed();
        }

        public void OnExitButtonPressed()
        {
            m_Controller.OnExitButtonPressed();
        }
    }
}
