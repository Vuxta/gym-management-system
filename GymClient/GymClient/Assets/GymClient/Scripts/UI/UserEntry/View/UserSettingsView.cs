﻿using UnityEngine;
using UnityEngine.UI;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserEntry.Context;
using VuxtaStudio.GymClient.UI.UserEntry.Controller;

namespace VuxtaStudio.GymClient.UI.UserEntry.View
{
    public class UserSettingsView : AnimateView
    {
        private IUserSettingsController m_Controller;

        public override void OnEnter(BaseViewContext context)
        {
            base.OnEnter(context);
            m_Controller = ((UserSettingsViewContext)context).Controller;
        }

        public override void OnPause(BaseViewContext context)
        {
            m_Animator.SetTrigger("OnExit");
        }

        public override void OnResume(BaseViewContext context)
        {
            m_Animator.SetTrigger("OnEnter");
        }

        public void OnTokenSettingsButtonPressed()
        {
            m_Controller.OnTokenSettingsButtonPressed();
        }

        public void OnBackButtonPressed()
        {
            m_Controller.OnBackButtonPressed();
        }
    }
}
