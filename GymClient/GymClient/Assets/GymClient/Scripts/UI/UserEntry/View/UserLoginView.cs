﻿// <copyright file="UserLoginView.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/01/2019 11:01:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 1, 2019

using UnityEngine;
using UnityEngine.UI;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserEntry.Context;
using VuxtaStudio.GymClient.UI.UserEntry.Controller;

namespace VuxtaStudio.GymClient.UI.UserEntry.View
{
    /// <summary>
    /// User login view
    /// </summary>
    public class UserLoginView : AnimateView
    {
        [SerializeField]
        private InputField m_UserIdInputs;

        [SerializeField]
        private InputField m_PasswordInputs;

        private IUserLoginController m_Controller;

        public override void OnEnter(BaseViewContext context)
        {
            base.OnEnter(context);
            m_Controller = ((UserLoginViewContext)context).Controller;
        }

        public override void OnPause(BaseViewContext context)
        {
            m_Animator.SetTrigger("OnExit");
        }

        public override void OnResume(BaseViewContext context)
        {
            m_Animator.SetTrigger("OnEnter");
        }

        public void OnLoginButtonPressed()
        {
            m_Controller.OnLoginSubmit(m_UserIdInputs.text, m_PasswordInputs.text);
            m_PasswordInputs.text = string.Empty;
        }

        public void OnBackButtonPressed()
        {
            m_Controller.OnBackButtonPressed();
        }
    }
}
