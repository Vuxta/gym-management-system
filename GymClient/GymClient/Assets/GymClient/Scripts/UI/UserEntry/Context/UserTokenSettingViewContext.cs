﻿// <copyright file="UserTokenSettingViewContext.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/01/2019 11:01:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 1, 2019

using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserEntry.Controller;

namespace VuxtaStudio.GymClient.UI.UserEntry.Context
{
    public class UserTokenSettingViewContext : BaseViewContext
    {
        private static readonly UIType m_UiType = new UIType("View/UserEntry/UserTokenSettingView");

        public IUserTokenSettingController Controller { get; private set; }

        public UserTokenSettingViewContext(IUserTokenSettingController controller) : base(m_UiType)
        {
            Controller = controller;
        }
    }
}
