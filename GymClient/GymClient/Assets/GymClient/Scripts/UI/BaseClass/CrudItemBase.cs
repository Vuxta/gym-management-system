﻿// <copyright file="CrudItemBase.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/26/2019 15:01:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 26, 2019
using UnityEngine;
using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.UI
{
    /// <summary>
    /// Base class for displaying CRUD model in view
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class CrudItemBase<T> : MonoBehaviour where T : CrudModelBase
    {
        protected T m_Model;
        protected int m_Index;

        /// <summary>
        /// Act as constructor for inject model
        /// </summary>
        /// <param name="model"></param>
        public void Init(T model, int index)
        {
            m_Model = model;
            m_Index = index;
            OnInitDisplay();
        }

        protected virtual void OnInitDisplay()
        {
        }
    }
}
