﻿// <copyright file="PopupMessageView.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/02/2019 10:19:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 2, 2019

using System;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.Common.Context;
using UnityEngine;
using UnityEngine.UI;

namespace VuxtaStudio.GymClient.UI.Common.View
{
    /// <summary>
    /// View for handling popup messages
    /// </summary>
    public class PopupMessageView : AnimateView
    {
        [SerializeField]
        private Text m_PopupText;

        private Action m_OnOkPressed;

        public override void OnEnter(BaseViewContext context)
        {
            base.OnEnter(context);
            PopupMessageViewContext popupContext = ((PopupMessageViewContext)context);
            m_PopupText.text = popupContext.Message;
            m_OnOkPressed = popupContext.OnOkButtonPressed;
        }

        public void OnOkButtonPressed()
        {
            ViewContextManager.Instance.Pop();
            m_OnOkPressed?.Invoke();
        }
    }
}
