﻿// <copyright file="PopupMessageViewContext.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/02/2019 10:19:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 2, 2019

using System;
using VuxtaStudio.Common.UISystems;

namespace VuxtaStudio.GymClient.UI.Common.Context
{
    /// <summary>
    /// Popup message view context for accepting message and callback in context
    /// </summary>
    public class PopupMessageViewContext : BaseViewContext
    {
        private static readonly UIType m_UiType = new UIType("View/Common/PopupMessageView");

        public readonly string Message;
        public readonly Action OnOkButtonPressed;

        public PopupMessageViewContext(string message, Action onOkCallback = null) : base(m_UiType)
        {
            Message = message;
            OnOkButtonPressed = onOkCallback;
        }
    }
}
