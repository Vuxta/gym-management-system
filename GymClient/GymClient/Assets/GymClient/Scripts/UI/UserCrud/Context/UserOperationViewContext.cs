﻿// <copyright file="UserOperationViewContext.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/02/2019 10:19:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 2, 2019

using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserCrud.Controller;

namespace VuxtaStudio.GymClient.UI.UserCrud.Context
{
    public class UserOperationViewContext : BaseViewContext
    {
        private static readonly UIType m_UiType = new UIType("View/UserCrud/UserOperationView");

        public IUserOperationController Controller { get; private set; }

        public UserOperationViewContext(IUserOperationController controller) : base(m_UiType)
        {
            Controller = controller;
        }
    }
}
