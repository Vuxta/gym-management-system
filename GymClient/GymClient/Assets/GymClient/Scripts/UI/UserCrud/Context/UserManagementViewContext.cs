﻿// <copyright file="UserCrudManagementViewContext.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/27/2019 10:50:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 27, 2019

using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserCrud.Controller;

namespace VuxtaStudio.GymClient.UI.UserCrud.Context
{
    public class UserManagementViewContext : BaseViewContext
    {
        private static readonly UIType m_UiType = new UIType("View/UserCrud/UserManagementView");

        public IUserManagementController UserManagementController { get; private set; }

        public UserManagementViewContext(IUserManagementController controller) : base(m_UiType)
        {
            UserManagementController = controller;
        }
    }
}
