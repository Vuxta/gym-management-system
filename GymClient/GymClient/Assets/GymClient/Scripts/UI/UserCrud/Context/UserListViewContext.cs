﻿// <copyright file="UserListViewContext.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/27/2019 10:50:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 27, 2019
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserCrud.Controller;

namespace VuxtaStudio.GymClient.UI.UserCrud.Context
{
    /// <summary>
    /// View Context for CRUD list all
    /// </summary>
    public class UserListViewContext : BaseViewContext
    {
        private static readonly UIType m_UiType = new UIType("View/UserCrud/UserListView");

        public IUserListController UserListController { get; private set; }

        public UserListViewContext(IUserListController userListController) : base(m_UiType)
        {
            UserListController = userListController;
        }
    }
}
