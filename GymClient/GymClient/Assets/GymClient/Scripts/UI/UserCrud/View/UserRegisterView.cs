﻿// <copyright file="UserRegisterView.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/02/2019 10:19:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 2, 2019

using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserCrud.Context;
using VuxtaStudio.GymClient.UI.UserCrud.Controller;
using UnityEngine;
using UnityEngine.UI;

namespace VuxtaStudio.GymClient.UI.UserCrud.View
{
    /// <summary>
    /// Register view for specific user
    /// </summary>
    public class UserRegisterView : AnimateView
    {
        [SerializeField]
        private InputField m_PasswordInput;

        private IUserRegisterController m_Controller;

        public override void OnEnter(BaseViewContext context)
        {
            base.OnEnter(context);
            m_Controller = ((UserRegisterViewContext)context).Controller;
        }

        public void OnSubmitButtonPressed()
        {
            m_Controller.OnSubmit(m_PasswordInput.text);
        }

        public void OnBackButtonPressed()
        {
            m_Controller.OnBackButtonPressed();
        }
    }
}
