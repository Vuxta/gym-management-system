﻿// <copyright file="UserCreateView.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/27/2019 10:50:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 27, 2019

using UnityEngine;
using UnityEngine.UI;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserCrud.Context;
using VuxtaStudio.GymClient.UI.UserCrud.Controller;

namespace VuxtaStudio.GymClient.UI.UserCrud.View
{
    /// <summary>
    /// User CRUD create view
    /// </summary>
    public class UserCreateView : AnimateView
    {
        [SerializeField]
        private InputField m_NameInputs;

        [SerializeField]
        private GridToggler m_GridToggler;

        private IUserCreateController m_UserCreateController;

        public override void OnEnter(BaseViewContext context)
        {
            base.OnEnter(context);
            m_UserCreateController = ((UserCreateViewContext)context).UserCreateController;
            m_UserCreateController.Setup(m_GridToggler);
        }

        public void OnSubmitButtonPressed()
        {
            m_UserCreateController.OnSubmit(m_NameInputs.text);
        }

        public void OnBackButtonPressed()
        {
            m_UserCreateController.OnBackButtonPressed();
        }
    }
}
