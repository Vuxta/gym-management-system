﻿// <copyright file="UserListView.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/27/2019 10:50:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 27, 2019
using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserCrud.Context;
using VuxtaStudio.GymClient.UI.UserCrud.Controller;

namespace VuxtaStudio.GymClient.UI.UserCrud.View
{
    /// <summary>
    /// View for user CRUD list all
    /// </summary>
    public class UserListView : AnimateView
    {
        [SerializeField]
        private GridScroller m_GridScroller;

        private IUserListController m_UserListController;

        public override void OnEnter(BaseViewContext context)
        {
            base.OnEnter(context);
            m_UserListController = ((UserListViewContext)context).UserListController;
            m_UserListController.Setup(m_GridScroller);
            m_UserListController.OnRefreshButtonPressed();
        }

        public override void OnPause(BaseViewContext context)
        {
            m_Animator.SetTrigger("OnExit");
        }

        public override void OnResume(BaseViewContext context)
        {
            m_Animator.SetTrigger("OnEnter");
        }

        public void OnBackButtonPressed()
        {
            m_UserListController.OnBackButtonPressed();
        }

        public void OnRefreshButtonPressed()
        {
            m_UserListController.OnRefreshButtonPressed();
        }
    }
}
