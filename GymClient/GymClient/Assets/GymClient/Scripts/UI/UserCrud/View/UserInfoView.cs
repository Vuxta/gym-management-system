﻿// <copyright file="UserInfoView.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/02/2019 10:19:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 2, 2019

using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserCrud.Context;
using UnityEngine;
using UnityEngine.UI;
using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.UI.UserCrud.View
{
    /// <summary>
    /// Information view for specific user
    /// </summary>
    public class UserInfoView : AnimateView
    {
        [SerializeField]
        private Text m_DisplayText;

        private UserCrudModel m_Model;

        public override void OnEnter(BaseViewContext context)
        {
            base.OnEnter(context);
            m_Model = ((UserInfoViewContext)context).UserModel;
            ShowModel();
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        private void ShowModel()
        {
            m_DisplayText.text = string.Format("ID: {0}\nName: {1}\nMail: {2}\nPhone: {3}",
                                                m_Model.Id, m_Model.UserName, m_Model.RegisteredMail, m_Model.RegisteredPhone);
        }
    }
}
