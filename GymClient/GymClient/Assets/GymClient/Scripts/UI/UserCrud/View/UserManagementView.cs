﻿// <copyright file="UserManagementView.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/28/2019 15:01:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 28, 2019

using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserCrud.Context;
using VuxtaStudio.GymClient.UI.UserCrud.Controller;

namespace VuxtaStudio.GymClient.UI.UserCrud.View
{
    /// <summary>
    /// User CRUD entry view
    /// </summary>
    public class UserManagementView : AnimateView
    {
        private IUserManagementController m_UserManagementController;

        public override void OnEnter(BaseViewContext context)
        {
            base.OnEnter(context);
            m_UserManagementController = ((UserManagementViewContext)context).UserManagementController;
        }

        public override void OnPause(BaseViewContext context)
        {
            m_Animator.SetTrigger("OnExit");
        }

        public override void OnResume(BaseViewContext context)
        {
            m_Animator.SetTrigger("OnEnter");
        }

        public void OnCreateButtonPressed()
        {
            m_UserManagementController.OnCreateButtonPressed();
        }

        public void OnListAllButtonPressed()
        {
            m_UserManagementController.OnListAllButtonPressed();
        }

        public void OnBackButtonPressed()
        {
            m_UserManagementController.OnBackButtonPressed();
        }
    }
}
