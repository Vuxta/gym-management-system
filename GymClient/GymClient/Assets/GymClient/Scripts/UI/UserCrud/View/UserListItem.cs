﻿// <copyright file="UserListItem.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/27/2019 10:50:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 27, 2019

using System;
using UnityEngine;
using UnityEngine.UI;
using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.UI.UserCrud.View
{
    /// <summary>
    /// Item to show info about individual user in view
    /// </summary>
    public class UserListItem : CrudItemBase<UserCrudModel>
    {
        [SerializeField]
        private Text m_DisplayText;

        public Action<UserCrudModel, int> OnSelect;

        protected override void OnInitDisplay()
        {
            m_DisplayText.text = string.Format("{0}\n{1}", m_Model.UserName, m_Model.Id);
        }

        public void OnButtonPressed()
        {
            OnSelect?.Invoke(m_Model, m_Index);
        }
    }
}
