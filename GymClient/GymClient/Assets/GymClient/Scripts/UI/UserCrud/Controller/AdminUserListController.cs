﻿// <copyright file="AdminUserListController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/27/2019 10:50:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 27, 2019
using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.Common.Context;
using VuxtaStudio.GymClient.UI.UserCrud.Context;
using VuxtaStudio.GymClient.UI.UserCrud.View;
using VuxtaStudio.GymClient.WebServices.Models;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.UserCrud.Controller
{
    /// <summary>
    /// Controller for handling list view operations for admin users
    /// </summary>
    public class AdminUserListController : IUserListController
    {
        private GridScroller m_GridScroller;
        private AdminUser[] m_AdminUsers;

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        public void OnRefreshButtonPressed()
        {
            GetAllRequest();
        }

        public void Setup(GridScroller gridScroller)
        {
            m_GridScroller = gridScroller;
        }

        public void OnChange(Transform trans, int index)
        {
            //trans.GetComponent<HighScoreItem>().Init(index);
            UserListItem userListItem = trans.GetComponent<UserListItem>();
            userListItem.OnSelect = OnItemSelected;
            userListItem.Init(m_AdminUsers[index], index);
        }

        private void OnItemSelected(UserCrudModel obj, int index)
        {
            //TODO: push to user specific UI
            Debug.LogFormat("Select {0} : {1}", m_AdminUsers[index].UserName, m_AdminUsers[index].Id);
            ViewContextManager.Instance.Push(new UserOperationViewContext(new AdminOperationController(m_AdminUsers[index])));
        }

        private void GetAllRequest()
        {
            //TODO: use events
            AdminUserCrudService.Instance.GetAllData(OnGetAllRequestComplete);
        }

        private void OnGetAllRequestComplete(bool isSuccess, string messages, AdminUser[] datas)
        {
            m_AdminUsers = null;

            if (!isSuccess)
            {
                m_GridScroller.Clear();
                Debug.LogWarningFormat("Fail to get all admin user datas: {0}", messages);
                ViewContextManager.Instance.Push(new PopupMessageViewContext(
                                                        string.Format("Fail to get all admin user datas !!!\n{0}",
                                                        messages)));
                return;
            }

            if (datas == null || datas.Length <= 0)
            {
                m_GridScroller.Clear();
                Debug.LogFormat("No admin user datas: {0}", messages);
                ViewContextManager.Instance.Push(new PopupMessageViewContext("No admin user datas"));
                return;
            }

            m_AdminUsers = datas;

            m_GridScroller.Init(OnChange, m_AdminUsers.Length, new Vector2(0.12f, 1f));
        }
    }
}
