﻿// <copyright file="InstructorListController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/01/2019 11:01:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 1, 2019

using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.Common.Context;
using VuxtaStudio.GymClient.UI.UserCrud.Context;
using VuxtaStudio.GymClient.UI.UserCrud.View;
using VuxtaStudio.GymClient.WebServices.Models;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.UserCrud.Controller
{
    /// <summary>
    /// Instructor list view controller
    /// </summary>
    public class InstructorListController : IUserListController
    {
        private GridScroller m_GridScroller;
        private Instructor[] m_Instructors;

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        public void OnRefreshButtonPressed()
        {
            GetAllRequest();
        }

        public void Setup(GridScroller gridScroller)
        {
            m_GridScroller = gridScroller;
        }

        public void OnChange(Transform trans, int index)
        {
            //trans.GetComponent<HighScoreItem>().Init(index);
            UserListItem userListItem = trans.GetComponent<UserListItem>();
            userListItem.OnSelect = OnItemSelected;
            userListItem.Init(m_Instructors[index], index);
        }

        private void OnItemSelected(UserCrudModel obj, int index)
        {
            //TODO: push to user specific UI
            Debug.LogFormat("Select {0} : {1}", m_Instructors[index].UserName, m_Instructors[index].Id);
            ViewContextManager.Instance.Push(new UserOperationViewContext(new InstructorOperationController(m_Instructors[index])));
        }

        private void GetAllRequest()
        {
            //TODO: use events
            InstructorCrudService.Instance.GetAllData(OnGetAllRequestComplete);
        }

        private void OnGetAllRequestComplete(bool isSuccess, string messages, Instructor[] datas)
        {
            m_Instructors = null;

            if (!isSuccess)
            {
                m_GridScroller.Clear();
                Debug.LogWarningFormat("Fail to get all instructor datas: {0}", messages);
                ViewContextManager.Instance.Push(new PopupMessageViewContext(
                                        string.Format("Fail to get all instructor datas !!!\n{0}",
                                        messages)));
                return;
            }

            if (datas == null || datas.Length <= 0)
            {
                m_GridScroller.Clear();
                Debug.LogFormat("No instructor datas: {0}", messages);
                ViewContextManager.Instance.Push(new PopupMessageViewContext("No instructor datas"));
                return;
            }

            m_Instructors = datas;

            m_GridScroller.Init(OnChange, m_Instructors.Length, new Vector2(0.12f, 1f));
        }
    }
}
