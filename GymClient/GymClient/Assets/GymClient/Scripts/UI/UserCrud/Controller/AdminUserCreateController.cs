﻿// <copyright file="AdminUserCreateController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/27/2019 10:50:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 27, 2019

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.Global.Defines;
using VuxtaStudio.GymClient.UI.Common.Context;
using VuxtaStudio.GymClient.WebServices.Models;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.UserCrud.Controller
{
    /// <summary>
    /// Admin CRUD create view controller
    /// </summary>
    public class AdminUserCreateController : IUserCreateController
    {
        private long m_Roles = 0;
        private Dictionary<string, long> m_RoleNames = new Dictionary<string, long>() {
                                                            {"Super User", AdminRoles.SuperUser},
                                                            {"Admin", AdminRoles.AdminManager},
                                                            {"Instructor", AdminRoles.InstructorManager},
                                                            {"Member", AdminRoles.MemberManager}
                                                        };

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        public void OnSubmit(string userName)
        {
            AdminUser adminUser = new AdminUser();
            adminUser.UserName = userName;

            adminUser.Roles = m_Roles;

            //TODO: use events to send data
            AdminUserCrudService.Instance.CreateData(adminUser, OnCreateComplete);
        }

        public void Setup(GridToggler gridToggler)
        {
            gridToggler.Init(OnToggleValueChanged, m_RoleNames.Keys.ToArray());
        }

        private void OnToggleValueChanged(bool onOff, string toggleName)
        {
            if (onOff)
                m_Roles |= m_RoleNames[toggleName];
            else
                m_Roles &= (~m_RoleNames[toggleName]);
        }

        private void OnCreateComplete(bool isSuccess, string message, AdminUser returnedData)
        {
            if (isSuccess)
            {
                //TODO: create result UI ?
                Debug.LogFormat("Create admin user success: {0}", returnedData.SaveToJson());
                ViewContextManager.Instance.Push(new PopupMessageViewContext(
                                                                string.Format("Create admin user success !!!\n{0}/{1}",
                                                                            returnedData.UserName,
                                                                            returnedData.Id)));
            }
            else
            {
                Debug.LogWarningFormat("Create admin user fail: {0}", message);
                ViewContextManager.Instance.Push(new PopupMessageViewContext(string.Format("Create admin user fail !!!\n{0}", message)));
            }
        }
    }
}
