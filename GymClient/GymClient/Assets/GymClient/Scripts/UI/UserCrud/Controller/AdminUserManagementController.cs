﻿// <copyright file="AdminUserManagementController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/28/2019 15:01:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 28, 2019
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserCrud.Context;

namespace VuxtaStudio.GymClient.UI.UserCrud.Controller
{
    /// <summary>
    /// Controller for user CRUD entry
    /// </summary>
    public class AdminUserManagementController : IUserManagementController
    {
        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        public void OnCreateButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserCreateViewContext(new AdminUserCreateController()));
        }

        public void OnListAllButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserListViewContext(new AdminUserListController()));
        }
    }
}
