﻿// <copyright file="MemberOperationController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/02/2019 10:19:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 2, 2019

using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserCrud.Context;
using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.UI.UserCrud.Controller
{
    public class MemberOperationController : IUserOperationController
    {
        private Member m_User;

        public MemberOperationController(Member user)
        {
            m_User = user;
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        public void OnRegisterButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserRegisterViewContext(new MemberRegisterController(m_User)));
        }

        public void OnShowInfoButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserInfoViewContext(m_User));
        }
    }
}
