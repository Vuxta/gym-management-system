﻿// <copyright file="InstructorCreateController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/01/2019 11:01:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 1, 2019

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.Global.Defines;
using VuxtaStudio.GymClient.UI.Common.Context;
using VuxtaStudio.GymClient.WebServices.Models;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.UserCrud.Controller
{
    /// <summary>
    /// Instructor create view controller
    /// </summary>
    public class InstructorCreateController : IUserCreateController
    {
        private long m_Roles = 0;
        private Dictionary<string, long> m_RoleNames = new Dictionary<string, long>() {
                                                            {"Normal", InstructorRoles.Normal},
                                                        };

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        public void OnSubmit(string userName)
        {
            Instructor user = new Instructor();
            user.UserName = userName;

            user.Roles = m_Roles;

            //TODO: use events to send data
            InstructorCrudService.Instance.CreateData(user, OnCreateComplete);
        }

        public void Setup(GridToggler gridToggler)
        {
            gridToggler.Init(OnToggleValueChanged, m_RoleNames.Keys.ToArray());
        }

        private void OnToggleValueChanged(bool onOff, string toggleName)
        {
            if (onOff)
                m_Roles |= m_RoleNames[toggleName];
            else
                m_Roles &= (~m_RoleNames[toggleName]);
        }

        private void OnCreateComplete(bool isSuccess, string message, Instructor returnedData)
        {
            if (isSuccess)
            {
                //TODO: create result UI ?
                Debug.LogFormat("Create instructor success: {0}", returnedData.SaveToJson());
                ViewContextManager.Instance.Push(new PopupMessageViewContext(
                                                string.Format("Create instructor success !!!\n{0}/{1}",
                                                            returnedData.UserName,
                                                            returnedData.Id)));
            }
            else
            {
                Debug.LogWarningFormat("Create instructor fail: {0}", message);
                ViewContextManager.Instance.Push(new PopupMessageViewContext(string.Format("Create instructor fail !!!\n{0}", message)));
            }
        }
    }
}
