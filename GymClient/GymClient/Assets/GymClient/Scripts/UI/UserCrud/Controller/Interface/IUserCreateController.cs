﻿// <copyright file="IUserCreateController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/27/2019 10:50:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 27, 2019
using VuxtaStudio.Common.UISystems;

namespace VuxtaStudio.GymClient.UI.UserCrud.Controller
{
    /// <summary>
    /// User CRUD create controller
    /// </summary>
    public interface IUserCreateController
    {
        /// <summary>
        /// Init functions called by view when started
        /// </summary>
        /// <param name="gridToggler"></param>
        void Setup(GridToggler gridToggler);

        /// <summary>
        /// Submit data and upload to server
        /// </summary>
        /// <param name="userName"></param>
        void OnSubmit(string userName);
        void OnBackButtonPressed();
    }
}
