﻿// <copyright file="MemberCreateController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/28/2019 17:56:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 28, 2019

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.Global.Defines;
using VuxtaStudio.GymClient.UI.Common.Context;
using VuxtaStudio.GymClient.WebServices.Models;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.UserCrud.Controller
{
    /// <summary>
    /// Member create view controller
    /// </summary>
    public class MemberCreateController : IUserCreateController
    {
        private long m_Roles = 0;
        private Dictionary<string, long> m_RoleNames = new Dictionary<string, long>() {
                                                            {"Normal", MemberRoles.Normal},
                                                        };

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        public void OnSubmit(string userName)
        {
            Member user = new Member();
            user.UserName = userName;

            user.Roles = m_Roles;

            //TODO: use events to send data
            MemberCrudService.Instance.CreateData(user, OnCreateComplete);
        }

        public void Setup(GridToggler gridToggler)
        {
            gridToggler.Init(OnToggleValueChanged, m_RoleNames.Keys.ToArray());
        }

        private void OnToggleValueChanged(bool onOff, string toggleName)
        {
            if (onOff)
                m_Roles |= m_RoleNames[toggleName];
            else
                m_Roles &= (~m_RoleNames[toggleName]);
        }

        private void OnCreateComplete(bool isSuccess, string message, Member returnedData)
        {
            if (isSuccess)
            {
                //TODO: create result UI ?
                Debug.LogFormat("Create member success: {0}", returnedData.SaveToJson());
                ViewContextManager.Instance.Push(new PopupMessageViewContext(
                                string.Format("Create member success !!!\n{0}/{1}",
                                            returnedData.UserName,
                                            returnedData.Id)));
            }
            else
            {
                Debug.LogWarningFormat("Create member fail: {0}", message);
                ViewContextManager.Instance.Push(new PopupMessageViewContext(string.Format("Create member fail !!!\n{0}", message)));
            }
        }
    }
}
