﻿// <copyright file="IUserManagementController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/28/2019 15:01:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 28, 2019

namespace VuxtaStudio.GymClient.UI.UserCrud.Controller
{
    /// <summary>
    /// Interface for user CRUD entry view
    /// </summary>
    public interface IUserManagementController
    {
        void OnCreateButtonPressed();
        void OnListAllButtonPressed();
        void OnBackButtonPressed();
    }
}
