﻿// <copyright file="InstructorManagementController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/01/2019 11:01:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 1, 2019

using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.UserCrud.Context;

namespace VuxtaStudio.GymClient.UI.UserCrud.Controller
{
    /// <summary>
    /// Instructor management view controller
    /// </summary>
    public class InstructorManagementController : IUserManagementController
    {
        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        public void OnCreateButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserCreateViewContext(new InstructorCreateController()));
        }

        public void OnListAllButtonPressed()
        {
            ViewContextManager.Instance.Push(new UserListViewContext(new InstructorListController()));
        }
    }
}
