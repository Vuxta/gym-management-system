﻿// <copyright file="MemberRegisterController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/02/2019 10:19:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 2, 2019

using System;
using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.Common.Context;
using VuxtaStudio.GymClient.UI.UserCrud.Context;
using VuxtaStudio.GymClient.WebServices.Models;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.UserCrud.Controller
{
    public class MemberRegisterController : IUserRegisterController
    {
        private Member m_User;

        public MemberRegisterController(Member user)
        {
            m_User = user;
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        public void OnSubmit(string password)
        {
            MemberRegisterService.Instance.Register(m_User.Id, password, OnRegisterComplete);
        }

        private void OnRegisterComplete(bool isSuccess, string message)
        {
            if (!isSuccess)
            {
                Debug.LogWarningFormat("Member register failed: {0}", message);
                ViewContextManager.Instance.Push(new PopupMessageViewContext(string.Format("Member register failed:\n{0}", message)));
                return;
            }

            Debug.LogFormat("Member register success: {0}", message);
            ViewContextManager.Instance.Push(new PopupMessageViewContext(string.Format("Member register success:\n{0}", message)));
        }
    }
}
