﻿// <copyright file="MemberListController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/28/2019 17:56:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 28, 2019

using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.Common.Context;
using VuxtaStudio.GymClient.UI.UserCrud.Context;
using VuxtaStudio.GymClient.UI.UserCrud.View;
using VuxtaStudio.GymClient.WebServices.Models;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.UserCrud.Controller
{
    /// <summary>
    /// Member list controller view
    /// </summary>
    public class MemberListController : IUserListController
    {
        private GridScroller m_GridScroller;
        private Member[] m_Members;

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        public void OnRefreshButtonPressed()
        {
            GetAllRequest();
        }

        public void Setup(GridScroller gridScroller)
        {
            m_GridScroller = gridScroller;
        }

        public void OnChange(Transform trans, int index)
        {
            //trans.GetComponent<HighScoreItem>().Init(index);
            UserListItem userListItem = trans.GetComponent<UserListItem>();
            userListItem.OnSelect = OnItemSelected;
            userListItem.Init(m_Members[index], index);
        }

        private void OnItemSelected(UserCrudModel obj, int index)
        {
            //TODO: push to user specific UI
            Debug.LogFormat("Select {0} : {1}", m_Members[index].UserName, m_Members[index].Id);
            ViewContextManager.Instance.Push(new UserOperationViewContext(new MemberOperationController(m_Members[index])));
        }

        private void GetAllRequest()
        {
            //TODO: use events
            MemberCrudService.Instance.GetAllData(OnGetAllRequestComplete);
        }

        private void OnGetAllRequestComplete(bool isSuccess, string messages, Member[] datas)
        {
            m_Members = null;

            if (!isSuccess)
            {
                m_GridScroller.Clear();
                Debug.LogWarningFormat("Fail to get all member datas: {0}", messages);
                ViewContextManager.Instance.Push(new PopupMessageViewContext(
                        string.Format("Fail to get all member datas !!!\n{0}",
                        messages)));
                return;
            }

            if (datas == null || datas.Length <= 0)
            {
                m_GridScroller.Clear();
                Debug.LogFormat("No member datas: {0}", messages);
                ViewContextManager.Instance.Push(new PopupMessageViewContext("No member datas"));
                return;
            }

            m_Members = datas;

            m_GridScroller.Init(OnChange, m_Members.Length, new Vector2(0.12f, 1f));
        }
    }
}
