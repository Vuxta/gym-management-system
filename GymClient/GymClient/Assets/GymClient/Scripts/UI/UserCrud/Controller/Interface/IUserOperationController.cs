﻿// <copyright file="IUserOperationController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/02/2019 10:19:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 2, 2019

namespace VuxtaStudio.GymClient.UI.UserCrud.Controller
{
    public interface IUserOperationController
    {
        /// <summary>
        /// Show specific user info
        /// </summary>
        void OnShowInfoButtonPressed();

        /// <summary>
        /// Show registration page
        /// </summary>
        void OnRegisterButtonPressed();
        void OnBackButtonPressed();
    }
}
