﻿// <copyright file="IUserRegisterController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/02/2019 10:19:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 2, 2019

namespace VuxtaStudio.GymClient.UI.UserCrud.Controller
{
    /// <summary>
    /// Controller Interface for user register view
    /// </summary>
    public interface IUserRegisterController
    {
        /// <summary>
        /// Register submit with password
        /// </summary>
        /// <param name="password"></param>
        void OnSubmit(string password);
        void OnBackButtonPressed();
    }
}
