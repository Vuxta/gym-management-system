﻿// <copyright file="IUserListController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/27/2019 10:50:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 27, 2019
using VuxtaStudio.Common.UISystems;

namespace VuxtaStudio.GymClient.UI.UserCrud.Controller
{
    public interface IUserListController
    {
        void Setup(GridScroller gridScroller);
        void OnBackButtonPressed();
        void OnRefreshButtonPressed();
    }
}
