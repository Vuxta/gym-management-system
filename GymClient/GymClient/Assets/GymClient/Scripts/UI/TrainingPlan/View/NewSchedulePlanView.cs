﻿using UnityEngine;
using UnityEngine.UI;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.TrainingPlanUI.Context;
using VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller;
using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.UI.TrainingPlanUI.View
{
    public class NewSchedulePlanView : AnimateView
    {
        [SerializeField]
        private Text m_DisplayText;

        private INewSchedulePlanController m_Controller;
        private TrainingPlan m_Plan;

        public override void OnEnter(BaseViewContext context)
        {
            base.OnEnter(context);
            m_Plan = ((NewSchedulePlanViewContext)context).Plan;
            m_Controller = ((NewSchedulePlanViewContext)context).Controller;

            ShowModel();
        }

        public void OnAddToScheduleButtonPressed()
        {
            m_Controller.AddToSchedule();
        }

        public void OnBackButtonPressed()
        {
            m_Controller.OnBackButtonPressed();
        }

        private void ShowModel()
        {
            m_DisplayText.text = string.Format("Name: {0}\nID: {1}\nStart: {2}\nEnd: {3}",
                                                m_Plan.Name, m_Plan.Id, m_Plan.StartDate, m_Plan.EndDate);
        }
    }
}
