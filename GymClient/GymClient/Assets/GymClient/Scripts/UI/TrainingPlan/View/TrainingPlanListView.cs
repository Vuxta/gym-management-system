﻿using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.TrainingPlanUI.Context;
using VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller;

namespace VuxtaStudio.GymClient.UI.TrainingPlanUI.View
{
    public class TrainingPlanListView : AnimateView
    {
        [SerializeField]
        private GridScroller m_GridScroller;

        private ITrainingPlanListController m_Controller;

        public override void OnEnter(BaseViewContext context)
        {
            base.OnEnter(context);
            m_Controller = ((TrainingPlanListViewContext)context).Controller;
            m_Controller.Setup(m_GridScroller);
            m_Controller.OnRefreshButtonPressed();
        }

        public void OnBackButtonPressed()
        {
            m_Controller.OnBackButtonPressed();
        }

        public void OnRefreshButtonPressed()
        {
            m_Controller.OnRefreshButtonPressed();
        }
    }
}
