﻿using System;
using UnityEngine;
using UnityEngine.UI;
using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.UI.TrainingPlanUI.View
{
    class TrainingPlanListItem : CrudItemBase<TrainingPlan>
    {
        [SerializeField]
        private Text m_DisplayText;

        public Action<TrainingPlan, int> OnSelect;

        protected override void OnInitDisplay()
        {
            m_DisplayText.text = string.Format("{0}\n{1}\n{2} to {3}", m_Model.Name, m_Model.Id, m_Model.StartDate, m_Model.EndDate);
        }

        public void OnButtonPressed()
        {
            OnSelect?.Invoke(m_Model, m_Index);
        }
    }
}
