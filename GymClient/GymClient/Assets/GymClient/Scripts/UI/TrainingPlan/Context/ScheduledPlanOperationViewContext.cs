﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller;
using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.UI.TrainingPlanUI.Context
{
    class ScheduledPlanOperationViewContext : BaseViewContext
    {
        private static readonly UIType m_UiType = new UIType("View/TrainingPlan/ScheduledPlanOperationView");

        public readonly TrainingPlan Plan;
        public readonly IScheduledPlanOperationController Controller;

        public ScheduledPlanOperationViewContext(TrainingPlan plan, IScheduledPlanOperationController controller) : base(m_UiType)
        {
            Plan = plan;
            Controller = controller;
        }
    }
}
