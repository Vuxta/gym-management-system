﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller;
using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.UI.TrainingPlanUI.Context
{
    public class NewSchedulePlanViewContext : BaseViewContext
    {
        private static readonly UIType m_UiType = new UIType("View/TrainingPlan/NewSchedulePlanView");

        public readonly TrainingPlan Plan;
        public readonly INewSchedulePlanController Controller;

        public NewSchedulePlanViewContext(TrainingPlan plan, INewSchedulePlanController controller) : base(m_UiType)
        {
            Plan = plan;
            Controller = controller;
        }
    }
}
