﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller;

namespace VuxtaStudio.GymClient.UI.TrainingPlanUI.Context
{
    public class TrainingPlanListViewContext : BaseViewContext
    {
        private static readonly UIType m_UiType = new UIType("View/TrainingPlan/TrainingPlanListView");

        public readonly ITrainingPlanListController Controller;

        public TrainingPlanListViewContext(ITrainingPlanListController controller) : base(m_UiType)
        {
            Controller = controller;
        }
    }
}
