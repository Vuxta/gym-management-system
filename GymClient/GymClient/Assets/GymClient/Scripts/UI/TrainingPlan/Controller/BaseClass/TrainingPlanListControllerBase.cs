﻿using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.Common.Context;
using VuxtaStudio.GymClient.UI.TrainingPlanUI.View;
using VuxtaStudio.GymClient.WebServices.Models;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller
{
    public abstract class TrainingPlanListControllerBase : ITrainingPlanListController
    {
        protected GridScroller m_GridScroller;
        protected TrainingPlan[] m_Plans;

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        public void OnRefreshButtonPressed()
        {
            GetAllRequest();
        }

        public void Setup(GridScroller gridScroller)
        {
            m_GridScroller = gridScroller;
        }

        public void OnChange(Transform trans, int index)
        {
            //trans.GetComponent<HighScoreItem>().Init(index);
            TrainingPlanListItem listItem = trans.GetComponent<TrainingPlanListItem>();
            listItem.OnSelect = OnItemSelected;
            listItem.Init(m_Plans[index], index);
        }

        protected virtual void OnItemSelected(TrainingPlan obj, int index)
        {
            //TODO: push to user specific UI
            Debug.LogFormat("Select {0} : {1}", m_Plans[index].Name, m_Plans[index].Id);
            //ViewContextManager.Instance.Push(new UserOperationViewContext(new AdminOperationController(m_Plans[index])));
        }

        protected virtual void GetAllRequest()
        {
            //TODO: use events
            TrainingScheduleService.Instance.GetAllTrainingPlans(OnGetAllRequestComplete);
        }

        protected void OnGetAllRequestComplete(bool isSuccess, string messages, TrainingPlan[] datas)
        {
            m_Plans = null;

            if (!isSuccess)
            {
                m_GridScroller.Clear();
                Debug.LogWarningFormat("Fail to get all training plan datas: {0}", messages);
                ViewContextManager.Instance.Push(new PopupMessageViewContext(
                                                        string.Format("Fail to get all training plan datas !!!\n{0}",
                                                        messages)));
                return;
            }

            if (datas == null || datas.Length <= 0)
            {
                m_GridScroller.Clear();
                Debug.LogFormat("No training plan datas: {0}", messages);
                ViewContextManager.Instance.Push(new PopupMessageViewContext("No training plan datas"));
                return;
            }

            m_Plans = datas;

            m_GridScroller.Init(OnChange, m_Plans.Length, new Vector2(0.12f, 1f));
        }
    }
}
