﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.Common.Context;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller
{
    public class MemberNewSchedulePlanController : INewSchedulePlanController
    {
        private string m_MemberID;
        private string m_PlanID;

        public MemberNewSchedulePlanController(string memberId, string planId)
        {
            m_MemberID = memberId;
            m_PlanID = planId;
        }

        public void AddToSchedule()
        {
            TrainingScheduleService.Instance.AddToMemberSchedule(m_MemberID, OnUpdateComplete, m_PlanID);
        }

        private void OnUpdateComplete(bool isSuccess, string message)
        {
            if (!isSuccess)
            {
                Debug.LogWarningFormat("On add new member schedule failed: {0}", message);
                ViewContextManager.Instance.Push(new PopupMessageViewContext(string.Format("On add new member schedule failed: {0}", message)));
            }
            else
            {
                Debug.LogFormat("On add new member schedule success: {0}", message);
                ViewContextManager.Instance.Push(new PopupMessageViewContext("Add new member schedule success !!!"));
            }
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }
    }
}
