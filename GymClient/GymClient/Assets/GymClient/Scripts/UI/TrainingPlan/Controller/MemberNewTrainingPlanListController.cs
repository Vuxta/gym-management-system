﻿using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.TrainingPlanUI.Context;
using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller
{
    public class MemberNewTrainingPlanListController : TrainingPlanListControllerBase
    {
        private string m_MemberID;

        public MemberNewTrainingPlanListController(string id)
        {
            m_MemberID = id;
        }

        protected override void OnItemSelected(TrainingPlan obj, int index)
        {
            base.OnItemSelected(obj, index);
            ViewContextManager.Instance.Push(new NewSchedulePlanViewContext(obj, new MemberNewSchedulePlanController(m_MemberID, obj.Id)));
        }
    }
}
