﻿namespace VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller
{
    public interface IScheduledPlanOperationController
    {
        void AddToAttended();
        void RemoveFromSchedule();
        void OnBackButtonPressed();
    }
}
