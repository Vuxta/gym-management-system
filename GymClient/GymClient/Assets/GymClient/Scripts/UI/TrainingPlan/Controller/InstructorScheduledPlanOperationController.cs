﻿using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.Common.Context;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller
{
    public class InstructorScheduledPlanOperationController : IScheduledPlanOperationController
    {
        private string m_InstructorID;
        private string m_PlanID;

        public InstructorScheduledPlanOperationController(string instructorId, string planId)
        {
            m_InstructorID = instructorId;
            m_PlanID = planId;
        }

        public void AddToAttended()
        {
            TrainingScheduleService.Instance.AddToInstructorAttended(m_InstructorID, OnUpdateComplete, m_PlanID);
        }

        public void RemoveFromSchedule()
        {
            TrainingScheduleService.Instance.RemoveFromInstructorSchedule(m_InstructorID, OnUpdateComplete, m_PlanID);
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        private void OnUpdateComplete(bool isSuccess, string message)
        {
            if (!isSuccess)
            {
                Debug.LogWarningFormat("On update instructor schedule failed: {0}", message);
                ViewContextManager.Instance.Push(new PopupMessageViewContext(string.Format("On update instructor schedule failed: {0}", message)));
            }
            else
            {
                Debug.LogFormat("On update instructor schedule success: {0}", message);
                ViewContextManager.Instance.Push(new PopupMessageViewContext("On update instructor schedule success !!!"));
            }
        }
    }
}
