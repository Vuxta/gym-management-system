﻿using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.TrainingPlanUI.Context;
using VuxtaStudio.GymClient.WebServices.Models;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller
{
    public class InstructorScheduledTrainingPlanListController : TrainingPlanListControllerBase
    {
        private string m_InstructorID;

        public InstructorScheduledTrainingPlanListController(string id)
        {
            m_InstructorID = id;
        }

        protected override void OnItemSelected(TrainingPlan obj, int index)
        {
            base.OnItemSelected(obj, index);
            ViewContextManager.Instance.Push(new ScheduledPlanOperationViewContext(obj, new InstructorScheduledPlanOperationController(m_InstructorID, obj.Id)));
        }

        protected override void GetAllRequest()
        {
            TrainingScheduleService.Instance.GetInstructorScheduledPlans(m_InstructorID, OnGetAllRequestComplete);
        }
    }
}
