﻿using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.TrainingPlanUI.Context;
using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller
{
    public class InstructorNewTrainingPlanListController : TrainingPlanListControllerBase
    {
        private string m_InstructorID;

        public InstructorNewTrainingPlanListController(string id)
        {
            m_InstructorID = id;
        }

        protected override void OnItemSelected(TrainingPlan obj, int index)
        {
            base.OnItemSelected(obj, index);
            ViewContextManager.Instance.Push(new NewSchedulePlanViewContext(obj, new InstructorNewSchedulePlanController(m_InstructorID, obj.Id)));
        }
    }
}
