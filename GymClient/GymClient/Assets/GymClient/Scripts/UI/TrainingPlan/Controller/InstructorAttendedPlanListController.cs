﻿using VuxtaStudio.GymClient.WebServices.Models;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller
{
    public class InstructorAttendedPlanListController : TrainingPlanListControllerBase
    {
        private string m_InstructorID;

        public InstructorAttendedPlanListController(string id)
        {
            m_InstructorID = id;
        }

        protected override void OnItemSelected(TrainingPlan obj, int index)
        {
            base.OnItemSelected(obj, index);
            //TODO: go to next view
        }

        protected override void GetAllRequest()
        {
            TrainingScheduleService.Instance.GetInstructorAttendedPlans(m_InstructorID, OnGetAllRequestComplete);
        }
    }
}
