﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.Common.Context;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller
{
    public class MemberScheduledPlanOperationController : IScheduledPlanOperationController
    {
        private string m_MemberID;
        private string m_PlanID;

        public MemberScheduledPlanOperationController(string memberId, string planId)
        {
            m_MemberID = memberId;
            m_PlanID = planId;
        }

        public void AddToAttended()
        {
            TrainingScheduleService.Instance.AddToMemberAttended(m_MemberID, OnUpdateComplete, m_PlanID);
        }

        public void RemoveFromSchedule()
        {
            TrainingScheduleService.Instance.RemoveFromMemberSchedule(m_MemberID, OnUpdateComplete, m_PlanID);
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        private void OnUpdateComplete(bool isSuccess, string message)
        {
            if (!isSuccess)
            {
                Debug.LogWarningFormat("On update member schedule failed: {0}", message);
                ViewContextManager.Instance.Push(new PopupMessageViewContext(string.Format("On update member schedule failed: {0}", message)));
            }
            else
            {
                Debug.LogFormat("On update member schedule success: {0}", message);
                ViewContextManager.Instance.Push(new PopupMessageViewContext("On update member schedule success !!!"));
            }
        }
    }
}
