﻿using VuxtaStudio.GymClient.WebServices.Models;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller
{
    public class MemberAttendedPlanListController : TrainingPlanListControllerBase
    {
        private string m_MemberID;

        public MemberAttendedPlanListController(string id)
        {
            m_MemberID = id;
        }

        protected override void OnItemSelected(TrainingPlan obj, int index)
        {
            base.OnItemSelected(obj, index);
            //TODO: go to next view
        }

        protected override void GetAllRequest()
        {
            TrainingScheduleService.Instance.GetMemberAttendedPlans(m_MemberID, OnGetAllRequestComplete);
        }
    }
}
