﻿using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.TrainingPlanUI.Context;
using VuxtaStudio.GymClient.WebServices.Models;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller
{
    public class MemberScheduledTrainingPlanListController : TrainingPlanListControllerBase
    {
        private string m_MemberID;

        public MemberScheduledTrainingPlanListController(string id)
        {
            m_MemberID = id;
        }

        protected override void OnItemSelected(TrainingPlan obj, int index)
        {
            base.OnItemSelected(obj, index);
            ViewContextManager.Instance.Push(new ScheduledPlanOperationViewContext(obj, new MemberScheduledPlanOperationController(m_MemberID, obj.Id)));
        }

        protected override void GetAllRequest()
        {
            TrainingScheduleService.Instance.GetMemberScheduledPlans(m_MemberID, OnGetAllRequestComplete);
        }
    }
}
