﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VuxtaStudio.Common.UISystems;

namespace VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller
{
    public interface INewSchedulePlanController
    {
        void AddToSchedule();
        void OnBackButtonPressed();
    }
}
