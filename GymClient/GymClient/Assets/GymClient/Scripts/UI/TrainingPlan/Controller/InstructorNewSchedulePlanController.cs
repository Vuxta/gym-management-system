﻿using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.UI.Common.Context;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller
{
    public class InstructorNewSchedulePlanController : INewSchedulePlanController
    {
        private string m_InstructorID;
        private string m_PlanID;

        public InstructorNewSchedulePlanController(string instructorId, string planId)
        {
            m_InstructorID = instructorId;
            m_PlanID = planId;
        }

        public void AddToSchedule()
        {
            TrainingScheduleService.Instance.AddToInstructorSchedule(m_InstructorID, OnUpdateComplete, m_PlanID);
        }

        private void OnUpdateComplete(bool isSuccess, string message)
        {
            if (!isSuccess)
            {
                Debug.LogWarningFormat("On add new instructor schedule failed: {0}", message);
                ViewContextManager.Instance.Push(new PopupMessageViewContext(string.Format("On add new instructor schedule failed: {0}", message)));
            }
            else
            {
                Debug.LogFormat("On add new instructor schedule success: {0}", message);
                ViewContextManager.Instance.Push(new PopupMessageViewContext("Add new instructor schedule success !!!"));
            }
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }
    }
}
