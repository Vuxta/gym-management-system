﻿// <copyright file="MemberAddPhysicalInfoViewContext.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/08/2019 16:00:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 8, 2019

using VuxtaStudio.Common.UISystems;

namespace VuxtaStudio.GymClient.UI.MemberUI.Context
{
    public class MemberAddPhysicalInfoViewContext : BaseViewContext
    {
        private static readonly UIType m_UiType = new UIType("View/Member/MemberAddPhysicalInfoView");

        public readonly string MemberID;

        public MemberAddPhysicalInfoViewContext(string id) : base(m_UiType)
        {
            MemberID = id;
        }
    }
}
