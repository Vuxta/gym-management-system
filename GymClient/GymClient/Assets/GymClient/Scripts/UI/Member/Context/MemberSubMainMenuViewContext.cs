﻿// <copyright file="MemberSubMainMenuViewContext.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/08/2019 16:00:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 8, 2019

using VuxtaStudio.Common.UISystems;

namespace VuxtaStudio.GymClient.UI.MemberUI.Context
{
    /// <summary>
    /// Entry view context after member login
    /// </summary>
    public class MemberSubMainMenuViewContext : BaseViewContext
    {
        private static readonly UIType m_UiType = new UIType("View/Member/MemberSubMainMenuView");

        public MemberSubMainMenuViewContext() : base(m_UiType)
        {
        }
    }
}
