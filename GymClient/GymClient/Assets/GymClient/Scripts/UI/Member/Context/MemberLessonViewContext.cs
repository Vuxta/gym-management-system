﻿using VuxtaStudio.Common.UISystems;

namespace VuxtaStudio.GymClient.UI.MemberUI.Context
{
    public class MemberLessonViewContext : BaseViewContext
    {
        private static readonly UIType m_UiType = new UIType("View/Member/MemberLessonView");

        public MemberLessonViewContext() : base(m_UiType)
        {
        }
    }
}
