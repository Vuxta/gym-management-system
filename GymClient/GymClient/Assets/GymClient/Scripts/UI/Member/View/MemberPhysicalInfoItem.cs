﻿// <copyright file="MemberPhysicalInfoItem.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/08/2019 16:00:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 8, 2019

using System;
using UnityEngine;
using UnityEngine.UI;
using VuxtaStudio.GymClient.WebServices.Models;

namespace VuxtaStudio.GymClient.UI.MemberUI.View
{
    /// <summary>
    /// Scrolable item for showing individual physical info
    /// </summary>
    public class MemberPhysicalInfoItem : MonoBehaviour
    {
        [SerializeField]
        private Text m_DisplayText;

        private PhysicalInfo m_Model;
        private int m_Index;

        public Action<PhysicalInfo, int> OnSelect;

        /// <summary>
        /// Act as constructor for inject model
        /// </summary>
        /// <param name="model"></param>
        public void Init(PhysicalInfo model, int index)
        {
            m_Model = model;
            m_Index = index;
            OnInitDisplay();
        }

        private void OnInitDisplay()
        {
            m_DisplayText.text = string.Format("{0}", m_Model.Date);
        }

        public void OnButtonPressed()
        {
            OnSelect?.Invoke(m_Model, m_Index);
        }
    }
}
