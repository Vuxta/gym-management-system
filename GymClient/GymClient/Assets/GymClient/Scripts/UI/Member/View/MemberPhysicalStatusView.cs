﻿// <copyright file="MemberPhysicalStatusView.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/08/2019 16:00:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 8, 2019

using System;
using VuxtaStudio.Common.UISystems;
using UnityEngine;
using VuxtaStudio.GymClient.UI.MemberUI.Context;
using VuxtaStudio.GymClient.WebServices.Models;
using VuxtaStudio.GymClient.WebServices.Services;
using VuxtaStudio.GymClient.UI.Common.Context;

namespace VuxtaStudio.GymClient.UI.MemberUI.View
{
    /// <summary>
    /// View for showing a list of member physical info and operation menu
    /// </summary>
    public class MemberPhysicalStatusView : AnimateView
    {
        [SerializeField]
        private GridScroller m_GridScroller;

        private string m_MemberID;
        private PhysicalInfo[] m_PhysicalInfos;

        public override void OnEnter(BaseViewContext context)
        {
            base.OnEnter(context);
            m_MemberID = ((MemberPhysicalStatusViewContext)context).MemberID;
            OnRefreshButtonPressed();
        }

        public void OnRefreshButtonPressed()
        {
            GetAllRequest();
        }

        public void OnAddButtonPressed()
        {
            //TODO: go to new data page
            ViewContextManager.Instance.Push(new MemberAddPhysicalInfoViewContext(m_MemberID));
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }

        public void OnChange(Transform trans, int index)
        {
            //trans.GetComponent<HighScoreItem>().Init(index);
            MemberPhysicalInfoItem userListItem = trans.GetComponent<MemberPhysicalInfoItem>();
            userListItem.OnSelect = OnItemSelected;
            userListItem.Init(m_PhysicalInfos[index], index);
        }

        private void OnItemSelected(PhysicalInfo obj, int index)
        {
            //TODO: push to user specific UI
            Debug.LogFormat("Select {0}", m_PhysicalInfos[index].Date);
            ViewContextManager.Instance.Push(new PopupMessageViewContext(string.Format("Date: {0}\nHeight: {1}\nWeight: {2}",
                                                                        m_PhysicalInfos[index].Date,
                                                                        m_PhysicalInfos[index].Height,
                                                                        m_PhysicalInfos[index].Weight)));
        }

        private void GetAllRequest()
        {
            //TODO: use events
            MemberPhysicalStatusService.Instance.GetMemberStatus(m_MemberID, OnGetMemberStatus);
        }

        private void OnGetMemberStatus(bool isSuccess, string messages, PhysicalStatus status)
        {
            m_PhysicalInfos = null;

            if (!isSuccess)
            {
                m_GridScroller.Clear();
                Debug.LogWarningFormat("Fail to get physical datas: {0}", messages);
                ViewContextManager.Instance.Push(new PopupMessageViewContext(
                                                        string.Format("Fail to get all physical datas !!!\n{0}",
                                                        messages)));
                return;
            }

            if (status == null
                || status.PhysicalInfos == null
                || status.PhysicalInfos.Count <= 0)
            {
                m_GridScroller.Clear();
                Debug.LogFormat("No physical info datas: {0}", messages);
                ViewContextManager.Instance.Push(new PopupMessageViewContext("No physical info datas"));
                return;
            }

            m_PhysicalInfos = status.PhysicalInfos.ToArray();

            m_GridScroller.Init(OnChange, m_PhysicalInfos.Length, new Vector2(0.12f, 1f));
        }
    }
}
