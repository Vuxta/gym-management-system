﻿// <copyright file="MemberSubMainMenuView.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/08/2019 16:00:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 8, 2019

using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.Global.Services;
using VuxtaStudio.GymClient.UI.MemberUI.Context;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.MemberUI.View
{
    /// <summary>
    /// Entry view for member after login
    /// </summary>
    public class MemberSubMainMenuView : AnimateView
    {
        public void OnPhysicalStatusButtonPressed()
        {
            ViewContextManager.Instance.Push(new MemberPhysicalStatusViewContext(SessionInfo.Instance.ID));
        }

        public void OnLessonButtonPressed()
        {
            ViewContextManager.Instance.Push(new MemberLessonViewContext());
        }

        public void OnLogoutButtonPressed()
        {
            MemberLoginService.Instance.Logout();
            ViewContextManager.Instance.Pop();
        }
    }
}
