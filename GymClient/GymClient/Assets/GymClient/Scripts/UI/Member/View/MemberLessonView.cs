﻿using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.Global.Services;
using VuxtaStudio.GymClient.UI.TrainingPlanUI.Context;
using VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller;

namespace VuxtaStudio.GymClient.UI.MemberUI.View
{
    public class MemberLessonView : AnimateView
    {
        public void OnNewTrainingButtonPressed()
        {
            ViewContextManager.Instance.Push(new TrainingPlanListViewContext(new MemberNewTrainingPlanListController(SessionInfo.Instance.ID)));
        }

        public void OnMyScheduledLessonsButtonPressed()
        {
            ViewContextManager.Instance.Push(new TrainingPlanListViewContext(new MemberScheduledTrainingPlanListController(SessionInfo.Instance.ID)));
        }

        public void OnAttendedTrainingButtonPressed()
        {
            ViewContextManager.Instance.Push(new TrainingPlanListViewContext(new MemberAttendedPlanListController(SessionInfo.Instance.ID)));
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }
    }
}
