﻿// <copyright file="MemberAddPhysicalInfoView.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>07/08/2019 16:00:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, July 8, 2019

using System;
using VuxtaStudio.Common.UISystems;
using UnityEngine;
using VuxtaStudio.GymClient.UI.MemberUI.Context;
using VuxtaStudio.GymClient.WebServices.Models;
using VuxtaStudio.GymClient.WebServices.Services;
using VuxtaStudio.GymClient.UI.Common.Context;
using UnityEngine.UI;

namespace VuxtaStudio.GymClient.UI.MemberUI.View
{
    /// <summary>
    /// View for member to add physic info and upload
    /// </summary>
    public class MemberAddPhysicalInfoView : AnimateView
    {
        [SerializeField]
        private InputField m_HeightInput;

        [SerializeField]
        private InputField m_WeightInput;

        private string m_MemberID;

        public override void OnEnter(BaseViewContext context)
        {
            base.OnEnter(context);
            m_MemberID = ((MemberAddPhysicalInfoViewContext)context).MemberID;
        }

        public void OnSubmitButtonPressed()
        {
            PhysicalInfo physicalInfo = new PhysicalInfo();

            physicalInfo.Date = DateTime.Now.ToString("yyyyMMddHHmmss");

            if (!double.TryParse(m_HeightInput.text, out physicalInfo.Height))
            {
                ViewContextManager.Instance.Push(new PopupMessageViewContext("Wrong input"));
                return;
            }

            if (!double.TryParse(m_WeightInput.text, out physicalInfo.Weight))
            {
                ViewContextManager.Instance.Push(new PopupMessageViewContext("Wrong input"));
                return;
            }

            MemberPhysicalStatusService.Instance.UploadInfo(m_MemberID, OnUploadFinish, physicalInfo);
        }

        private void OnUploadFinish(bool isSuccess, string message, PhysicalStatus status)
        {
            if (!isSuccess)
            {
                ViewContextManager.Instance.Push(new PopupMessageViewContext(string.Format("Fail to upload info:\n{0}", message)));
                return;
            }

            ViewContextManager.Instance.Push(new PopupMessageViewContext("Upload success"));
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }
    }
}
