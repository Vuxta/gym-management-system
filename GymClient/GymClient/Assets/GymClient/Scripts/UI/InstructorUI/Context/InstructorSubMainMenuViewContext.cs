﻿using VuxtaStudio.Common.UISystems;

namespace VuxtaStudio.GymClient.UI.InstructorUI.Context
{
    public class InstructorSubMainMenuViewContext : BaseViewContext
    {
        private static readonly UIType m_UiType = new UIType("View/Instructor/InstructorSubMainMenuView");

        public InstructorSubMainMenuViewContext() : base(m_UiType)
        {
        }
    }
}
