﻿using VuxtaStudio.Common.UISystems;

namespace VuxtaStudio.GymClient.UI.InstructorUI.Context
{
    public class InstructorLessonViewContext : BaseViewContext
    {
        private static readonly UIType m_UiType = new UIType("View/Instructor/InstructorLessonView");

        public InstructorLessonViewContext() : base(m_UiType)
        {
        }
    }
}
