﻿using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.Global.Services;
using VuxtaStudio.GymClient.UI.InstructorUI.Context;
using VuxtaStudio.GymClient.UI.TrainingPlanUI.Context;
using VuxtaStudio.GymClient.UI.TrainingPlanUI.Controller;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.InstructorUI.View
{
    public class InstructorLessonView : AnimateView
    {
        public void OnNewTrainingButtonPressed()
        {
            ViewContextManager.Instance.Push(new TrainingPlanListViewContext(new InstructorNewTrainingPlanListController(SessionInfo.Instance.ID)));
        }

        public void OnMyScheduledLessonsButtonPressed()
        {
            ViewContextManager.Instance.Push(new TrainingPlanListViewContext(new InstructorScheduledTrainingPlanListController(SessionInfo.Instance.ID)));
        }

        public void OnAttendedTrainingButtonPressed()
        {
            ViewContextManager.Instance.Push(new TrainingPlanListViewContext(new InstructorAttendedPlanListController(SessionInfo.Instance.ID)));
        }

        public void OnBackButtonPressed()
        {
            ViewContextManager.Instance.Pop();
        }
    }
}
