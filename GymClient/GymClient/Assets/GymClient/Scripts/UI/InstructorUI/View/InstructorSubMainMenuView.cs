﻿using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.Global.Services;
using VuxtaStudio.GymClient.UI.InstructorUI.Context;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient.UI.InstructorUI.View
{
    public class InstructorSubMainMenuView : AnimateView
    {
        public void OnLessonButtonPressed()
        {
            ViewContextManager.Instance.Push(new InstructorLessonViewContext());
        }

        public void OnLogoutButtonPressed()
        {
            InstructorLoginService.Instance.Logout();
            ViewContextManager.Instance.Pop();
        }
    }
}
