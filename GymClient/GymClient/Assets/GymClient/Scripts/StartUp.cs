﻿// <copyright file="StartUp.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/24/2019 14:31:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 24, 2019

using UnityEngine;
using VuxtaStudio.Common.UISystems;
using VuxtaStudio.GymClient.Global.Services;
using VuxtaStudio.GymClient.UI.DebugUI.Context;
using VuxtaStudio.GymClient.UI.UserEntry.Context;
using VuxtaStudio.GymClient.UI.UserEntry.Controller;
using VuxtaStudio.GymClient.WebServices.Services;

namespace VuxtaStudio.GymClient
{
    /// <summary>
    /// Init script to run at application start
    /// </summary>
    public class StartUp : MonoBehaviour
    {
        private void Start()
        {
            UIManager.Instance.Create();
            ViewContextManager.Instance.Create();

#if (GYMCLIENT_DEBUG_APP)
            ViewContextManager.Instance.Push(new DebugEntryViewContext());
#elif (GYMCLIENT_ADMIN_APP)
            ViewContextManager.Instance.Push(new UserMainMenuViewContext(new AdminMainMenuController()));
#elif (GYMCLIENT_MEMBER_APP)
            ViewContextManager.Instance.Push(new UserMainMenuViewContext(new MemberMainMenuController()));
#elif (GYMCLIENT_INSTRUCTOR_APP)
            ViewContextManager.Instance.Push(new UserMainMenuViewContext(new InstructorMainMenuController()));
#endif

            //Common services
            SessionInfo.Instance.Setup();
            TrainingScheduleService.Instance.Setup();

            //TODO: defines in different purposes
            //Admin services
#if (GYMCLIENT_ADMIN_APP || GYMCLIENT_DEBUG_APP)
            AdminLoginService.Instance.Setup();
            AdminUserCrudService.Instance.Setup();
            MemberCrudService.Instance.Setup();
            InstructorCrudService.Instance.Setup();
            AdminRegisterService.Instance.Setup();
            MemberRegisterService.Instance.Setup();
            InstructorRegisterService.Instance.Setup();
            CourseCategoryCrudService.Instance.Setup();
            CoursePlanCrudService.Instance.Setup();
            TrainingCategoryCrudService.Instance.Setup();
            TrainingPlanCrudService.Instance.Setup();
#endif

            //Member sevices
#if (GYMCLIENT_MEMBER_APP || GYMCLIENT_DEBUG_APP)
            MemberLoginService.Instance.Setup();
            MemberPhysicalStatusService.Instance.Setup();
#endif

            //Instructor services
#if (GYMCLIENT_INSTRUCTOR_APP || GYMCLIENT_DEBUG_APP)
            InstructorLoginService.Instance.Setup();
#endif
        }
    }
}
