﻿// <copyright file="DateTimeHelper.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/17/2019 16:27:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 17, 2019

using System;
using System.Collections.Generic;
using System.Linq;

namespace VuxtaStudio.Common.Utils
{
    /// <summary>
    /// Indicate date time periods
    /// </summary>
    public class DateTimeRange
    {
        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="start">start date</param>
        /// <param name="end">end date</param>
        public DateTimeRange(DateTime start, DateTime end)
        {
            this.Start = start;
            this.End = end;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="startDateString">string of start date</param>
        /// <param name="endDateString">string of end date</param>
        /// <param name="format">format of representation</param>
        /// <param name="formatProvider">format provider, default for Invariant Culture</param>
        public DateTimeRange(string startDateString,
                            string endDateString,
                            string format,
                            IFormatProvider formatProvider = null)
        {
            if (formatProvider == null)
            {
                formatProvider = System.Globalization.CultureInfo.InvariantCulture;
            }

            Start = DateTime.ParseExact(startDateString, format, formatProvider);
            End = DateTime.ParseExact(endDateString, format, formatProvider);
        }
    }

    /// <summary>
    /// Helper class for date time
    /// </summary>
    public static class DateTimeHelper
    {
        /// <summary>
        /// Check if any overlap of date time
        /// </summary>
        /// <param name="meetings">List of date time</param>
        /// <returns>True if any overlap</returns>
        public static bool DoesNotOverlap(IEnumerable<DateTimeRange> meetings)
        {
            DateTime endPrior = DateTime.MinValue;
            foreach (DateTimeRange meeting in meetings.OrderBy(x => x.Start))
            {
                if (meeting.Start > meeting.End)
                    return false;
                if (meeting.Start < endPrior)
                    return false;
                endPrior = meeting.End;
            }
            return true;
        }
    }
}
