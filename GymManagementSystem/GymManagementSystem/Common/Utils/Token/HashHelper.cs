﻿// <copyright file="HashHelper.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/05/2019 14:12:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 5, 2019

using System;
using System.Security.Cryptography;
using System.Text;

namespace VuxtaStudio.Common.Utils.Token
{
    /// <summary>
    /// Helper class to create hash
    /// </summary>
    public static class HashHelper
    {
        private static string m_Key = "demosalt";

        /// <summary>
        /// Salt for hash generation
        /// </summary>
        /// <param name="key">key for salt</param>
        public static void SetKey(string key)
        {
            m_Key = key;
        }

        /// <summary>
        /// Generate hash base on the input and salt
        /// </summary>
        /// <param name="input">input string</param>
        /// <returns>A hash string</returns>
        public static string ComputeHash(string input)
        {
            string inputString = input + m_Key;

            MD5 md5 = MD5.Create();
            return Convert.ToBase64String(md5.ComputeHash(Encoding.Default.GetBytes(inputString)));
        }
    }
}
