﻿// <copyright file="TokenHelper.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/05/2019 14:12:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 5, 2019

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using JWT;
using JWT.Algorithms;
using JWT.Builder;

namespace VuxtaStudio.Common.Utils.Token
{
    /// <summary>
    /// Helper class for JWT token
    /// </summary>
    public static class TokenHelper
    {
        //move to files afterwards
        private static string m_SecretKey = "demodemodemo";

        /// <summary>
        /// Setup secret key for JWT token generation
        /// </summary>
        /// <param name="key">secret key</param>
        public static void SetKey(string key)
        {
            m_SecretKey = key;
        }

        /// <summary>
        /// Method to generate API Key
        /// </summary>
        /// <param name="userId">user ID</param>
        /// <param name="userPassword">user password</param>
        /// <param name="group">user group</param>
        /// <param name="validate">validate hash</param>
        /// <param name="extras">extra payload</param>
        /// <returns>A string of API Key</returns>
        public static string GenerateApiKey(string userId, string userPassword, string group, string validate, string[] extras)
        {
            string apiKey = new JwtBuilder()
                .WithAlgorithm(new HMACSHA256Algorithm())
                .WithSecret(m_SecretKey)
                .AddClaim(TokenField.UserId, userId)
                .AddClaim(TokenField.Password, userPassword)
                .AddClaim(TokenField.Group, group)
                .AddClaim(TokenField.Validate, validate)
                .AddClaim(TokenField.Extra, extras)
                .Build();
            return apiKey;
        }

        /// <summary>
        /// Generate web API token during login session
        /// </summary>
        /// <param name="userId">user ID</param>
        /// <param name="expireTime">expire time in Epoch</param>
        /// <param name="group">user group</param>
        /// <param name="roles">user roles</param>
        /// <returns>A string of session token</returns>
        public static string GenerateSessionToken(string userId, long expireTime, string group, string ipAddress, long roles)
        {
            string token = new JwtBuilder()
                .WithAlgorithm(new HMACSHA256Algorithm())
                .WithSecret(m_SecretKey)
                .AddClaim(TokenField.UserId, userId)
                .AddClaim(TokenField.Expire, expireTime)
                .AddClaim(TokenField.Group, group)
                .AddClaim(TokenField.IpAddress, ipAddress)
                .AddClaim(TokenField.Roles, roles)
                .Build();
            return token;
        }

        /// <summary>
        /// Decode JWT token to dictionary
        /// </summary>
        /// <param name="key">JWT token</param>
        /// <param name="result">Json result in dictionary structure</param>
        /// <param name="resultMessage">Decoding result message</param>
        /// <returns>Return true if success</returns>
        public static bool DecodeKey(string key, out Dictionary<string, object> result, out string resultMessage)
        {
            result = null;
            resultMessage = string.Empty;

            try
            {
                result = new JwtBuilder()
                    .WithAlgorithm(new HMACSHA256Algorithm())
                    .WithSecret(m_SecretKey)
                    .MustVerifySignature()
                    .Decode<Dictionary<string, object>>(key);

                if (result == null)
                {
                    resultMessage = "Fail to decode json";
                    return false;
                }

                resultMessage = "Decode success";
                return true;
            }
            catch (TokenExpiredException)
            {
                resultMessage = "Token expired";
                return false;
            }
            catch (SignatureVerificationException)
            {
                resultMessage = "Token does not have right signature";
                return false;
            }
            catch (Exception ex)
            {
                resultMessage = string.Format("Token parsing error: {0}", ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Check if all token filed exist and not null
        /// </summary>
        /// <param name="result">Decoded token</param>
        /// <param name="tokenFields">token fields</param>
        /// <returns>Return true if pass</returns>
        public static bool IsAllTokenValid(Dictionary<string, object> result, params string[] tokenFields)
        {
            if (result == null)
                return false;

            if (!tokenFields.All(i => result.ContainsKey(i)))
                return false;

            for (int i = 0; i < tokenFields.Length; i++)
            {
                if (result[tokenFields[i]] == null)
                    return false;
            }

            return true;
        }
    }
}
