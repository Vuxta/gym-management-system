﻿// <copyright file="TokenField.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/19/2019 15:25:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 19, 2019

namespace VuxtaStudio.Common.Utils.Token
{
    public class TokenField
    {
        public const string UserId = "userId";
        public const string Password = "password";
        public const string Group = "group";
        public const string Validate = "validate";
        public const string Extra = "extra";
        public const string Expire = "exp";
        public const string IpAddress = "ipAddress";
        public const string Roles = "roles";
    }
}
