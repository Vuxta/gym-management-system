﻿// <copyright file="JwtAuthBase.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/19/2019 15:25:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 19, 2019

using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using VuxtaStudio.Common.Utils.Token;

namespace VuxtaStudio.Common.Filters
{
    /// <summary>
    /// Base class for JWT authentication filter
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = true)]
    public abstract class JwtAuthBase : ActionFilterAttribute
    {
        /// <summary>
        /// Overide base class when executing actions
        /// Checks for headers, decode token, and verify token
        /// </summary>
        /// <param name="context">Executing context</param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (string.IsNullOrEmpty(context.HttpContext.Request.Headers["Authorization"]))
            {
                setErrorResponse(context, "Missing Authorization headers");
                base.OnActionExecuting(context);
                return;
            }

            AuthenticationHeaderValue auth = AuthenticationHeaderValue.Parse(context.HttpContext.Request.Headers["Authorization"]);

            if (auth == null)
            {
                setErrorResponse(context, "Fail to parse Authorization headers");
                base.OnActionExecuting(context);
                return;
            }

            if (auth.Scheme != "Bearer")
            {
                setErrorResponse(context, "Schema is not bearer");
                base.OnActionExecuting(context);
                return;
            }

            Dictionary<string, object> result = null;
            string resultMessage = string.Empty;

            if (!TokenHelper.DecodeKey(auth.Parameter, out result, out resultMessage))
            {
                setErrorResponse(context, resultMessage);
                base.OnActionExecuting(context);
                return;
            }

            if (!OnDecodeVerify(result, context))
            {
                setErrorResponse(context, "Fail on verification");
                base.OnActionExecuting(context);
                return;
            }

            base.OnActionExecuting(context);
        }

        /// <summary>
        /// Abstract class for verification on decoded tokens
        /// </summary>
        /// <param name="result">Decoded tokens</param>
        /// <param name="actionContext">Executing context</param>
        /// <returns></returns>
        public abstract bool OnDecodeVerify(Dictionary<string, object> result, ActionExecutingContext actionContext);

        /// <summary>
        /// Set error response if any
        /// </summary>
        /// <param name="actionContext">Executing context</param>
        /// <param name="message">Error messages</param>
        private void setErrorResponse(ActionExecutingContext actionContext, string message)
        {
            actionContext.Result = new BadRequestObjectResult(message);
        }
    }
}
