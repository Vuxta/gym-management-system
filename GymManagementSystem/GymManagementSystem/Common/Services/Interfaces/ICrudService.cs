﻿// <copyright file="ICrudService.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/03/2019 10:14:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 3, 2019
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace VuxtaStudio.Common.Services
{
    /// <summary>
    /// Interface for executing CRUD operations
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ICrudService<T>
    {
        List<T> Get();
        T Get(Expression<Func<T, bool>> filter);
        T Create(T item);
        void Update(Expression<Func<T, bool>> filter, T item);
        void Remove(Expression<Func<T, bool>> filter);
    }
}
