﻿// <copyright file="ICrudModel.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/11/2019 17:45:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 11, 2019

namespace VuxtaStudio.Common.Services
{
    /// <summary>
    /// Interface of CrudModel
    /// </summary>
    public interface ICrudModel
    {
        /// <summary>
        /// Called when model is being created
        /// </summary>
        void OnCreateModel();

        /// <summary>
        /// Called when model is being updated
        /// </summary>
        void OnUpdateModel();

        /// <summary>
        /// Called when model is being deleted
        /// </summary>
        void OnDeleteModel();
    }
}
