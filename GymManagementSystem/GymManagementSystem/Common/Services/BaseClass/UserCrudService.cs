﻿// <copyright file="UserCrudService.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/05/2019 14:12:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 5, 2019

using VuxtaStudio.Common.Services.Models;

namespace VuxtaStudio.Common.Services
{
    /// <summary>
    /// Base class for User CRUD service
    /// </summary>
    /// <typeparam name="T">Type of user model</typeparam>
    public abstract class UserCrudService<T> : MongoDbCrudService<T> where T : UserCrudModel
    {
        protected abstract string m_GroupName
        {
            get;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="connectionString">connection string</param>
        /// <param name="databaseName">Database name</param>
        /// <param name="collectionName">Collection name</param>
        public UserCrudService(string connectionString, string databaseName, string collectionName)
            : base(connectionString, databaseName, collectionName)
        {
        }

        /// <summary>
        /// User Group name
        /// </summary>
        /// <returns>A string of group name</returns>
        public string GetGroupName()
        {
            return m_GroupName;
        }
    }
}
