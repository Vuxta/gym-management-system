﻿// <copyright file="MongoDbCrudService.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/03/2019 10:14:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 3, 2019

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using MongoDB.Driver;

namespace VuxtaStudio.Common.Services
{
    /// <summary>
    /// MongoDB CRUD services
    /// </summary>
    /// <typeparam name="T">Type of data to be stored in MongoDB</typeparam>
    public abstract class MongoDbCrudService<T> : ICrudService<T>
    {
        protected MongoClient m_MongoClient;
        protected IMongoDatabase m_MongoDatabase;
        protected IMongoCollection<T> m_Collections;

        /// <summary>
        /// Constructor of MongoDB CRUD service
        /// </summary>
        /// <param name="connectionString">connection strings</param>
        /// <param name="databaseName">name of database</param>
        /// <param name="collectionName">collection name in database</param>
        public MongoDbCrudService(string connectionString, string databaseName, string collectionName)
        {
            m_MongoClient = new MongoClient(connectionString);
            m_MongoDatabase = m_MongoClient.GetDatabase(databaseName);
            m_Collections = m_MongoDatabase.GetCollection<T>(collectionName);
        }

        /// <summary>
        /// Create method of CRUD
        /// </summary>
        /// <param name="item">item to be created</param>
        /// <returns>item to be created</returns>
        public T Create(T item)
        {
            m_Collections.InsertOne(item);
            return item;
        }

        /// <summary>
        /// Get list of collections
        /// </summary>
        /// <returns>List of collections</returns>
        public List<T> Get()
        {
            return m_Collections.Find(item => true).ToList();
        }

        /// <summary>
        /// Get item in collection by the filter
        /// </summary>
        /// <param name="filter">filter use for searching</param>
        /// <returns>Item found</returns>
        public T Get(Expression<Func<T, bool>> filter)
        {
            return m_Collections.Find<T>(filter).FirstOrDefault();
        }

        /// <summary>
        /// Get items by mongo db filters
        /// </summary>
        /// <param name="filter">MongoDb filter</param>
        /// <returns></returns>
        public List<T> GetByMongoDbFilter(FilterDefinition<T> filter)
        {
            return m_Collections.Find<T>(filter).ToList();
        }

        /// <summary>
        /// Delete item in collection by filter
        /// </summary>
        /// <param name="filter">filter use for searching</param>
        public void Remove(Expression<Func<T, bool>> filter)
        {
            m_Collections.DeleteOne(filter);
        }

        /// <summary>
        /// Update item in collection by filter
        /// </summary>
        /// <param name="filter">filter use for searching</param>
        /// <param name="item">item use for replacing</param>
        public void Update(Expression<Func<T, bool>> filter, T item)
        {
            m_Collections.ReplaceOne(filter, item);
        }
    }
}
