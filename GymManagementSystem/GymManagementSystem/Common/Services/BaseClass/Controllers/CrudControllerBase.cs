﻿// <copyright file="CrudControllerBase.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/11/2019 17:45:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 11, 2019

using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using VuxtaStudio.Common.Services.Models;

namespace VuxtaStudio.Common.Services.Controllers
{
    /// <summary>
    /// CRUD controller base class
    /// </summary>
    /// <typeparam name="T">Type of CrudModelBase</typeparam>
    public abstract class CrudControllerBase<T> : ControllerBase where T : CrudModelBase
    {
        protected readonly ICrudService<T> m_CrudService;

        public CrudControllerBase(ICrudService<T> crudService)
        {
            m_CrudService = crudService;
        }

        /// <summary>
        /// Get list of collection
        /// </summary>
        /// <returns>List of collection</returns>
        [HttpGet]
        public virtual ActionResult<List<T>> Get()
        {
            return m_CrudService.Get();
        }

        /// <summary>
        /// Get collection info by ID
        /// </summary>
        /// <param name="id">collection ID</param>
        /// <returns>collection infos</returns>
        [HttpGet("{id:length(24)}")]
        public virtual ActionResult<T> Get(string id)
        {
            var result = m_CrudService.Get(item => item.Id == id);

            if (result == null)
            {
                return NotFound();
            }

            return result;
        }

        /// <summary>
        /// Create new collection
        /// </summary>
        /// <param name="data">collection data to be created</param>
        /// <returns>Return created item</returns>
        [HttpPost]
        public virtual ActionResult<T> Create(T data)
        {
            if (!string.IsNullOrEmpty(data.Id))
            {
                var result = m_CrudService.Get(item => item.Id == data.Id);

                if (result != null)
                {
                    return BadRequest("ID is used");
                }
            }

            data.OnCreateModel();
            m_CrudService.Create(data);

            return CreatedAtRoute(new { id = data.Id }, data);
        }

        /// <summary>
        /// Update collection data by ID
        /// </summary>
        /// <param name="id">ID of collection</param>
        /// <param name="data">Data to be replaced</param>
        /// <returns></returns>
        [HttpPut("{id:length(24)}")]
        public virtual IActionResult Update(string id, T data)
        {
            var book = m_CrudService.Get(item => item.Id == id);

            if (book == null)
            {
                return NotFound();
            }

            data.OnUpdateModel();
            m_CrudService.Update(item => item.Id == id, data);

            return NoContent();
        }

        /// <summary>
        /// Delete by collection by ID
        /// </summary>
        /// <param name="id">ID of collection</param>
        /// <returns></returns>
        [HttpDelete("{id:length(24)}")]
        public virtual IActionResult Delete(string id)
        {
            var data = m_CrudService.Get(item => item.Id == id);

            if (data == null)
            {
                return NotFound();
            }

            data.OnDeleteModel();
            m_CrudService.Remove(item => item.Id == id);

            return NoContent();
        }
    }
}
