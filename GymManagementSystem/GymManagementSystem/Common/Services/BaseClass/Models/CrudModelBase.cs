﻿// <copyright file="CrudModelBase.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/11/2019 17:45:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 11, 2019

using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace VuxtaStudio.Common.Services.Models
{
    /// <summary>
    /// Base class of CRUD model
    /// </summary>
    public abstract class CrudModelBase : ICrudModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        /// <summary>
        /// Called when model is being created
        /// </summary>
        public virtual void OnCreateModel() { }

        /// <summary>
        /// Called when model is being deleted
        /// </summary>
        public virtual void OnDeleteModel() { }

        /// <summary>
        /// Called when model is being updated
        /// </summary>
        public virtual void OnUpdateModel() { }
    }
}
