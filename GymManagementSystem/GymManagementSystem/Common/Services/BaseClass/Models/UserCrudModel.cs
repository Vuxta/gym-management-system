﻿// <copyright file="UserCrudModel.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/05/2019 14:12:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 5, 2019

using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace VuxtaStudio.Common.Services.Models
{
    /// <summary>
    /// User CRUD model base
    /// </summary>
    public abstract class UserCrudModel : CrudModelBase
    {
        [BsonElement("UserName")]
        public string UserName;

        [BsonElement("RegisteredMail")]
        public string RegisteredMail;

        [BsonElement("RegisteredPhone")]
        public string RegisteredPhone;

        [BsonElement("Roles")]
        public long Roles;

        [BsonElement("Status")]
        public string Status;

        [BsonElement("JoinDate")]
        public string JoinDate;

        [BsonElement("RegisterDate")]
        public string RegisterDate;

        /// <summary>
        /// Generate hash candidate string
        /// </summary>
        /// <returns>A candidate string that later will be used to generate hash</returns>
        public string GetHashCandidate()
        {
            return Id + JoinDate + RegisterDate;
        }

        public override void OnCreateModel()
        {
            JoinDate = DateTime.Now.ToString("yyyyMMddHHmmss");
        }
    }
}
