﻿// <copyright file="InstructorCrudService.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/10/2019 11:49:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 10, 2019

using Microsoft.Extensions.Configuration;
using VuxtaStudio.Common.Services;
using VuxtaStudio.GymManagementSystem.Global.Defines;
using VuxtaStudio.GymManagementSystem.InstructorServices.Models;

namespace VuxtaStudio.GymManagementSystem.InstructorServices.Services
{
    /// <summary>
    /// Instructor CRUD service
    /// </summary>
    public class InstructorCrudService : UserCrudService<Instructor>
    {
        protected override string m_GroupName
        {
            get
            {
                return GroupName.Instructor;
            }
        }

        /// <summary>
        /// Constructor of Instructor CRUD service
        /// </summary>
        /// <param name="config">Configuration passed when StartUp</param>
        public InstructorCrudService(IConfiguration config)
            : base(config.GetConnectionString("InstructorDb"), "InstructorDb", "Instructor")
        {
        }
    }
}
