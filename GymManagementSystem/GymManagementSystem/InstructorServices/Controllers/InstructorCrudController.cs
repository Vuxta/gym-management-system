﻿// <copyright file="InstructorCrudController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/10/2019 11:49:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 10, 2019

using Microsoft.AspNetCore.Mvc;
using VuxtaStudio.Common.Services.Controllers;
using VuxtaStudio.GymManagementSystem.Global.Defines;
using VuxtaStudio.GymManagementSystem.Global.Filters;
using VuxtaStudio.GymManagementSystem.InstructorServices.Models;
using VuxtaStudio.GymManagementSystem.InstructorServices.Services;

namespace VuxtaStudio.GymManagementSystem.InstructorServices.Controllers
{
    /// <summary>
    /// Controller of instructor CRUD web API
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [JwtUserAuthActionFilter(AllowAdmin = true, AdminRoles = AdminRoles.InstructorManager)]
    public class InstructorCrudController : CrudControllerBase<Instructor>
    {
        private readonly InstructorCrudService m_InstructorCrudService;

        /// <summary>
        /// Constructor of controller
        /// </summary>
        /// <param name="instructorCrudService">CRUD services</param>
        public InstructorCrudController(InstructorCrudService instructorCrudService)
            : base(instructorCrudService)
        {
            m_InstructorCrudService = instructorCrudService;
        }
    }
}
