﻿// <copyright file="InstructorTrainingSchedule.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/12/2019 11:40:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 12, 2019

using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using VuxtaStudio.Common.Services.Models;

namespace VuxtaStudio.GymManagementSystem.TrainingServices.Models
{
    /// <summary>
    /// Instructor Training Schedule CRUD model, The represented ID should be the same in Instructor Model ID
    /// This will store all the scheduled and attended training plan IDs
    /// Later these IDs will be used as key for searching
    /// </summary>
    public class InstructorTrainingSchedule : CrudModelBase
    {
        [BsonElement("ScheduledTrainingPlanIds")]
        public List<string> ScheduledTrainingPlanIds { get; set; }

        [BsonElement("AttendedTrainingPlanIds")]
        public List<string> AttendedTrainingPlanIds { get; set; }
    }
}
