﻿// <copyright file="CoursePlan.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/12/2019 11:40:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 12, 2019

using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using VuxtaStudio.Common.Services.Models;

namespace VuxtaStudio.GymManagementSystem.TrainingServices.Models
{
    /// <summary>
    /// Course Plan CRUD model, which could be the parent set of all Training Plans
    /// It will also store a list of Training Plan ID which will be used for later searching
    /// </summary>
    public class CoursePlan : CrudModelBase
    {
        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("CourseCategoryId")]
        public string CourseCategoryId { get; set; }

        [BsonElement("StartDate")]
        public string StartDate { get; set; }

        [BsonElement("EndDate")]
        public string EndDate { get; set; }

        [BsonElement("Place")]
        public string Place { get; set; }

        [BsonElement("Description")]
        public string Description { get; set; }

        [BsonElement("TrainingPlanIds")]
        public List<string> TrainingPlanIds { get; set; }
    }
}
