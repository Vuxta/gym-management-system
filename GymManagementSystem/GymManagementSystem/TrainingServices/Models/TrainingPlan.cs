﻿// <copyright file="TrainingPlan.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/12/2019 11:40:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 12, 2019

using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using VuxtaStudio.Common.Services.Models;

namespace VuxtaStudio.GymManagementSystem.TrainingServices.Models
{
    /// <summary>
    /// Training plan CRUD model which stores one individual lesson info
    /// Least subset of entire training model database
    /// </summary>
    public class TrainingPlan : CrudModelBase
    {
        [BsonElement("Name")]
        public string Name { get; set; }

        [BsonElement("TrainingCategoryId")]
        public string TrainingCategoryId { get; set; }

        [BsonElement("StartDate")]
        public string StartDate { get; set; }

        [BsonElement("EndDate")]
        public string EndDate { get; set; }

        [BsonElement("Place")]
        public string Place { get; set; }

        [BsonElement("Description")]
        public string Description { get; set; }

        [BsonElement("AssignedInstructorIds")]
        public List<string> AssignedInstructorIds { get; set; }

        [BsonElement("ScheduledMemberIds")]
        public List<string> ScheduledMemberIds { get; set; }

        [BsonElement("AttendedInstructorIds")]
        public List<string> AttendedInstructorIds { get; set; }

        [BsonElement("AttendedMemberIds")]
        public List<string> AttendedMemberIds { get; set; }
    }
}
