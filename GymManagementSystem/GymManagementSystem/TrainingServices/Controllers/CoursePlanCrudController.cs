﻿// <copyright file="CoursePlanCrudController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/12/2019 11:40:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 12, 2019

using Microsoft.AspNetCore.Mvc;
using VuxtaStudio.Common.Services.Controllers;
using VuxtaStudio.GymManagementSystem.Global.Defines;
using VuxtaStudio.GymManagementSystem.Global.Filters;
using VuxtaStudio.GymManagementSystem.TrainingServices.Models;
using VuxtaStudio.GymManagementSystem.TrainingServices.Services;

namespace VuxtaStudio.GymManagementSystem.TrainingServices.Controllers
{
    /// <summary>
    /// Course plan CRUD controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [JwtUserAuthActionFilter(AllowAdmin = true, AdminRoles = AdminRoles.InstructorManager)]
    public class CoursePlanCrudController : CrudControllerBase<CoursePlan>
    {
        private readonly CoursePlanCrudService m_CoursePlanCrudService;

        /// <summary>
        /// Constructor of controller
        /// </summary>
        /// <param name="coursePlanCrudService">CRUD services</param>
        public CoursePlanCrudController(CoursePlanCrudService coursePlanCrudService)
            : base(coursePlanCrudService)
        {
            m_CoursePlanCrudService = coursePlanCrudService;
        }
    }
}
