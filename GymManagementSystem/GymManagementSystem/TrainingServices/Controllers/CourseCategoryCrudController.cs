﻿// <copyright file="CourseCategoryCrudController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/12/2019 11:40:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 12, 2019

using Microsoft.AspNetCore.Mvc;
using VuxtaStudio.Common.Services.Controllers;
using VuxtaStudio.GymManagementSystem.Global.Defines;
using VuxtaStudio.GymManagementSystem.Global.Filters;
using VuxtaStudio.GymManagementSystem.TrainingServices.Models;
using VuxtaStudio.GymManagementSystem.TrainingServices.Services;

namespace VuxtaStudio.GymManagementSystem.TrainingServices.Controllers
{
    /// <summary>
    /// Course category CRUD controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [JwtUserAuthActionFilter(AllowAdmin = true, AdminRoles = AdminRoles.InstructorManager)]
    public class CourseCategoryCrudController : CrudControllerBase<CourseCategory>
    {
        private readonly CourseCategoryCrudService m_CourseCategoryCrudService;

        /// <summary>
        /// Constructor of controller
        /// </summary>
        /// <param name="courseCategoryCrudService">CRUD services</param>
        public CourseCategoryCrudController(CourseCategoryCrudService courseCategoryCrudService)
            : base(courseCategoryCrudService)
        {
            m_CourseCategoryCrudService = courseCategoryCrudService;
        }
    }
}
