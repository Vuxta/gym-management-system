﻿// <copyright file="TrainingScheduleController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/17/2019 16:27:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 17, 2019

using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using VuxtaStudio.GymManagementSystem.Global.Defines;
using VuxtaStudio.GymManagementSystem.Global.Filters;
using VuxtaStudio.GymManagementSystem.TrainingServices.Models;
using VuxtaStudio.GymManagementSystem.TrainingServices.Services;

namespace VuxtaStudio.GymManagementSystem.TrainingServices.Controllers
{
    /// <summary>
    /// Controller for instructor/member schedule/attended assigning
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class TrainingScheduleController : ControllerBase
    {
        private TrainingScheduleMasterService m_TrainingScheduleService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="trainingScheduleMasterService">Schedule master service</param>
        public TrainingScheduleController(TrainingScheduleMasterService trainingScheduleMasterService)
        {
            m_TrainingScheduleService = trainingScheduleMasterService;
        }

        /// <summary>
        /// Get all schedules of all members
        /// </summary>
        /// <returns>All memebers schedule</returns>
        [HttpGet]
        [Route("MemberSchedule")]
        [JwtUserAuthActionFilter(AllowAdmin = true, AdminRoles = AdminRoles.MemberManager)]
        public ActionResult<List<MemberTrainingSchedule>> GetAllMemberSchedule()
        {
            return m_TrainingScheduleService.MemberTrainingScheduleService.Get();
        }

        /// <summary>
        /// Return a schedule of specific member
        /// </summary>
        /// <param name="id">ID of member</param>
        /// <returns>Member's schedule</returns>
        [HttpGet]
        [Route("MemberSchedule/{id:length(24)}")]
        [JwtUserAuthActionFilter(AllowAdmin = true,
                                AllowMember = true,
                                AdminRoles = AdminRoles.MemberManager,
                                MemberRoles = MemberRoles.Normal,
                                CheckMemberIdRouteMatch = true)]
        public ActionResult<MemberTrainingSchedule> GetMemberSchedule(string id)
        {
            MemberTrainingSchedule trainingSchedule = m_TrainingScheduleService.MemberTrainingScheduleService.Get(item => item.Id == id);

            if (trainingSchedule == null)
            {
                return NotFound();
            }

            return trainingSchedule;
        }

        /// <summary>
        /// Add scheduled plan id to member
        /// </summary>
        /// <param name="id">ID of member</param>
        /// <param name="data">scheduled plan ids</param>
        /// <returns>Return member's schedule</returns>
        [HttpPut]
        [Route("MemberSchedule/{id:length(24)}/AddSchedule")]
        [JwtUserAuthActionFilter(AllowAdmin = true,
                                AllowMember = true,
                                AdminRoles = AdminRoles.MemberManager,
                                MemberRoles = MemberRoles.Normal,
                                CheckMemberIdRouteMatch = true)]
        public ActionResult<MemberTrainingSchedule> AddToMemberSchedule(string id, string[] data)
        {
            string errorMessage = string.Empty;
            if (!m_TrainingScheduleService.AddScheduledPlanToMember(id, out errorMessage, data))
                return BadRequest(errorMessage);

            return m_TrainingScheduleService.MemberTrainingScheduleService.Get(item => item.Id == id);
        }

        /// <summary>
        /// Remove scheduled pla ID from member
        /// </summary>
        /// <param name="id">ID of member</param>
        /// <param name="data">ID of training plans</param>
        /// <returns>Updated member's schedule</returns>
        [HttpPut]
        [Route("MemberSchedule/{id:length(24)}/RemoveSchedule")]
        [JwtUserAuthActionFilter(AllowAdmin = true,
                                AllowMember = true,
                                AdminRoles = AdminRoles.MemberManager,
                                MemberRoles = MemberRoles.Normal,
                                CheckMemberIdRouteMatch = true)]
        public ActionResult<MemberTrainingSchedule> RemoveFromMemberSchedule(string id, string[] data)
        {
            m_TrainingScheduleService.RemoveScheduledPlanFromMember(id, data);

            return m_TrainingScheduleService.MemberTrainingScheduleService.Get(item => item.Id == id);
        }

        /// <summary>
        /// Add attended plan to member
        /// </summary>
        /// <param name="id">ID of member</param>
        /// <param name="data">attended plan IDs</param>
        /// <returns>Updated member's schedule</returns>
        [HttpPut]
        [Route("MemberSchedule/{id:length(24)}/AddAttended")]
        [JwtUserAuthActionFilter(AllowAdmin = true,
                                AllowMember = true,
                                AdminRoles = AdminRoles.MemberManager,
                                MemberRoles = MemberRoles.Normal,
                                CheckMemberIdRouteMatch = true)]
        public ActionResult<MemberTrainingSchedule> AddToMemberAttended(string id, string[] data)
        {
            string errorMessage = string.Empty;
            if (!m_TrainingScheduleService.AddAttendedPlanToMember(id, out errorMessage, data))
                return BadRequest(errorMessage);

            return m_TrainingScheduleService.MemberTrainingScheduleService.Get(item => item.Id == id);
        }

        /// <summary>
        /// Remove attended plan ID from member
        /// </summary>
        /// <param name="id">ID of member</param>
        /// <param name="data">ID of attended plans</param>
        /// <returns>Updated schedule of member</returns>
        [HttpPut]
        [Route("MemberSchedule/{id:length(24)}/RemoveAttended")]
        [JwtUserAuthActionFilter(AllowAdmin = true,
                                AllowMember = true,
                                AdminRoles = AdminRoles.MemberManager,
                                MemberRoles = MemberRoles.Normal,
                                CheckMemberIdRouteMatch = true)]
        public ActionResult<MemberTrainingSchedule> RemoveFromMemberAttended(string id, string[] data)
        {
            m_TrainingScheduleService.RemoveAttendedPlanFromMember(id, data);

            return m_TrainingScheduleService.MemberTrainingScheduleService.Get(item => item.Id == id);
        }

        /// <summary>
        /// All instructor's schedule
        /// </summary>
        /// <returns>All instructors' schedule</returns>
        [HttpGet]
        [Route("InstructorSchedule")]
        [JwtUserAuthActionFilter(AllowAdmin = true, AdminRoles = AdminRoles.InstructorManager)]
        public ActionResult<List<InstructorTrainingSchedule>> GetAllInstructorSchedule()
        {
            return m_TrainingScheduleService.InstructorTrainingScheduleService.Get();
        }

        /// <summary>
        /// Get specific instructor schedule
        /// </summary>
        /// <param name="id">ID of instructor</param>
        /// <returns>Instructor's schedule</returns>
        [HttpGet]
        [Route("InstructorSchedule/{id:length(24)}")]
        [JwtUserAuthActionFilter(AllowAdmin = true,
                                AllowInstructor = true,
                                AdminRoles = AdminRoles.InstructorManager,
                                InstructorRoles = InstructorRoles.Normal,
                                CheckInstructorIdRouteMatch = true)]
        public ActionResult<InstructorTrainingSchedule> GetInstructorSchedule(string id)
        {
            InstructorTrainingSchedule trainingSchedule = m_TrainingScheduleService.InstructorTrainingScheduleService.Get(item => item.Id == id);

            if (trainingSchedule == null)
            {
                return NotFound();
            }

            return trainingSchedule;
        }

        /// <summary>
        /// Add to instructor schedule
        /// </summary>
        /// <param name="id">ID of instructor</param>
        /// <param name="data">plan IDs</param>
        /// <returns>Updated instructor schedule</returns>
        [HttpPut]
        [Route("InstructorSchedule/{id:length(24)}/AddSchedule")]
        [JwtUserAuthActionFilter(AllowAdmin = true,
                                AllowInstructor = true,
                                AdminRoles = AdminRoles.InstructorManager,
                                InstructorRoles = InstructorRoles.Normal,
                                CheckInstructorIdRouteMatch = true)]
        public ActionResult<InstructorTrainingSchedule> AddToInstructorSchedule(string id, string[] data)
        {
            string errorMessage = string.Empty;
            if (!m_TrainingScheduleService.AddScheduledPlanToInstructor(id, out errorMessage, data))
                return BadRequest(errorMessage);

            return m_TrainingScheduleService.InstructorTrainingScheduleService.Get(item => item.Id == id);
        }

        /// <summary>
        /// Remove from instructor schedule
        /// </summary>
        /// <param name="id">ID of instructor</param>
        /// <param name="data">plan IDs</param>
        /// <returns>Updated instructor schedule</returns>
        [HttpPut]
        [Route("InstructorSchedule/{id:length(24)}/RemoveSchedule")]
        [JwtUserAuthActionFilter(AllowAdmin = true,
                                AllowInstructor = true,
                                AdminRoles = AdminRoles.InstructorManager,
                                InstructorRoles = InstructorRoles.Normal,
                                CheckInstructorIdRouteMatch = true)]
        public ActionResult<InstructorTrainingSchedule> RemoveFromInstructorSchedule(string id, string[] data)
        {
            m_TrainingScheduleService.RemoveScheduledPlanFromInstructor(id, data);

            return m_TrainingScheduleService.InstructorTrainingScheduleService.Get(item => item.Id == id);
        }

        /// <summary>
        /// Add to instructor attended record
        /// </summary>
        /// <param name="id">ID of instructor</param>
        /// <param name="data">plan IDs</param>
        /// <returns>Updated instructor schedule</returns>
        [HttpPut]
        [Route("InstructorSchedule/{id:length(24)}/AddAttended")]
        [JwtUserAuthActionFilter(AllowAdmin = true,
                                AllowInstructor = true,
                                AdminRoles = AdminRoles.InstructorManager,
                                InstructorRoles = InstructorRoles.Normal,
                                CheckInstructorIdRouteMatch = true)]
        public ActionResult<InstructorTrainingSchedule> AddToInstructorAttended(string id, string[] data)
        {
            string errorMessage = string.Empty;
            if (!m_TrainingScheduleService.AddAttendedPlanToInstructor(id, out errorMessage, data))
                return BadRequest(errorMessage);

            return m_TrainingScheduleService.InstructorTrainingScheduleService.Get(item => item.Id == id);
        }

        /// <summary>
        /// Remove from instructor attended record
        /// </summary>
        /// <param name="id">ID of instructor</param>
        /// <param name="data">plan IDs</param>
        /// <returns>Updated instructor schedule</returns>
        [HttpPut]
        [Route("InstructorSchedule/{id:length(24)}/RemoveAttended")]
        [JwtUserAuthActionFilter(AllowAdmin = true,
                                AllowInstructor = true,
                                AdminRoles = AdminRoles.InstructorManager,
                                InstructorRoles = InstructorRoles.Normal,
                                CheckInstructorIdRouteMatch = true)]
        public ActionResult<InstructorTrainingSchedule> RemoveFromInstructorAttended(string id, string[] data)
        {
            m_TrainingScheduleService.RemoveAttendedPlanFromInstructor(id, data);

            return m_TrainingScheduleService.InstructorTrainingScheduleService.Get(item => item.Id == id);
        }

        [HttpGet]
        [Route("MemberSchedule/{id:length(24)}/ScheduledPlans")]
        [JwtUserAuthActionFilter(AllowAdmin = true,
                                AllowMember = true,
                                AdminRoles = AdminRoles.MemberManager,
                                MemberRoles = MemberRoles.Normal,
                                CheckMemberIdRouteMatch = true)]
        public ActionResult<List<TrainingPlan>> GetMemberScheduledPlans(string id)
        {
            MemberTrainingSchedule trainingSchedule = m_TrainingScheduleService.MemberTrainingScheduleService.Get(item => item.Id == id);

            if (trainingSchedule == null
                || trainingSchedule.ScheduledTrainingPlanIds == null
                || trainingSchedule.ScheduledTrainingPlanIds.Count <= 0)
            {
                return BadRequest("No scheduled plans exist yet");
            }

            List<TrainingPlan> trainingPlansFound = null;
            if (!m_TrainingScheduleService.GetExistTrainingPlans(out trainingPlansFound, trainingSchedule.ScheduledTrainingPlanIds.ToArray()))
            {
                return BadRequest("Training plans not found");
            }

            return trainingPlansFound;
        }

        [HttpGet]
        [Route("MemberSchedule/{id:length(24)}/AttendedPlans")]
        [JwtUserAuthActionFilter(AllowAdmin = true,
                        AllowMember = true,
                        AdminRoles = AdminRoles.MemberManager,
                        MemberRoles = MemberRoles.Normal,
                        CheckMemberIdRouteMatch = true)]
        public ActionResult<List<TrainingPlan>> GetMemberAttendedPlans(string id)
        {
            MemberTrainingSchedule trainingSchedule = m_TrainingScheduleService.MemberTrainingScheduleService.Get(item => item.Id == id);

            if (trainingSchedule == null
                || trainingSchedule.AttendedTrainingPlanIds == null
                || trainingSchedule.AttendedTrainingPlanIds.Count <= 0)
            {
                return BadRequest("No attended plans exist yet");
            }

            List<TrainingPlan> trainingPlansFound = null;
            if (!m_TrainingScheduleService.GetExistTrainingPlans(out trainingPlansFound, trainingSchedule.AttendedTrainingPlanIds.ToArray()))
            {
                return BadRequest("Training plans not found");
            }

            return trainingPlansFound;
        }

        [HttpGet]
        [Route("InstructorSchedule/{id:length(24)}/ScheduledPlans")]
        [JwtUserAuthActionFilter(AllowAdmin = true,
                        AllowInstructor = true,
                        AdminRoles = AdminRoles.InstructorManager,
                        InstructorRoles = InstructorRoles.Normal,
                        CheckInstructorIdRouteMatch = true)]
        public ActionResult<List<TrainingPlan>> GetInstructorScheduledPlans(string id)
        {
            InstructorTrainingSchedule trainingSchedule = m_TrainingScheduleService.InstructorTrainingScheduleService.Get(item => item.Id == id);

            if (trainingSchedule == null
                || trainingSchedule.ScheduledTrainingPlanIds == null
                || trainingSchedule.ScheduledTrainingPlanIds.Count <= 0)
            {
                return BadRequest("No scheduled plans exist yet");
            }

            List<TrainingPlan> trainingPlansFound = null;
            if (!m_TrainingScheduleService.GetExistTrainingPlans(out trainingPlansFound, trainingSchedule.ScheduledTrainingPlanIds.ToArray()))
            {
                return BadRequest("Training plans not found");
            }

            return trainingPlansFound;
        }

        [HttpGet]
        [Route("InstructorSchedule/{id:length(24)}/AttendedPlans")]
        [JwtUserAuthActionFilter(AllowAdmin = true,
                AllowInstructor = true,
                AdminRoles = AdminRoles.InstructorManager,
                InstructorRoles = InstructorRoles.Normal,
                CheckInstructorIdRouteMatch = true)]
        public ActionResult<List<TrainingPlan>> GetInstructorAttendedPlans(string id)
        {
            InstructorTrainingSchedule trainingSchedule = m_TrainingScheduleService.InstructorTrainingScheduleService.Get(item => item.Id == id);

            if (trainingSchedule == null
                || trainingSchedule.AttendedTrainingPlanIds == null
                || trainingSchedule.AttendedTrainingPlanIds.Count <= 0)
            {
                return BadRequest("No attended plans exist yet");
            }

            List<TrainingPlan> trainingPlansFound = null;
            if (!m_TrainingScheduleService.GetExistTrainingPlans(out trainingPlansFound, trainingSchedule.AttendedTrainingPlanIds.ToArray()))
            {
                return BadRequest("Training plans not found");
            }

            return trainingPlansFound;
        }

        [HttpGet]
        [Route("TrainingPlans")]
        [JwtUserAuthActionFilter(AllowAdmin = true,
            AllowInstructor = true,
            AllowMember = true,
            AdminRoles = AdminRoles.InstructorManager | AdminRoles.MemberManager,
            InstructorRoles = InstructorRoles.Normal,
            MemberRoles = MemberRoles.Normal,
            CheckMemberIdRouteMatch = false,
            CheckInstructorIdRouteMatch = false)]
        public ActionResult<List<TrainingPlan>> GetAllTrainingPlans()
        {
            return m_TrainingScheduleService.TrainingPlanService.Get();
        }
    }
}
