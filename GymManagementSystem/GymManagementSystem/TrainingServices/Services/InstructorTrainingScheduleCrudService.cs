﻿// <copyright file="InstructorTrainingScheduleCrudService.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/12/2019 11:40:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 12, 2019

using Microsoft.Extensions.Configuration;
using VuxtaStudio.Common.Services;
using VuxtaStudio.GymManagementSystem.TrainingServices.Models;

namespace VuxtaStudio.GymManagementSystem.TrainingServices.Services
{
    public class InstructorTrainingScheduleCrudService : MongoDbCrudService<InstructorTrainingSchedule>
    {
        public InstructorTrainingScheduleCrudService(IConfiguration config)
            : base(config.GetConnectionString("CourseDb"), "CourseDb", "InstructorTrainingSchedule")
        {
        }
    }
}
