﻿// <copyright file="TrainingScheduleMasterService.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/17/2019 16:27:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 17, 2019

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using VuxtaStudio.Common.Utils;
using VuxtaStudio.GymManagementSystem.InstructorServices.Models;
using VuxtaStudio.GymManagementSystem.InstructorServices.Services;
using VuxtaStudio.GymManagementSystem.MemberServices.Models;
using VuxtaStudio.GymManagementSystem.MemberServices.Services;
using VuxtaStudio.GymManagementSystem.TrainingServices.Models;

namespace VuxtaStudio.GymManagementSystem.TrainingServices.Services
{
    /// <summary>
    /// Master service of training schedule and attended record handling
    /// </summary>
    public class TrainingScheduleMasterService
    {
        public readonly MemberTrainingScheduleCrudService MemberTrainingScheduleService;
        public readonly InstructorTrainingScheduleCrudService InstructorTrainingScheduleService;
        public readonly TrainingPlanCrudService TrainingPlanService;
        public readonly MemberCrudService MemberService;
        public readonly InstructorCrudService InstructorService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="config">configs</param>
        public TrainingScheduleMasterService(IConfiguration config)
        {
            this.MemberTrainingScheduleService = new MemberTrainingScheduleCrudService(config);
            this.InstructorTrainingScheduleService = new InstructorTrainingScheduleCrudService(config);
            this.TrainingPlanService = new TrainingPlanCrudService(config);
            this.MemberService = new MemberCrudService(config);
            this.InstructorService = new InstructorCrudService(config);
        }

        /// <summary>
        /// Check if member ID exist
        /// </summary>
        /// <param name="memberId">member ID</param>
        /// <returns>True if exist</returns>
        public bool IsMemberExist(string memberId)
        {
            Member memberData = MemberService.Get(item => item.Id == memberId);
            return (memberData != null);
        }

        /// <summary>
        /// Check if instructor exist
        /// </summary>
        /// <param name="instructorId">Instructor ID</param>
        /// <returns>True if exist</returns>
        public bool IsInstructorExist(string instructorId)
        {
            Instructor instructorData = InstructorService.Get(item => item.Id == instructorId);
            return (instructorData != null);
        }

        /// <summary>
        /// Check if plan exist
        /// </summary>
        /// <param name="planId">Plan ID</param>
        /// <returns>True if exist</returns>
        public bool IsTrainingPlanExist(string planId)
        {
            TrainingPlan trainingPlan = TrainingPlanService.Get(item => item.Id == planId);
            return (trainingPlan != null);
        }

        /// <summary>
        /// Get all exist training plan by IDs
        /// </summary>
        /// <param name="trainingPlans">A list training plan found</param>
        /// <param name="planIds">ID to search</param>
        /// <returns>True if there is any plan found</returns>
        public bool GetExistTrainingPlans(out List<TrainingPlan> trainingPlans, params string[] planIds)
        {
            FilterDefinitionBuilder<TrainingPlan> filterBuilder = new FilterDefinitionBuilder<TrainingPlan>();
            FilterDefinition<TrainingPlan> filter = filterBuilder.In(item => item.Id, planIds);
            trainingPlans = TrainingPlanService.GetByMongoDbFilter(filter);

            if (trainingPlans == null || trainingPlans.Count <= 0)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Query for member schedule
        /// </summary>
        /// <param name="memberId">Member ID</param>
        /// <param name="memberTrainingSchedule">Member's chedule</param>
        /// <returns>True if found any</returns>
        public bool InitMemberSchedule(string memberId, out MemberTrainingSchedule memberTrainingSchedule)
        {
            if (!IsMemberExist(memberId))
            {
                memberTrainingSchedule = null;
                return false;
            }

            memberTrainingSchedule = MemberTrainingScheduleService.Get(item => item.Id == memberId);

            if (memberTrainingSchedule == null)
            {
                memberTrainingSchedule = new MemberTrainingSchedule();
                memberTrainingSchedule.Id = memberId;

                MemberTrainingScheduleService.Create(memberTrainingSchedule);
            }

            if (memberTrainingSchedule.ScheduledTrainingPlanIds == null)
            {
                memberTrainingSchedule.ScheduledTrainingPlanIds = new List<string>();
            }


            if (memberTrainingSchedule.AttendedTrainingPlanIds == null)
            {
                memberTrainingSchedule.AttendedTrainingPlanIds = new List<string>();
            }

            return true;
        }

        /// <summary>
        /// Query for instructor schedule
        /// </summary>
        /// <param name="instructorId">Instructor ID</param>
        /// <param name="instructorTrainingSchedule">Instructor schedule</param>
        /// <returns>True if exist</returns>
        public bool InitInstructorSchedule(string instructorId, out InstructorTrainingSchedule instructorTrainingSchedule)
        {
            if (!IsInstructorExist(instructorId))
            {
                instructorTrainingSchedule = null;
                return false;
            }

            instructorTrainingSchedule = InstructorTrainingScheduleService.Get(item => item.Id == instructorId);

            if (instructorTrainingSchedule == null)
            {
                instructorTrainingSchedule = new InstructorTrainingSchedule();
                instructorTrainingSchedule.Id = instructorId;

                InstructorTrainingScheduleService.Create(instructorTrainingSchedule);
            }

            if (instructorTrainingSchedule.ScheduledTrainingPlanIds == null)
            {
                instructorTrainingSchedule.ScheduledTrainingPlanIds = new List<string>();
            }


            if (instructorTrainingSchedule.AttendedTrainingPlanIds == null)
            {
                instructorTrainingSchedule.AttendedTrainingPlanIds = new List<string>();
            }

            return true;
        }

        /// <summary>
        /// Add plan IDs to member's schedule
        /// </summary>
        /// <param name="memberId">Member ID</param>
        /// <param name="errorMessage">error message if any</param>
        /// <param name="trainingPlanIds">IDs of training plans</param>
        /// <returns>True if successfully added</returns>
        public bool AddScheduledPlanToMember(string memberId, out string errorMessage, params string[] trainingPlanIds)
        {
            errorMessage = string.Empty;
            MemberTrainingSchedule memberTrainingSchedule;
            if (!InitMemberSchedule(memberId, out memberTrainingSchedule))
            {
                errorMessage = "Member ID not found";
                return false;
            }

            List<TrainingPlan> trainingPlansFound = null;
            if (!GetExistTrainingPlans(out trainingPlansFound, trainingPlanIds))
            {
                errorMessage = "Training plans not found";
                return false;
            }

            List<TrainingPlan> memberScheduledPlans = null;
            if (memberTrainingSchedule.ScheduledTrainingPlanIds != null
                && GetExistTrainingPlans(out memberScheduledPlans, memberTrainingSchedule.ScheduledTrainingPlanIds.ToArray()))
            {
                trainingPlansFound.AddRange(memberScheduledPlans);
            }

            //remove duplicate items
            trainingPlansFound = trainingPlansFound.GroupBy(s => s.Id)
                                                 .Select(grp => grp.FirstOrDefault())
                                                 .OrderBy(s => DateTime.ParseExact(s.StartDate, "yyyyMMddHHmm", System.Globalization.CultureInfo.InvariantCulture))
                                                 .ToList();

            //find overlapping
            List<DateTimeRange> dateTimeRanges = trainingPlansFound
                                                    .Select(i => new DateTimeRange(i.StartDate, i.EndDate, "yyyyMMddHHmm"))
                                                    .ToList();

            if (!DateTimeHelper.DoesNotOverlap(dateTimeRanges))
            {
                errorMessage = "Member schedule overlapped";
                return false;
            }

            //Add member Id back to training plans
            List<string> trainingPlanIdsFound = trainingPlansFound.Select(i => i.Id).ToList();

            trainingPlansFound.ForEach(i => AddMemberIdToTrainingPlan(i, memberId));

            //TODO: if there is no overlapping , store all ids to scheduled plans
            memberTrainingSchedule.ScheduledTrainingPlanIds = trainingPlanIdsFound;

            MemberTrainingScheduleService.Update(item => item.Id == memberTrainingSchedule.Id
                                                , memberTrainingSchedule);

            return true;
        }

        /// <summary>
        /// Remove plan IDs from member's schedule
        /// </summary>
        /// <param name="memberId">Member ID</param>
        /// <param name="trainingPlanIds">IDs of training plans</param>
        public void RemoveScheduledPlanFromMember(string memberId, params string[] trainingPlanIds)
        {
            MemberTrainingSchedule memberTrainingSchedule;
            if (InitMemberSchedule(memberId, out memberTrainingSchedule))
            {
                //Remove training plans from member's schedule
                memberTrainingSchedule.ScheduledTrainingPlanIds.RemoveAll(i => trainingPlanIds.Contains(i));

                MemberTrainingScheduleService.Update(item => item.Id == memberTrainingSchedule.Id
                                                    , memberTrainingSchedule);
            }

            List<TrainingPlan> trainingPlansFound = null;
            if (GetExistTrainingPlans(out trainingPlansFound, trainingPlanIds))
            {
                trainingPlansFound.ForEach(i => RemoveMemberIdFromTrainingPlan(i, memberId));
            }

            return;
        }

        /// <summary>
        /// Add plan IDs to member's attended records
        /// </summary>
        /// <param name="memberId">Member ID</param>
        /// <param name="errorMessage">error message if any</param>
        /// <param name="trainingPlanIds">IDs of training plans</param>
        /// <returns>True if successfully added</returns>
        public bool AddAttendedPlanToMember(string memberId, out string errorMessage, params string[] trainingPlanIds)
        {
            errorMessage = string.Empty;
            MemberTrainingSchedule memberTrainingSchedule;
            if (!InitMemberSchedule(memberId, out memberTrainingSchedule))
            {
                errorMessage = "Member ID not found";
                return false;
            }

            List<TrainingPlan> trainingPlansFound = null;
            if (!GetExistTrainingPlans(out trainingPlansFound, trainingPlanIds))
            {
                errorMessage = "Training plans not found";
                return false;
            }

            List<TrainingPlan> memberAttendedPlans = null;
            if (memberTrainingSchedule.AttendedTrainingPlanIds != null
                && GetExistTrainingPlans(out memberAttendedPlans, memberTrainingSchedule.AttendedTrainingPlanIds.ToArray()))
            {
                trainingPlansFound.AddRange(memberAttendedPlans);
            }

            //remove duplicate items
            trainingPlansFound = trainingPlansFound.GroupBy(s => s.Id)
                                                 .Select(grp => grp.FirstOrDefault())
                                                 .OrderBy(s => DateTime.ParseExact(s.StartDate, "yyyyMMddHHmm", System.Globalization.CultureInfo.InvariantCulture))
                                                 .ToList();

            //find overlapping
            List<DateTimeRange> dateTimeRanges = trainingPlansFound
                                                    .Select(i => new DateTimeRange(i.StartDate, i.EndDate, "yyyyMMddHHmm"))
                                                    .ToList();

            if (!DateTimeHelper.DoesNotOverlap(dateTimeRanges))
            {
                errorMessage = "Member attended plan overlapped";
                return false;
            }

            //Add member Id back to training plans
            List<string> trainingPlanIdsFound = trainingPlansFound.Select(i => i.Id).ToList();

            trainingPlansFound.ForEach(i => AddMemberIdToAttendedTrainingPlan(i, memberId));

            //TODO: if there is no overlapping , store all ids to attended plans
            memberTrainingSchedule.AttendedTrainingPlanIds = trainingPlanIdsFound;

            MemberTrainingScheduleService.Update(item => item.Id == memberTrainingSchedule.Id
                                                , memberTrainingSchedule);

            return true;
        }

        /// <summary>
        /// Add plan IDs to Member's schedule
        /// </summary>
        /// <param name="memberId">Member ID</param>
        /// <param name="trainingPlanIds">IDs of training plans</param>
        public void RemoveAttendedPlanFromMember(string memberId, params string[] trainingPlanIds)
        {
            MemberTrainingSchedule memberTrainingSchedule;
            if (InitMemberSchedule(memberId, out memberTrainingSchedule))
            {
                //Remove training plans from member's schedule
                memberTrainingSchedule.AttendedTrainingPlanIds.RemoveAll(i => trainingPlanIds.Contains(i));

                MemberTrainingScheduleService.Update(item => item.Id == memberTrainingSchedule.Id
                                                    , memberTrainingSchedule);
            }

            List<TrainingPlan> trainingPlansFound = null;
            if (GetExistTrainingPlans(out trainingPlansFound, trainingPlanIds))
            {
                trainingPlansFound.ForEach(i => RemoveMemberIdFromAttendedTrainingPlan(i, memberId));
            }

            return;
        }

        /// <summary>
        /// Add plan IDs to instructor's schedule
        /// </summary>
        /// <param name="instructorId">Instructor ID</param>
        /// <param name="errorMessage">error message if any</param>
        /// <param name="trainingPlanIds">IDs of training plans</param>
        /// <returns>True if successfully added</returns>
        public bool AddScheduledPlanToInstructor(string instructorId, out string errorMessage, params string[] trainingPlanIds)
        {
            errorMessage = string.Empty;

            InstructorTrainingSchedule instructorTrainingSchedule;
            if (!InitInstructorSchedule(instructorId, out instructorTrainingSchedule))
            {
                errorMessage = "Instructor not found";
                return false;
            }

            List<TrainingPlan> trainingPlansFound = null;
            if (!GetExistTrainingPlans(out trainingPlansFound, trainingPlanIds))
            {
                errorMessage = "Training plan not found";
                return false;
            }

            List<TrainingPlan> instructorScheduledPlans = null;
            if (instructorTrainingSchedule.ScheduledTrainingPlanIds != null
                && GetExistTrainingPlans(out instructorScheduledPlans, instructorTrainingSchedule.ScheduledTrainingPlanIds.ToArray()))
            {
                trainingPlansFound.AddRange(instructorScheduledPlans);
            }

            //remove duplicate items
            trainingPlansFound = trainingPlansFound.GroupBy(s => s.Id)
                                                 .Select(grp => grp.FirstOrDefault())
                                                 .OrderBy(s => DateTime.ParseExact(s.StartDate, "yyyyMMddHHmm", System.Globalization.CultureInfo.InvariantCulture))
                                                 .ToList();

            //find overlapping
            List<DateTimeRange> dateTimeRanges = trainingPlansFound
                                                    .Select(i => new DateTimeRange(i.StartDate, i.EndDate, "yyyyMMddHHmm"))
                                                    .ToList();

            if (!DateTimeHelper.DoesNotOverlap(dateTimeRanges))
            {
                errorMessage = "Schuedule plan time conflict";
                return false;
            }

            //Add instructor Id back to training plans
            List<string> trainingPlanIdsFound = trainingPlansFound.Select(i => i.Id).ToList();

            trainingPlansFound.ForEach(i => AddInstructorIdToTrainingPlan(i, instructorId));

            //TODO: if there is no overlapping , store all ids to scheduled plans
            instructorTrainingSchedule.ScheduledTrainingPlanIds = trainingPlanIdsFound;

            InstructorTrainingScheduleService.Update(item => item.Id == instructorTrainingSchedule.Id
                                                    , instructorTrainingSchedule);

            return true;
        }

        /// <summary>
        /// Remove plan IDs to instructor's schedule
        /// </summary>
        /// <param name="instructorId">Instructor ID</param>
        /// <param name="trainingPlanIds">IDs of training plans</param>
        public void RemoveScheduledPlanFromInstructor(string instructorId, params string[] trainingPlanIds)
        {
            InstructorTrainingSchedule instructorTrainingSchedule;
            if (InitInstructorSchedule(instructorId, out instructorTrainingSchedule))
            {
                //Remove training plans from member's schedule
                instructorTrainingSchedule.ScheduledTrainingPlanIds.RemoveAll(i => trainingPlanIds.Contains(i));

                InstructorTrainingScheduleService.Update(item => item.Id == instructorTrainingSchedule.Id
                                                    , instructorTrainingSchedule);
            }

            List<TrainingPlan> trainingPlansFound = null;
            if (GetExistTrainingPlans(out trainingPlansFound, trainingPlanIds))
            {
                trainingPlansFound.ForEach(i => RemoveInstructorIdFromTrainingPlan(i, instructorId));
            }

            return;
        }

        /// <summary>
        /// Add plan IDs to instructor's attended records
        /// </summary>
        /// <param name="instructorId">instructor ID</param>
        /// <param name="errorMessage">error message if any</param>
        /// <param name="trainingPlanIds">IDs of training plans</param>
        /// <returns>True if successfully added</returns>
        public bool AddAttendedPlanToInstructor(string instructorId, out string errorMessage, params string[] trainingPlanIds)
        {
            errorMessage = string.Empty;

            InstructorTrainingSchedule instructorTrainingSchedule;
            if (!InitInstructorSchedule(instructorId, out instructorTrainingSchedule))
            {
                errorMessage = "Instructor not found";
                return false;
            }

            List<TrainingPlan> trainingPlansFound = null;
            if (!GetExistTrainingPlans(out trainingPlansFound, trainingPlanIds))
            {
                errorMessage = "Training plan not found";
                return false;
            }

            List<TrainingPlan> instructorScheduledPlans = null;
            if (instructorTrainingSchedule.AttendedTrainingPlanIds != null
                && GetExistTrainingPlans(out instructorScheduledPlans, instructorTrainingSchedule.AttendedTrainingPlanIds.ToArray()))
            {
                trainingPlansFound.AddRange(instructorScheduledPlans);
            }

            //remove duplicate items
            trainingPlansFound = trainingPlansFound.GroupBy(s => s.Id)
                                                 .Select(grp => grp.FirstOrDefault())
                                                 .OrderBy(s => DateTime.ParseExact(s.StartDate, "yyyyMMddHHmm", System.Globalization.CultureInfo.InvariantCulture))
                                                 .ToList();

            //find overlapping
            List<DateTimeRange> dateTimeRanges = trainingPlansFound
                                                    .Select(i => new DateTimeRange(i.StartDate, i.EndDate, "yyyyMMddHHmm"))
                                                    .ToList();

            if (!DateTimeHelper.DoesNotOverlap(dateTimeRanges))
            {
                errorMessage = "Attended plan time conflict";
                return false;
            }

            //Add instructor Id back to training plans
            List<string> trainingPlanIdsFound = trainingPlansFound.Select(i => i.Id).ToList();

            trainingPlansFound.ForEach(i => AddInstructorIdToAttendedTrainingPlan(i, instructorId));

            //TODO: if there is no overlapping , store all ids to scheduled plans
            instructorTrainingSchedule.AttendedTrainingPlanIds = trainingPlanIdsFound;

            InstructorTrainingScheduleService.Update(item => item.Id == instructorTrainingSchedule.Id
                                                    , instructorTrainingSchedule);

            return true;
        }

        /// <summary>
        /// Remove plan IDs from instructor's attended records
        /// </summary>
        /// <param name="instructorId">instructor ID</param>
        /// <param name="trainingPlanIds">IDs of training plans</param>
        public void RemoveAttendedPlanFromInstructor(string instructorId, params string[] trainingPlanIds)
        {
            InstructorTrainingSchedule instructorTrainingSchedule;
            if (InitInstructorSchedule(instructorId, out instructorTrainingSchedule))
            {
                //Remove training plans from member's schedule
                instructorTrainingSchedule.AttendedTrainingPlanIds.RemoveAll(i => trainingPlanIds.Contains(i));

                InstructorTrainingScheduleService.Update(item => item.Id == instructorTrainingSchedule.Id
                                                    , instructorTrainingSchedule);
            }

            List<TrainingPlan> trainingPlansFound = null;
            if (GetExistTrainingPlans(out trainingPlansFound, trainingPlanIds))
            {
                trainingPlansFound.ForEach(i => RemoveInstructorIdFromAttendedTrainingPlan(i, instructorId));
            }

            return;
        }

        private void AddMemberIdToTrainingPlan(TrainingPlan trainingPlan, string memberId)
        {
            if (trainingPlan.ScheduledMemberIds == null)
            {
                trainingPlan.ScheduledMemberIds = new List<string>();
            }

            if (trainingPlan.ScheduledMemberIds != null
                && trainingPlan.ScheduledMemberIds.Count > 0
                && trainingPlan.ScheduledMemberIds.Contains(memberId))
                return;

            trainingPlan.ScheduledMemberIds.Add(memberId);

            //TODO: there should be a better way to do bult update
            TrainingPlanService.Update(item => item.Id == trainingPlan.Id, trainingPlan);
        }

        private void AddMemberIdToAttendedTrainingPlan(TrainingPlan trainingPlan, string memberId)
        {
            if (trainingPlan.AttendedMemberIds == null)
            {
                trainingPlan.AttendedMemberIds = new List<string>();
            }

            if (trainingPlan.AttendedMemberIds != null
                && trainingPlan.AttendedMemberIds.Count > 0
                && trainingPlan.AttendedMemberIds.Contains(memberId))
                return;

            trainingPlan.AttendedMemberIds.Add(memberId);

            //TODO: there should be a better way to do bult update
            TrainingPlanService.Update(item => item.Id == trainingPlan.Id, trainingPlan);
        }

        private void RemoveMemberIdFromTrainingPlan(TrainingPlan trainingPlan, string memberId)
        {
            if (trainingPlan.ScheduledMemberIds == null)
            {
                trainingPlan.ScheduledMemberIds = new List<string>();
            }

            if (trainingPlan.ScheduledMemberIds != null
                && trainingPlan.ScheduledMemberIds.Count > 0
                && !trainingPlan.ScheduledMemberIds.Contains(memberId))
                return;

            trainingPlan.ScheduledMemberIds.Remove(memberId);

            //TODO: there should be a better way to do bult update
            TrainingPlanService.Update(item => item.Id == trainingPlan.Id, trainingPlan);
        }

        private void RemoveMemberIdFromAttendedTrainingPlan(TrainingPlan trainingPlan, string memberId)
        {
            if (trainingPlan.AttendedMemberIds == null)
            {
                trainingPlan.AttendedMemberIds = new List<string>();
            }

            if (trainingPlan.AttendedMemberIds != null
                && trainingPlan.AttendedMemberIds.Count > 0
                && !trainingPlan.AttendedMemberIds.Contains(memberId))
                return;

            trainingPlan.AttendedMemberIds.Remove(memberId);

            //TODO: there should be a better way to do bult update
            TrainingPlanService.Update(item => item.Id == trainingPlan.Id, trainingPlan);
        }

        private void AddInstructorIdToTrainingPlan(TrainingPlan trainingPlan, string instructorId)
        {
            if (trainingPlan.AssignedInstructorIds == null)
            {
                trainingPlan.AssignedInstructorIds = new List<string>();
            }

            if (trainingPlan.AssignedInstructorIds != null
                && trainingPlan.AssignedInstructorIds.Count > 0
                && trainingPlan.AssignedInstructorIds.Contains(instructorId))
                return;

            trainingPlan.AssignedInstructorIds.Add(instructorId);

            //TODO: there should be a better way to do bult update
            TrainingPlanService.Update(item => item.Id == trainingPlan.Id, trainingPlan);
        }

        private void AddInstructorIdToAttendedTrainingPlan(TrainingPlan trainingPlan, string instructorId)
        {
            if (trainingPlan.AttendedInstructorIds == null)
            {
                trainingPlan.AttendedInstructorIds = new List<string>();
            }

            if (trainingPlan.AttendedInstructorIds != null
                && trainingPlan.AttendedInstructorIds.Count > 0
                && trainingPlan.AttendedInstructorIds.Contains(instructorId))
                return;

            trainingPlan.AttendedInstructorIds.Add(instructorId);

            //TODO: there should be a better way to do bult update
            TrainingPlanService.Update(item => item.Id == trainingPlan.Id, trainingPlan);
        }

        private void RemoveInstructorIdFromTrainingPlan(TrainingPlan trainingPlan, string instructorId)
        {
            if (trainingPlan.AssignedInstructorIds == null)
            {
                trainingPlan.AssignedInstructorIds = new List<string>();
            }

            if (trainingPlan.AssignedInstructorIds != null
                && trainingPlan.AssignedInstructorIds.Count > 0
                && !trainingPlan.AssignedInstructorIds.Contains(instructorId))
                return;

            trainingPlan.AssignedInstructorIds.Remove(instructorId);

            //TODO: there should be a better way to do bult update
            TrainingPlanService.Update(item => item.Id == trainingPlan.Id, trainingPlan);
        }

        private void RemoveInstructorIdFromAttendedTrainingPlan(TrainingPlan trainingPlan, string instructorId)
        {
            if (trainingPlan.AttendedInstructorIds == null)
            {
                trainingPlan.AttendedInstructorIds = new List<string>();
            }

            if (trainingPlan.AttendedInstructorIds != null
                && trainingPlan.AttendedInstructorIds.Count > 0
                && !trainingPlan.AttendedInstructorIds.Contains(instructorId))
                return;

            trainingPlan.AttendedInstructorIds.Remove(instructorId);

            //TODO: there should be a better way to do bult update
            TrainingPlanService.Update(item => item.Id == trainingPlan.Id, trainingPlan);
        }
    }
}
