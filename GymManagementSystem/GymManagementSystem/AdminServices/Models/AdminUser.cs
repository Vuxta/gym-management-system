﻿// <copyright file="AdminUser.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/03/2019 10:14:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 3, 2019

using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using VuxtaStudio.Common.Services.Models;

namespace VuxtaStudio.GymManagementSystem.AdminServices.Models
{
    /// <summary>
    /// Model of AdminUser CRUD service
    /// </summary>
    public class AdminUser : UserCrudModel
    {
        [BsonElement("ContactNumbers")]
        public List<string> ContactNumbers;

        [BsonElement("ContactEmails")]
        public List<string> ContactEmails;
    }
}
