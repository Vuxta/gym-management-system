﻿// <copyright file="AdminUserController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/03/2019 10:14:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 3, 2019

using Microsoft.AspNetCore.Mvc;
using VuxtaStudio.Common.Services.Controllers;
using VuxtaStudio.GymManagementSystem.AdminServices.Models;
using VuxtaStudio.GymManagementSystem.AdminServices.Services;
using VuxtaStudio.GymManagementSystem.Global.Defines;
using VuxtaStudio.GymManagementSystem.Global.Filters;

namespace VuxtaStudio.GymManagementSystem.AdminServices.Controllers
{
    /// <summary>
    /// AdminUser controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [JwtUserAuthActionFilter(AllowAdmin = true, AdminRoles = AdminRoles.SuperUser | AdminRoles.AdminManager)]
    public class AdminUserCrudController : CrudControllerBase<AdminUser>
    {
        private readonly AdminUserCrudService m_AdminUserCrudService;

        /// <summary>
        /// Constructor of controller
        /// </summary>
        /// <param name="adminUserCrudService">CRUD services</param>
        public AdminUserCrudController(AdminUserCrudService adminUserCrudService)
            : base(adminUserCrudService)
        {
            m_AdminUserCrudService = adminUserCrudService;
        }
    }
}
