﻿// <copyright file="AdminUserCrudService.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/03/2019 10:14:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 3, 2019

using Microsoft.Extensions.Configuration;
using VuxtaStudio.Common.Services;
using VuxtaStudio.GymManagementSystem.AdminServices.Models;
using VuxtaStudio.GymManagementSystem.Global.Defines;

namespace VuxtaStudio.GymManagementSystem.AdminServices.Services
{
    /// <summary>
    /// Implementation of AdminUser CRUD service
    /// </summary>
    public class AdminUserCrudService : UserCrudService<AdminUser>
    {
        protected override string m_GroupName
        {
            get
            {
                return GroupName.Admin;
            }
        }

        /// <summary>
        /// Constructor of AdminUser CRUD service
        /// </summary>
        /// <param name="config">configuration passed at StartUp</param>
        public AdminUserCrudService(IConfiguration config)
            : base(config.GetConnectionString("AdminUserDb"), "AdminUserDb", "AdminUser")
        {
        }
    }
}
