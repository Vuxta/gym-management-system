﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Serialization;
using VuxtaStudio.GymManagementSystem.AdminServices.Services;
using VuxtaStudio.GymManagementSystem.InstructorServices.Services;
using VuxtaStudio.GymManagementSystem.MemberServices.Services;
using VuxtaStudio.GymManagementSystem.TrainingServices.Services;

namespace VuxtaStudio.GymManagementSystem
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<AdminUserCrudService>();
            services.AddScoped<MemberCrudService>();
            services.AddScoped<MemberPhysicalStatusService>();
            services.AddScoped<InstructorCrudService>();
            services.AddScoped<CourseCategoryCrudService>();
            services.AddScoped<CoursePlanCrudService>();
            services.AddScoped<TrainingCategoryCrudService>();
            services.AddScoped<TrainingPlanCrudService>();
            services.AddScoped<TrainingScheduleMasterService>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
                .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver()); ;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
