﻿// <copyright file="InstructorRegisterController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/10/2019 11:49:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 10, 2019

using System;
using Microsoft.AspNetCore.Mvc;
using VuxtaStudio.Common.Utils.Token;
using VuxtaStudio.GymManagementSystem.Global.Defines;
using VuxtaStudio.GymManagementSystem.Global.Filters;
using VuxtaStudio.GymManagementSystem.InstructorServices.Models;
using VuxtaStudio.GymManagementSystem.InstructorServices.Services;
using VuxtaStudio.GymManagementSystem.RegisterService.Models;

namespace VuxtaStudio.GymManagementSystem.RegisterService.Controllers
{
    /// <summary>
    /// Instructor register controller for Web API key
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [JwtUserAuthActionFilter(AllowAdmin = true, AdminRoles = AdminRoles.InstructorManager)]
    public class InstructorRegisterController : UserRegisterControllerBase
    {
        private readonly InstructorCrudService m_InstructorCrudService;

        /// <summary>
        /// Constructor of register controller
        /// </summary>
        /// <param name="instructorCrudService">Instructor CRUD service</param>
        public InstructorRegisterController(InstructorCrudService instructorCrudService)
            : base(instructorCrudService.GetGroupName())
        {
            m_InstructorCrudService = instructorCrudService;
        }

        /// <summary>
        /// Create token by register data
        /// </summary>
        /// <param name="data">Register data</param>
        /// <param name="token">Generated Web API key</param>
        /// <param name="errorMessage">Error message if any</param>
        /// <returns>Return true if API Key create success</returns>
        protected override bool CreateToken(RegisterData data, out string token, out string errorMessage)
        {
            token = string.Empty;
            errorMessage = string.Empty;
            Instructor result = m_InstructorCrudService.Get(item => item.Id == data.UserId);

            if (result == null)
            {
                errorMessage = "Member data not found";
                return false;
            }

            result.RegisterDate = DateTime.Now.ToString("yyyyMMddHHmmss");

            m_InstructorCrudService.Update(item => item.Id == result.Id, result);

            string validateHash = HashHelper.ComputeHash(result.GetHashCandidate());

            token = TokenHelper.GenerateApiKey(data.UserId, data.Password, data.Group, validateHash, data.Extras);

            return true;
        }
    }
}
