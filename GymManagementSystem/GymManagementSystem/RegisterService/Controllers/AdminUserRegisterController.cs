﻿// <copyright file="RegisterData.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/05/2019 14:12:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 5, 2019

using System;
using Microsoft.AspNetCore.Mvc;
using VuxtaStudio.GymManagementSystem.RegisterService.Models;
using VuxtaStudio.Common.Utils.Token;
using VuxtaStudio.GymManagementSystem.AdminServices.Services;
using VuxtaStudio.GymManagementSystem.AdminServices.Models;
using VuxtaStudio.GymManagementSystem.Global.Filters;
using VuxtaStudio.GymManagementSystem.Global.Defines;

namespace VuxtaStudio.GymManagementSystem.RegisterService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [JwtUserAuthActionFilter(AllowAdmin = true, AdminRoles = AdminRoles.AdminManager)]
    public class AdminUserRegisterController : UserRegisterControllerBase
    {
        private readonly AdminUserCrudService m_AdminUserCrudService;

        public AdminUserRegisterController(AdminUserCrudService adminUserCrudService)
            : base(adminUserCrudService.GetGroupName())
        {
            m_AdminUserCrudService = adminUserCrudService;
        }

        protected override bool CreateToken(RegisterData data, out string token, out string errorMessage)
        {
            token = string.Empty;
            errorMessage = string.Empty;
            AdminUser result = m_AdminUserCrudService.Get(item => item.Id == data.UserId);

            if (result == null)
            {
                errorMessage = "Admin user data not found";
                return false;
            }

            result.RegisterDate = DateTime.Now.ToString("yyyyMMddHHmmss");

            m_AdminUserCrudService.Update(item => item.Id == result.Id, result);

            string validateHash = HashHelper.ComputeHash(result.GetHashCandidate());

            token = TokenHelper.GenerateApiKey(data.UserId, data.Password, data.Group, validateHash, data.Extras);

            return true;
        }
    }
}
