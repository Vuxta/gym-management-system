﻿// <copyright file="UserRegisterControllerBase.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/05/2019 14:12:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 5, 2019

using Microsoft.AspNetCore.Mvc;
using VuxtaStudio.GymManagementSystem.RegisterService.Models;

namespace VuxtaStudio.GymManagementSystem.RegisterService.Controllers
{
    /// <summary>
    /// Base Class of User register controller
    /// </summary>
    public abstract class UserRegisterControllerBase : ControllerBase
    {
        protected readonly string m_GroupName;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="groupName">Name of user service group</param>
        public UserRegisterControllerBase(string groupName)
        {
            m_GroupName = groupName;
        }

        /// <summary>
        /// Post method for user registeration
        /// </summary>
        /// <param name="registerData">Register data used for registering</param>
        /// <returns>API toekn for accessing the service</returns>
        [HttpPost]
        public ActionResult<string> Post(RegisterData registerData)
        {
            if (registerData.Group != m_GroupName)
            {
                return BadRequest();
            }

            if (string.IsNullOrEmpty(registerData.UserId))
            {
                return BadRequest();
            }

            if (string.IsNullOrEmpty(registerData.Password))
            {
                return BadRequest();
            }

            string token = string.Empty;
            string errorMessage = string.Empty;

            if (!CreateToken(registerData, out token, out errorMessage))
            {
                return BadRequest(string.Format("Fail to create token, error: {0}", errorMessage));
            }

            return token;
        }

        /// <summary>
        /// Creating token from register data
        /// </summary>
        /// <param name="data">Registration data</param>
        /// <param name="token">token generated</param>
        /// <param name="errorMessage">error message generated</param>
        /// <returns>Return true if success</returns>
        protected abstract bool CreateToken(RegisterData data, out string token, out string errorMessage);
    }
}
