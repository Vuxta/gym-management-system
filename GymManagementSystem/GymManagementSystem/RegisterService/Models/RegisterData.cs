﻿// <copyright file="RegisterData.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/05/2019 14:12:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 5, 2019

namespace VuxtaStudio.GymManagementSystem.RegisterService.Models
{
    public class RegisterData
    {
        public string UserId { get; set; }
        public string Password { get; set; }
        public string Group { get; set; }
        public string[] Extras { get; set; }
    }
}
