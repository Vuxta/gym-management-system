﻿// <copyright file="JwtUserAuthActionFilter.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/19/2019 15:25:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 19, 2019

using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Filters;
using VuxtaStudio.Common.Filters;
using VuxtaStudio.Common.Utils.Token;
using VuxtaStudio.GymManagementSystem.Global.Defines;

namespace VuxtaStudio.GymManagementSystem.Global.Filters
{
    /// <summary>
    /// User JWT authentication filter
    /// </summary>
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = true)]
    public class JwtUserAuthActionFilter : JwtAuthBase
    {
        public bool AllowAdmin { get; set; } = true;
        public bool AllowInstructor { get; set; } = false;
        public bool AllowMember { get; set; } = false;

        public bool CheckAdminIdRouteMatch { get; set; } = false;
        public bool CheckInstructorIdRouteMatch { get; set; } = true;
        public bool CheckMemberIdRouteMatch { get; set; } = true;

        public long AdminRoles { get; set; } = 1; //AdminRoles.SuperUser
        public long InstructorRoles { get; set; } = 1;
        public long MemberRoles { get; set; } = 1;

        private string[] m_TokenFileds = new string[] { TokenField.UserId,
                                                        TokenField.Expire,
                                                        TokenField.Group,
                                                        TokenField.IpAddress,
                                                        TokenField.Roles};

        /// <summary>
        /// Parsing decoded token and verify if session information is valid
        /// </summary>
        /// <param name="result">Decoded session token</param>
        /// <param name="actionContext">Executing context</param>
        /// <returns>Return true if verification passed</returns>
        public override bool OnDecodeVerify(Dictionary<string, object> result, ActionExecutingContext actionContext)
        {
            if (!TokenHelper.IsAllTokenValid(result, m_TokenFileds))
            {
                return false;
            }

            if ((string)result[TokenField.IpAddress]
                != actionContext.HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString())
            {
                return false;
            }

            long roleBitwise = (long)result[TokenField.Roles];
            string UserGroup = (string)result[TokenField.Group];
            bool isVerified = false;
            string userId = (string)result[TokenField.UserId];
            string idRoute = actionContext.RouteData.Values["id"] as string;

            switch (UserGroup)
            {
                case GroupName.Admin:

                    if (AllowAdmin)
                    {
                        isVerified = CheckVerification(roleBitwise, AdminRoles, userId, idRoute, CheckAdminIdRouteMatch);
                    }

                    break;
                case GroupName.Member:

                    if (AllowMember)
                    {
                        isVerified = CheckVerification(roleBitwise, MemberRoles, userId, idRoute, CheckMemberIdRouteMatch);
                    }

                    break;
                case GroupName.Instructor:

                    if (AllowInstructor)
                    {
                        isVerified = CheckVerification(roleBitwise, InstructorRoles, userId, idRoute, CheckInstructorIdRouteMatch);
                    }

                    break;
                default:
                    return false;
            }

            return isVerified;
        }

        /// <summary>
        /// Check if roles and id are match for the requirements
        /// </summary>
        /// <param name="roleInput">Bitwise roles from decoded token</param>
        /// <param name="allowRoles">Roles allowed in bitwise format</param>
        /// <param name="userId">User ID decoded from token</param>
        /// <param name="inputId">ID from route</param>
        /// <param name="checkIdMatch">True if we need to check if ID is matched</param>
        /// <returns>Return true if passed</returns>
        private bool CheckVerification(long roleInput, long allowRoles, string userId, string inputId, bool checkIdMatch)
        {
            if (checkIdMatch)
            {
                if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(inputId))
                    return false;

                if (userId != inputId)
                    return false;
            }

            return ((roleInput & allowRoles) != 0);
        }
    }
}
