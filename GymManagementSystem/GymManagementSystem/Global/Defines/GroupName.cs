﻿// <copyright file="GroupName.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/19/2019 15:25:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 19, 2019

namespace VuxtaStudio.GymManagementSystem.Global.Defines
{
    /// <summary>
    /// Group name defines
    /// </summary>
    public static class GroupName
    {
        public const string Admin = "Admin";
        public const string Member = "Member";
        public const string Instructor = "Instructor";
    }
}
