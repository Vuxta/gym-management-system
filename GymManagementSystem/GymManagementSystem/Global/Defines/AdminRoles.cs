﻿// <copyright file="AdminRoles.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/19/2019 15:25:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 19, 2019

namespace VuxtaStudio.GymManagementSystem.Global.Defines
{
    /// <summary>
    /// Admin roles defines
    /// </summary>
    public static class AdminRoles
    {
        public const long SuperUser = 1 << 0;
        public const long AdminManager = 1 << 1;
        public const long InstructorManager = 1 << 2;
        public const long MemberManager = 1 << 3;
    }
}
