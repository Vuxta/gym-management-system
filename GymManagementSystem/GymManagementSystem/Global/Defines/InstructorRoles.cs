﻿// <copyright file="InstructorRoles.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/19/2019 15:25:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 19, 2019

namespace VuxtaStudio.GymManagementSystem.Global.Defines
{
    /// <summary>
    /// Instructor roles defines
    /// </summary>
    public static class InstructorRoles
    {
        public const long Normal = 1 << 0;
    }
}
