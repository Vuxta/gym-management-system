﻿// <copyright file="MemberPhysicalStatusService.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/18/2019 14:52:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 18, 2019

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using VuxtaStudio.GymManagementSystem.MemberServices.Models;

namespace VuxtaStudio.GymManagementSystem.MemberServices.Services
{
    /// <summary>
    /// Service for handling member physical status records
    /// </summary>
    public class MemberPhysicalStatusService
    {
        public readonly MemberCrudService MemberService;
        public readonly PhysicalStatusCrudService PhysicalStatusService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="configuration"></param>
        public MemberPhysicalStatusService(IConfiguration configuration)
        {
            this.MemberService = new MemberCrudService(configuration);
            this.PhysicalStatusService = new PhysicalStatusCrudService(configuration);
        }

        /// <summary>
        /// Check if member ID exist
        /// </summary>
        /// <param name="memberId">member ID</param>
        /// <returns>True if exist</returns>
        public bool IsMemberExist(string memberId)
        {
            Member memberData = MemberService.Get(item => item.Id == memberId);
            return (memberData != null);
        }

        /// <summary>
        /// Query for member physical status
        /// </summary>
        /// <param name="memberId">Member ID</param>
        /// <param name="memberPhysicalStatus">Member's physical status</param>
        /// <returns>True if found any</returns>
        public bool InitMemberStatus(string memberId, out PhysicalStatus memberPhysicalStatus)
        {
            memberPhysicalStatus = PhysicalStatusService.Get(item => item.Id == memberId);

            if (memberPhysicalStatus != null)
            {
                return true;
            }

            if (!IsMemberExist(memberId))
            {
                return false;
            }

            memberPhysicalStatus = new PhysicalStatus();
            memberPhysicalStatus.Id = memberId;

            PhysicalStatusService.Create(memberPhysicalStatus);

            return true;
        }

        /// <summary>
        /// Upload physical info
        /// </summary>
        /// <param name="memberId">Member ID</param>
        /// <param name="info">Physical infos</param>
        /// <returns>True if success</returns>
        public bool AddNewInfo(string memberId, PhysicalInfo[] info)
        {
            if (info == null || info.Length <= 0)
                return false;

            for (int i = 0; i < info.Length; i++)
            {
                if (string.IsNullOrEmpty(info[i].Date))
                {
                    return false;
                }
            }

            PhysicalStatus memberStatus;
            if (!InitMemberStatus(memberId, out memberStatus))
            {
                return false;
            }

            if (memberStatus.PhysicalInfos == null)
            {
                memberStatus.PhysicalInfos = new List<PhysicalInfo>();
            }

            memberStatus.PhysicalInfos.AddRange(info);

            memberStatus.PhysicalInfos = memberStatus.PhysicalInfos.GroupBy(s => s.Date)
                                                 .Select(grp => grp.FirstOrDefault())
                                                 .OrderBy(s => DateTime.ParseExact(s.Date, "yyyyMMddHHmmss", System.Globalization.CultureInfo.InvariantCulture))
                                                 .ToList();

            PhysicalStatusService.Update(item => item.Id == memberStatus.Id, memberStatus);

            return true;
        }
    }
}
