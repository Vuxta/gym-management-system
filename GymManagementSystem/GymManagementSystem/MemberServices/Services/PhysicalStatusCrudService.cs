﻿// <copyright file="PhysicalStatusCrudService.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/18/2019 14:52:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 18, 2019

using Microsoft.Extensions.Configuration;
using VuxtaStudio.Common.Services;
using VuxtaStudio.GymManagementSystem.MemberServices.Models;

namespace VuxtaStudio.GymManagementSystem.MemberServices.Services
{
    /// <summary>
    /// Basic Physical status CRUD service
    /// </summary>
    public class PhysicalStatusCrudService : MongoDbCrudService<PhysicalStatus>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="config"></param>
        public PhysicalStatusCrudService(IConfiguration config)
            : base(config.GetConnectionString("MemberDb"), "MemberDb", "PhysicalStatus")
        {
        }
    }
}
