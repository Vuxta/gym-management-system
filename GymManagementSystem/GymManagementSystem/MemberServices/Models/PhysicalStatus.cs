﻿// <copyright file="PhysicalStatus.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/18/2019 14:52:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 18, 2019

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace VuxtaStudio.GymManagementSystem.MemberServices.Models
{
    /// <summary>
    /// Member physical status model
    /// </summary>
    public class PhysicalStatus
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("PhysicalInfos")]
        public List<PhysicalInfo> PhysicalInfos;
    }
}
