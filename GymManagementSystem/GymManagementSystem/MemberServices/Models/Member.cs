﻿// <copyright file="Member.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/10/2019 11:49:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 10, 2019

using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using VuxtaStudio.Common.Services.Models;

namespace VuxtaStudio.GymManagementSystem.MemberServices.Models
{
    /// <summary>
    /// Member CRUD model
    /// </summary>
    public class Member : UserCrudModel
    {
        [BsonElement("ContactNumbers")]
        public List<string> ContactNumbers;

        [BsonElement("ContactEmails")]
        public List<string> ContactEmails;
    }
}
