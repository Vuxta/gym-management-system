﻿// <copyright file="PhysicalInfo.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/18/2019 14:52:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 18, 2019

using MongoDB.Bson.Serialization.Attributes;

namespace VuxtaStudio.GymManagementSystem.MemberServices.Models
{
    /// <summary>
    /// Physical info
    /// </summary>
    public class PhysicalInfo
    {
        [BsonElement("Height")]
        public double Height;

        [BsonElement("Weight")]
        public double Weight;

        [BsonElement("Date")]
        public string Date;
    }
}
