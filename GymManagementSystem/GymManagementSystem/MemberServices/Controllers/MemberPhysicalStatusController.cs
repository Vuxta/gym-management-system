﻿// <copyright file="MemberPhysicalStatusController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/18/2019 14:52:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 18, 2019

using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using VuxtaStudio.GymManagementSystem.Global.Defines;
using VuxtaStudio.GymManagementSystem.Global.Filters;
using VuxtaStudio.GymManagementSystem.MemberServices.Models;
using VuxtaStudio.GymManagementSystem.MemberServices.Services;

namespace VuxtaStudio.GymManagementSystem.MemberServices.Controllers
{
    /// <summary>
    /// Controller for Member physical status
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MemberPhysicalStatusController : ControllerBase
    {
        private MemberPhysicalStatusService m_MemberPhysicalStatusService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="memberPhysicalStatusService"></param>
        public MemberPhysicalStatusController(MemberPhysicalStatusService memberPhysicalStatusService)
        {
            m_MemberPhysicalStatusService = memberPhysicalStatusService;
        }

        /// <summary>
        /// List all member physical status
        /// </summary>
        /// <returns>all member physical status</returns>
        [HttpGet]
        [JwtUserAuthActionFilter(AllowAdmin = true, AdminRoles = AdminRoles.MemberManager)]
        public ActionResult<List<PhysicalStatus>> GetAllMemberStatus()
        {
            return m_MemberPhysicalStatusService.PhysicalStatusService.Get();
        }

        /// <summary>
        /// Specific member physical status
        /// </summary>
        /// <param name="id">Member ID</param>
        /// <returns>Physical status</returns>
        [HttpGet("{id:length(24)}")]
        [JwtUserAuthActionFilter(AllowAdmin = true,
                                AllowMember = true,
                                AdminRoles = AdminRoles.MemberManager,
                                MemberRoles = MemberRoles.Normal,
                                CheckMemberIdRouteMatch = true)]
        public ActionResult<PhysicalStatus> GetMemberStatus(string id)
        {
            PhysicalStatus physicalStatus = m_MemberPhysicalStatusService.PhysicalStatusService.Get(item => item.Id == id);

            if (physicalStatus == null)
                return NotFound();

            return physicalStatus;
        }

        /// <summary>
        /// Upload Physical infos
        /// </summary>
        /// <param name="id">Member ID</param>
        /// <param name="data">Physical infos</param>
        /// <returns>Updated physical status of member</returns>
        [HttpPut("{id:length(24)}")]
        [JwtUserAuthActionFilter(AllowAdmin = true,
                                AllowMember = true,
                                AdminRoles = AdminRoles.MemberManager,
                                MemberRoles = MemberRoles.Normal,
                                CheckMemberIdRouteMatch = true)]
        public ActionResult<PhysicalStatus> UploadInfo(string id, PhysicalInfo[] data)
        {
            if (!m_MemberPhysicalStatusService.AddNewInfo(id, data))
                return BadRequest("Fail to upload info");

            return m_MemberPhysicalStatusService.PhysicalStatusService.Get(item => item.Id == id);
        }
    }
}
