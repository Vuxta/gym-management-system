﻿// <copyright file="MemberCrudController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/10/2019 11:49:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 10, 2019

using Microsoft.AspNetCore.Mvc;
using VuxtaStudio.Common.Services.Controllers;
using VuxtaStudio.GymManagementSystem.Global.Defines;
using VuxtaStudio.GymManagementSystem.Global.Filters;
using VuxtaStudio.GymManagementSystem.MemberServices.Models;
using VuxtaStudio.GymManagementSystem.MemberServices.Services;

namespace VuxtaStudio.GymManagementSystem.MemberServices.Controllers
{
    /// <summary>
    /// Member CRUD Web API Controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    [JwtUserAuthActionFilter(AllowAdmin = true, AdminRoles = AdminRoles.MemberManager)]
    public class MemberCrudController : CrudControllerBase<Member>
    {
        private readonly MemberCrudService m_MemberCrudService;

        /// <summary>
        /// Constructor of controller
        /// </summary>
        /// <param name="memberUserCrudService">CRUD services</param>
        public MemberCrudController(MemberCrudService memberCrudService)
            : base(memberCrudService)
        {
            m_MemberCrudService = memberCrudService;
        }
    }
}
