﻿// <copyright file="InstructorLoginController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/10/2019 11:49:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 10, 2019

using Microsoft.AspNetCore.Mvc;
using VuxtaStudio.GymManagementSystem.InstructorServices.Models;
using VuxtaStudio.GymManagementSystem.InstructorServices.Services;

namespace VuxtaStudio.GymManagementSystem.LoginServices.Controllers
{
    /// <summary>
    /// Instructor login controller for session token
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class InstructorLoginController : UserLoginControllerBase<Instructor>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="instructorCrudService">Instructor CRUD service</param>
        public InstructorLoginController(InstructorCrudService instructorCrudService)
            : base(instructorCrudService)
        {
        }
    }
}
