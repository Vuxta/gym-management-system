﻿// <copyright file="MemberLoginController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/10/2019 11:49:10 AM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 10, 2019

using Microsoft.AspNetCore.Mvc;
using VuxtaStudio.GymManagementSystem.MemberServices.Models;
using VuxtaStudio.GymManagementSystem.MemberServices.Services;

namespace VuxtaStudio.GymManagementSystem.LoginServices.Controllers
{
    /// <summary>
    /// Member login controller for generate session token
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class MemberLoginController : UserLoginControllerBase<Member>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="memberUserCrudService">Member CRUD service</param>
        public MemberLoginController(MemberCrudService memberCrudService)
            : base(memberCrudService)
        {
        }
    }
}
