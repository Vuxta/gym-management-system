﻿// <copyright file="AdminUserLoginController.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/05/2019 14:12:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 5, 2019

using Microsoft.AspNetCore.Mvc;
using VuxtaStudio.GymManagementSystem.AdminServices.Models;
using VuxtaStudio.GymManagementSystem.AdminServices.Services;

namespace VuxtaStudio.GymManagementSystem.LoginServices.Controllers
{
    /// <summary>
    /// AdminUser login controller
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class AdminUserLoginController : UserLoginControllerBase<AdminUser>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="adminUserCrudService">Admin User CRUD service</param>
        public AdminUserLoginController(AdminUserCrudService adminUserCrudService)
            : base(adminUserCrudService)
        {
        }
    }
}
