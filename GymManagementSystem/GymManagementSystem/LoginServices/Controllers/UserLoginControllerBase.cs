﻿// <copyright file="UserLoginControllerBase.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/05/2019 14:12:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 5, 2019

using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using VuxtaStudio.Common.Services;
using VuxtaStudio.Common.Services.Models;
using VuxtaStudio.Common.Utils.Token;
using VuxtaStudio.GymManagementSystem.LoginServices.Models;

namespace VuxtaStudio.GymManagementSystem.LoginServices.Controllers
{
    /// <summary>
    /// Base class of User login controller
    /// </summary>
    /// <typeparam name="T">Type of user CRUD model</typeparam>
    public abstract class UserLoginControllerBase<T> : ControllerBase where T : UserCrudModel
    {
        protected readonly UserCrudService<T> m_UserCrudService;

        private string[] m_TokenFileds = new string[] { TokenField.UserId, TokenField.Password, TokenField.Group, TokenField.Validate };

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userCrudService">User CRUD service</param>
        public UserLoginControllerBase(UserCrudService<T> userCrudService)
        {
            m_UserCrudService = userCrudService;
        }

        /// <summary>
        /// Post method for login request
        /// </summary>
        /// <param name="loginData">Login info</param>
        /// <returns>Return session token</returns>
        [HttpPost]
        public ActionResult<string> Post(LoginData loginData)
        {
            if (string.IsNullOrEmpty(loginData.UserId))
            {
                return BadRequest("Missing usedId");
            }

            if (string.IsNullOrEmpty(loginData.Password))
            {
                return BadRequest("Missing password");
            }

            if (string.IsNullOrEmpty(loginData.Key))
            {
                return BadRequest("Missing key");
            }

            Dictionary<string, object> result = null;
            string resultMessage = string.Empty;

            if (!TokenHelper.DecodeKey(loginData.Key, out result, out resultMessage))
            {
                return BadRequest(string.Format("Key is in correct, error: {0}", resultMessage));
            }

            if (!TokenHelper.IsAllTokenValid(result, m_TokenFileds))
            {
                return BadRequest("Missing decoded fields");
            }

            if ((string)result[TokenField.UserId] != loginData.UserId
                || (string)result[TokenField.Password] != loginData.Password)
            {
                return BadRequest("UserID or password is incorrect");
            }

            if ((string)result["group"] != m_UserCrudService.GetGroupName())
            {
                return BadRequest("Incorrect group");
            }

            UserCrudModel dbData = m_UserCrudService.Get(item => item.Id == (string)result[TokenField.UserId]);

            if (dbData == null)
            {
                return NotFound("Data not found");
            }

            string validateHash = HashHelper.ComputeHash(dbData.GetHashCandidate());

            if ((string)result["validate"] != validateHash)
            {
                return BadRequest("Incorrect validate code");
            }

            string token = TokenHelper.GenerateSessionToken((string)result[TokenField.UserId],
                                                            DateTimeOffset.UtcNow.AddHours(1).ToUnixTimeSeconds(),
                                                            (string)result[TokenField.Group],
                                                            HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString(),
                                                            dbData.Roles);

            return token;
        }
    }
}
