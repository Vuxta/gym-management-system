﻿// <copyright file="LoginData.cs" company="Tzu Yen Peng">
//   Copyright(C) Tzu Yen Peng - All Rights Reserved
// </copyright>
// <author>Tzu Yen Peng</author>
// <date>06/05/2019 14:12:10 PM</date>
// Unauthorized copying of this code, via any medium is strictly prohibited
// Proprietary and confidential
// Written by Tzu Yen Peng <zombiebenz @gmail.com>, June 5, 2019

namespace VuxtaStudio.GymManagementSystem.LoginServices.Models
{
    /// <summary>
    /// Login data
    /// </summary>
    public class LoginData
    {
        public string UserId { get; set; }
        public string Password { get; set; }
        public string Key { get; set; }
    }
}
